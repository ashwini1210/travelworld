# TravelWorld

## Summery

TravelWorld a curated marketplace for unique and untold experiences, top festivals, excursions, events, activities around the globe. 
TravelWorld connects travellers with passionate local people with similar interests, who help them feel and understand local culture,
customs, places, history, festivals and uniqueness of the place. Experience a city through the eyes of a local. Experiential Travelling
with Passionate Locals, not professional guides is what makes us unique. Uber for local guides, activities, experiences and things to
do.

## Version

This app is developed for iPhone using `Swift 4.2`

## Build and Runtime Requirements

1. Xcode 10.0 or later
2. iOS 12 or later
3. OS X v10.14 or later

## Configuring the Project

1. Clone the repository 
2. command 'pod install' on the Terminal
3. Open TravelWorld.xcworkspace

## Pods:

1. Alamofire'
2. SwiftyJSON'
3. RealmSwift'
4. SkyFloatingLabelTextField'
5. SDWebImage/WebP'
6. DLRadioButton'
7. Toast-Swift'
8. FacebookCore'
9. FacebookLogin'
10. FacebookShare'
11. GoogleSignIn'
12. razorpay-pod'
13. Firebase'
14. Firebase/Messaging'

## How it works:

1. Choose a city where the customer is travelling to and book a local guy or any unique experience.
2. Choose about interests, travel plans so that it will manage to match a perfect local guide for customers.
3. Meet local amigo and start exploring new cultures and new cities.

## Features:

1. Listing of all cities and their famous spots to visit.
2. Packages of the tour including amenities and cost.
3. Customer can add interests (Food Experience, Things to do, etc.) to travel, likes, and dislike. The company will arrange the guide accordingly.
4. According to the interest choose plan.
5. Feature to select meet up point.
6. Specification listing of experience, confirmation policy, cancellation policy.
7. Recommendations for a similar experience.
8. Login with Facebook and Gmail.
9. Payment using Razorpay.
10. More highlights of wishlist, bookings, and profile.
