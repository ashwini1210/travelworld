//
//  CancelBookingModel.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 08/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import Alamofire

class CancelBookingModel: BaseModel {
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    /*
     * Method to hit on server for Cancel Booking
     */
    func requestForCancelBooking(data: [String:Any]?, identifier:String?){
        Alamofire.request(RestService.cancel_booking, method: .post, parameters: data!).responseJSON { response in switch response.result{
        case .success(let datas):

            var jsonData = JSON(datas)
            jsonData["booking_id"].stringValue =  data!["booking_id"] as! String
            
            //Store json response in a global varialble for future use
            self.responseJSONData = jsonData
            
            print("requestForCancelBooking: \(self.responseJSONData )")
            
            
        case .failure(let error):
            
            print("Error in CancelBookingModel \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: self.responseJSONData["status"].boolValue, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: identifier!)
        }
    }
}
