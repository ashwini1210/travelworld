import UIKit
import SwiftyJSON
import RealmSwift
import Alamofire

class UpdatePasswordModel: BaseModel {

    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    /*
     * Method to hit on server for Password
     */
    func requestForPassword(data: [String:Any]?){
        Alamofire.request(RestService.update_password, method: .post, parameters: data!).responseJSON { response in switch response.result{
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            print("requestForPassword: \(self.responseJSONData )")
            
            if self.responseJSONData["status"].boolValue{
              
            }
            
        case .failure(let error):
            
            print("Error in UpdatePasswordModel \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: self.responseJSONData["status"].boolValue, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: AppConstants.UPDATE_PASSWORD)
        }
    }
    
    
    
}
