//
//  TopDestinationModel.swift
//  LeamigoiOS
//
//  Created by Apple on 10/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class TopDestinationModel: BaseModel {

    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    
   
    func requestForTopDestination(){
        
        Alamofire.request(RestService.topDestination, method: .get, encoding: JSONEncoding.default).responseJSON { response in switch response.result {
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            if self.responseJSONData["status"].boolValue{
                self.parseTopDestinationJson(jsonData: self.responseJSONData)
            }
            
        case .failure(let error):
            
            print("Error in TopDestinationModel \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: response.result.isSuccess, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: AppConstants.TOP_DESTINATION)
        }
    }
    
    func parseTopDestinationJson(jsonData: JSON){
        autoreleasepool{
            let topDestinationDB = TopDestinationDB()
            if(topDestinationDB.findAll().count != 0){
                topDestinationDB.delete()
            }
            do {
                try topDestinationDB.realm.write {
                    let topDestinationDao = TopDestinationDao()
                    topDestinationDao.status = jsonData["status"].boolValue
                    topDestinationDao.message = jsonData["message"].stringValue
                    for imageData in jsonData["data"].arrayValue{
                        let imagesDao = DestinationListDao()
                        imagesDao.id = imageData["id"].stringValue
                        imagesDao.imageURL = "\(RestService.URL)"+"\(imageData["image"].stringValue)"
                        imagesDao.text = imageData["text"].stringValue
                        topDestinationDao.destinationListDao.append(imagesDao)
                    }
                    
                    topDestinationDB.realm.add(topDestinationDao, update: true)
                }
            }
            catch let error as NSError
            {
                print("Error in parseTopDestinationJson while saving data :\(error)")
            }
            
        }
    }
}
