//
//  AddBookingModel.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 11/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import Alamofire

class AddBookingModel: BaseModel {
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    func requestForAddBooking(data: [String:Any]?){
        print("url : \(RestService.add_booking)")
        print("Dict: \(data!)")
        Alamofire.request(RestService.add_booking, method: .post, parameters: data).responseJSON { response in switch response.result{
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            print("requestForAddBooking: \(self.responseJSONData )")
            if self.responseJSONData["status"].boolValue{
                self.parseBookingJson(jsonData: self.responseJSONData)
            }
            
        case .failure(let error):
            
            print("Error in AddBookingModel \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: self.responseJSONData["status"].boolValue, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: AppConstants.ADD_BOOKING)
        }
    }
    
    
    func parseBookingJson(jsonData: JSON){
        autoreleasepool{
            let addBookingDB = AddBookingDB()
            if(addBookingDB.findAll().count != 0){
                addBookingDB.delete()
            }
            do {
                try addBookingDB.realm.write {
                    let addBookingDao = AddBookingDao()
                    let bookingdict = jsonData["data"].dictionaryValue
                    
                    if((bookingdict["booking_date"]?.exists())!){
                        addBookingDao.booking_date = bookingdict["booking_date"]!.stringValue
                    }
                    if((bookingdict["pax"]?.exists())!){
                        addBookingDao.pax = bookingdict["pax"]!.stringValue
                    }
                    if((bookingdict["total_amount"]?.exists())!){
                        addBookingDao.total_amount = bookingdict["total_amount"]!.stringValue
                    }
                    if((bookingdict["booking_id"]?.exists())!){
                        addBookingDao.booking_id = bookingdict["booking_id"]!.stringValue
                    }
                    if((bookingdict["pax_rate"]?.exists())!){
                        addBookingDao.pax_rate = bookingdict["pax_rate"]!.stringValue
                    }
                    
                    //                    addBookingDao.exp_id = bookingdict["exp_id"]!.stringValue
                    //                   addBookingDao.booking_date = bookingdict["booking_date"]!.stringValue
                    //                    addBookingDao.booking_time = bookingdict["booking_time"]!.stringValue
                    //                    addBookingDao.child = bookingdict["child"]!.stringValue
                    //                    addBookingDao.adult = bookingdict["adult"]!.stringValue
                    //                    addBookingDao.product_id = bookingdict["product_id"]!.stringValue
                    //                    addBookingDao.vertical_id = bookingdict["vertical_id"]!.stringValue
                    //                    addBookingDao.city_id = bookingdict["city_id"]!.stringValue
                    //                    addBookingDao.currency_id = bookingdict["currency_id"]!.stringValue
                    //                    addBookingDao.base_price = bookingdict["base_price"]!.stringValue
                    //                    addBookingDao.duration_type = bookingdict["duration_type"]!.stringValue
                    //                    addBookingDao.total_amount = bookingdict["total_amount"]!.stringValue
                    
                    addBookingDB.realm.add(addBookingDao)
                }
            }
            catch let error as NSError
            {
                print("Error in parseBookingJson while saving data :\(error)")
            }
            
        }
    }
}
