//
//  FestivalDetailModel.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 22/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//


import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON

class FestivalDetailModel: BaseModel {
   // var majorAttractionArray = [JSON]()
    var includedArray = [JSON]()
    var notIncludedArray = [JSON]()
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    func requestForDetail(fevId : Int){
    
        let url = "\(RestService.fest_detail)\(fevId)"
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).responseJSON { response in switch response.result {
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            print("requestForDetail: \(self.responseJSONData)")
            
            if self.responseJSONData["status"].boolValue{
                self.parseDetailJson(jsonData: self.responseJSONData)
            }
            
        case .failure(let error):
            
            print("Error in DetailModel \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: response.result.isSuccess, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: AppConstants.DETAIL)
        }
    }
    
    func parseDetailJson(jsonData: JSON){
        autoreleasepool{
            let festivalDetailDB = FestivalDetailDB()
            if(festivalDetailDB.findAll().count != 0){
                festivalDetailDB.delete()
            }
            do {
                try festivalDetailDB.realm.write {
                    let detailDao = FestivalDetailDao()
                    let jsonDict : Dictionary? = jsonData["data"].dictionaryValue
                    if(jsonDict != nil){
                        
                        detailDao.id = jsonDict!["id"]!.stringValue
                        detailDao.product_id = jsonDict!["product_id"]!.stringValue
                         detailDao.festival_name = jsonDict!["festival_name"]!.stringValue
                        detailDao.city = jsonDict!["city"]!.stringValue
                        detailDao.tag_line = jsonDict!["tag_line"]!.stringValue
                        detailDao.city_id = jsonDict!["city_id"]!.stringValue
                        detailDao.desc = jsonDict!["desc"]!.stringValue
                        
                       // majorAttractionArray = jsonDict!["data"]!.arrayValue
                        includedArray = jsonDict!["included"]!.arrayValue
                        notIncludedArray = jsonDict!["not_included"]!.arrayValue
                        detailDao.about_amigo = jsonDict!["about_amigo"]!.stringValue
                        detailDao.duration_min = jsonDict!["duration_min"]!.stringValue
                        detailDao.duration_max = jsonDict!["duration_max"]!.stringValue
                        detailDao.duration_type = jsonDict!["duration_type"]!.stringValue
                        detailDao.min_guests = jsonDict!["min_guest"]!.stringValue
                        detailDao.max_guests = jsonDict!["max_guest"]!.stringValue
                        detailDao.base_rate = jsonDict!["base_rate"]!.stringValue
                        detailDao.base_currency_id = jsonDict!["base_currency_id"]!.stringValue
                        detailDao.confirmation_policy = jsonDict!["confirmation_policy"]!.stringValue
                        detailDao.cancellation_policy = jsonDict!["cancellation_policy"]!.stringValue
                        detailDao.pickup_point = jsonDict!["pick_up_point"]!.stringValue
                        detailDao.youtube_link = jsonDict!["youtube_link"]!.stringValue
                        detailDao.currency_sign = jsonDict!["currency_sign"]!.stringValue
                        detailDao.about_festival = jsonDict!["about_festival"]!.stringValue
                        detailDao.month = jsonDict!["month"]!.stringValue
                         detailDao.category_id = jsonDict!["category_id"]!.stringValue
                        
                        detailDao.day = jsonDict!["day"]!.stringValue
                        detailDao.is_top = jsonDict!["is_top"]!.stringValue
                        detailDao.status = jsonDict!["status"]!.stringValue
                        detailDao.card_image_url = "\(RestService.URL)"+"\(jsonDict!["card_image_url"]!.stringValue)"
                        detailDao.festival_id = jsonDict!["festival_id"]!.stringValue
                        detailDao.price_start = jsonDict!["price_start"]!.stringValue
                        
                        let imagesDaoList = jsonDict!["images"]!.arrayValue
                        for imagesDao in imagesDaoList{
                            let detailImagesDao = DetailImagesDao()
                            detailImagesDao.id = imagesDao["id"].stringValue
                            detailImagesDao.image_url = "\(RestService.URL)"+"\(imagesDao["image_url"].stringValue)"
                            detailDao.detailImagesDao.append(detailImagesDao)
                        }
                        
                        let similarDetailDaoList = jsonDict!["similar_festival"]!.arrayValue
                        for similarDetailvalue in similarDetailDaoList{
                            let similarDetailDao = SimilarDetailDao()
                            similarDetailDao.id = similarDetailvalue["id"].stringValue
                            similarDetailDao.heading = similarDetailvalue["festival_name"].stringValue
                            similarDetailDao.city = similarDetailvalue["city"].stringValue
                            similarDetailDao.country_id = similarDetailvalue["country_id"].stringValue
                            similarDetailDao.tag_line = similarDetailvalue["tag_line"].stringValue
                            similarDetailDao.price_start = similarDetailvalue["price_start"].stringValue
                            similarDetailDao.currency_id = similarDetailvalue["currency_id"].stringValue
                            similarDetailDao.status = similarDetailvalue["status"].stringValue
                            similarDetailDao.is_top = similarDetailvalue["is_top"].stringValue
                            similarDetailDao.card_image_url = "\(RestService.URL)"+"\(similarDetailvalue["card_image_url"].stringValue)"
                            similarDetailDao.country_name = similarDetailvalue["country_name"].stringValue
                            detailDao.similarDetailDao.append(similarDetailDao)
                        }
                    }
                    festivalDetailDB.realm.add(detailDao, update: true)
                }
            }
            catch let error as NSError
            {
                print("Error in parseDetailJson while saving data :\(error)")
            }
            
        }
    }
}
