//
//  WishlistModel.swift
//  LeamigoiOS
//
//  Created by Apple on 11/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import Alamofire

class WishlistModel: BaseModel {
    
    override init() {
        super.init()
    }
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    func requestToAddWishlist(wishlistData: [String:Any]?, identifier: String){
        
        Alamofire.request(RestService.wishlist, method: .post, parameters: wishlistData, encoding: JSONEncoding.default,headers: RestClient.getHeaders()).responseJSON { response in switch response.result {
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            print("Wishlist Response : \(self.responseJSONData)")
            
            if self.responseJSONData["status"].boolValue{
                self.parseWishlistJson(jsonData: self.responseJSONData, wishlistDict: wishlistData)
            }
            
        case .failure(let error):
            
            print("Error in TrendingActivityModel \(error.localizedDescription)")
            
            }
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: response.result.isSuccess, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: identifier)
        }
    }
    
    
    func parseWishlistJson(jsonData: JSON, wishlistDict : [String:Any]?){
        autoreleasepool{
            let wishlistDB = WishlistDB()
            do {
                try wishlistDB.realm.write {
                    let wishlistDao = WishlistDao()
                    wishlistDao.product_id = wishlistDict!["product_id"] as! String
                    wishlistDao.category_id = wishlistDict!["vertical_id"] as! String
                    wishlistDao.addedWishlist = true
                    wishlistDao.imagebase64 = wishlistDict!["imagebase64"] as! String
                    wishlistDB.realm.add(wishlistDao, update: true)
                }
            }
            catch let error as NSError
            {
                print("Error in parseWishlistJson while saving data :\(error)")
            }
            
        }
    }
    
}
