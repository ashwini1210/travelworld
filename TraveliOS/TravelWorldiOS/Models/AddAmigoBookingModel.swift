//
//  AmigoBooking.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 12/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import Alamofire

class AddAmigoBookingModel: BaseModel {
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    func requestForAddBooking(data: [String:Any]?){
        print("Dict: \(data!)")
        Alamofire.request(RestService.add_amigo_booking, method: .post, parameters: data!).responseJSON { response in switch response.result{
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            print("requestForAddBooking: \(self.responseJSONData )")
            if self.responseJSONData["status"].boolValue{
                self.parseBookingJson(jsonData: self.responseJSONData)
            }
            
        case .failure(let error):
            
            print("Error in AddAmigoBooking \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: self.responseJSONData["status"].boolValue, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: AppConstants.ADD_AMIGO_BOOKING)
        }
    }
    
    
    func parseBookingJson(jsonData: JSON){
        autoreleasepool{
            let addAmigoBookingDB = AddAmigoBookingDB()
            if(addAmigoBookingDB.findAll().count != 0){
                addAmigoBookingDB.delete()
            }
            do {
                try addAmigoBookingDB.realm.write {
                    let addAmigoBookingDao = AddAmigoBookingDao()
                    let bookingdict = jsonData["data"]["data"].dictionaryValue
                    if((bookingdict["pax"]?.exists())!){
                    addAmigoBookingDao.pax = bookingdict["pax"]!.stringValue
                    }
                    if((bookingdict["booking_id"]?.exists())!){
                    addAmigoBookingDao.booking_id = bookingdict["booking_id"]!.stringValue
                    }
                    if((bookingdict["pax_rate"]?.exists())!){
                    addAmigoBookingDao.pax_rate = bookingdict["pax_rate"]!.stringValue
                    }
                    if((bookingdict["total_amount"]?.exists())!){
                    addAmigoBookingDao.total_amount = bookingdict["total_amount"]!.stringValue
                    }
                    if((bookingdict["booking_date"]?.exists())!){
                    addAmigoBookingDao.booking_date = bookingdict["booking_date"]!.stringValue
                    }
                    addAmigoBookingDB.realm.add(addAmigoBookingDao)
                }
            }
            catch let error as NSError
            {
                print("Error in parseBookingJson while saving data :\(error)")
            }
            
        }
    }
}
