//
//  PlacesModel.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 18/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import Alamofire

class SearchExperienceModel: BaseModel {
    
    var searchExpArray = List<SearchExperiencePojo>()
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    /*
     * Method to hit on server for images
     */
    func requestForPlaces(searchChar: [String:Any]?){
        
        Alamofire.request(RestService.places, method: .post, parameters: searchChar!).responseJSON { response in switch response.result{
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            print("requestForPlaces: \(self.responseJSONData )")
            if self.responseJSONData["status"].boolValue{
                self.parsePlacesJson(jsonData: self.responseJSONData)
            }
            
        case .failure(let error):
            
            print("Error in SearchExperienceModel \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: response.result.isSuccess, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: AppConstants.PLACES)
        }
    }
    
    func parsePlacesJson(jsonData: JSON){
        autoreleasepool{
            if(searchExpArray.count != 0){
                searchExpArray.removeAll()
            }
            for experienceDaoList in jsonData["data"]["experiences"].arrayValue{
                let experienceListDao = SearchExperiencePojo()
                experienceListDao.id = experienceDaoList["id"].stringValue
                experienceListDao.heading = experienceDaoList["heading"].stringValue
                experienceListDao.city = experienceDaoList["city"].stringValue
                experienceListDao.countryId = experienceDaoList["country_id"].stringValue
                experienceListDao.countryName = experienceDaoList["country_name"].stringValue
                experienceListDao.tagLine = experienceDaoList["tag_line"].stringValue
                experienceListDao.priceStart = experienceDaoList["price_start"].stringValue
                experienceListDao.currencyId = experienceDaoList["currency_id"].stringValue
                experienceListDao.status = experienceDaoList["status"].stringValue
                experienceListDao.isTop = experienceDaoList["is_top"].stringValue
                experienceListDao.currencySign = experienceDaoList["currency_sign"].stringValue
                experienceListDao.currencyName = experienceDaoList["currency_name"].stringValue
                print("Rating:\(experienceDaoList["ratings"].stringValue)")
                experienceListDao.ratings = experienceDaoList["ratings"].stringValue
                experienceListDao.liked = experienceDaoList["liked"].stringValue
                experienceListDao.cardimageURL = "\(RestService.URL)"+"\(experienceDaoList["card_image_url"].stringValue)"
                experienceListDao.identifier = AppConstants.TOP_EXPERIENCE
                searchExpArray.append(experienceListDao)
            }
            for experienceDaoList in jsonData["data"]["festivals"].arrayValue{
                let experienceListDao = SearchExperiencePojo()
                experienceListDao.city = experienceDaoList["city"].stringValue
                experienceListDao.countryName = experienceDaoList["country_name"].stringValue
                experienceListDao.ratings = experienceDaoList["ratings"].stringValue
                experienceListDao.priceStart = experienceDaoList["price_start"].stringValue
                experienceListDao.currencyId = experienceDaoList["currency_id"].stringValue
                experienceListDao.tagLine = experienceDaoList["tag_line"].stringValue
                experienceListDao.id = experienceDaoList["id"].stringValue
                experienceListDao.cardimageURL = "\(RestService.URL)"+"\(experienceDaoList["card_image_url"].stringValue)"
                experienceListDao.heading = experienceDaoList["heading"].stringValue
                experienceListDao.currencySign = experienceDaoList["currency_sign"].stringValue
                experienceListDao.liked = experienceDaoList["liked"].stringValue
                experienceListDao.city_id = experienceDaoList["city_id"].stringValue
                experienceListDao.abbreviation = experienceDaoList["abbreviation"].stringValue
                experienceListDao.category_id = experienceDaoList["category_id"].stringValue
                experienceListDao.identifier = AppConstants.FESTIVAL
                //                experienceListDao.countryId = experienceDaoList["country_id"].stringValue
                //                experienceListDao.status = experienceDaoList["status"].stringValue
                //                experienceListDao.isTop = experienceDaoList["is_top"].stringValue
                //                experienceListDao.currencyName = experienceDaoList["currency_name"].stringValue
                searchExpArray.append(experienceListDao)
            }
        }
    }
}
