//
//  BlogsModel.swift
//  LeamigoiOS
//
//  Created by Apple on 11/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON

class BlogsModel: BaseModel {
    
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    func requestForBlogs(){
        
        Alamofire.request(RestService.blogs, method: .get, encoding: JSONEncoding.default).responseJSON { response in switch response.result {
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            if self.responseJSONData["status"].boolValue{
                self.parseBlogsJson(jsonData: self.responseJSONData)
            }
            
        case .failure(let error):
            
            print("Error in BlogsModel \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: response.result.isSuccess, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: AppConstants.BLOGS)
        }
    }
    
    
    func parseBlogsJson(jsonData: JSON){
        autoreleasepool{
            let blogsDB = BlogsDB()
            if(blogsDB.findAll().count != 0){
                blogsDB.delete()
            }
            do {
                try blogsDB.realm.write {
                    let blogsDao = BlogsDao()
                    if(blogsDao.blogListDao.count != 0){
                        blogsDao.blogListDao.removeAll()
                    }
                    blogsDao.status = jsonData["status"].boolValue
                    for blogsDaos in jsonData["data"]["data"].arrayValue{
                        let blogListDao = BlogListDao()
                        blogListDao.id = blogsDaos["id"].stringValue
                        blogListDao.title = blogsDaos["title"].stringValue
                        blogListDao.isFeatured = blogsDaos["is_featured"].stringValue
                        blogListDao.desc = blogsDaos["desc"].stringValue
                        blogListDao.writtenBy = blogsDaos["written_by"].stringValue
                        blogListDao.readTime = blogsDaos["read_time"].stringValue
                        blogListDao.cardimageURL = "\(RestService.URL)"+"\(blogsDaos["card_image_url"].stringValue)"
                        blogListDao.profileImageUrl = "\(RestService.URL)"+"\(blogsDaos["profile_image_url"].stringValue)"
                        
                        blogsDao.blogListDao.append(blogListDao)
                    }
                    
                    blogsDB.realm.add(blogsDao, update: true)
                }
            }
            catch let error as NSError
            {
                print("Error in parseFestivalJson while saving data :\(error)")
            }
            
        }
    }
}
