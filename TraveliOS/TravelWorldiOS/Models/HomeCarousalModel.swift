//
//  HomeCarousal.swift
//  LeamigoiOS
//
//  Created by Apple on 07/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire


class HomeCarousalModel: BaseModel {
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    /*
     * Method to hit on server for images
     */
    func requestForCarousal(){
        
        Alamofire.request(RestService.homeCarousal, method: .get, encoding: JSONEncoding.default).responseJSON { response in switch response.result {
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            if self.responseJSONData["status"].boolValue{
                self.parseCarousalJson(jsonData: self.responseJSONData)
            }
            
        case .failure(let error):
            
            print("Error in HomeCarousalModel \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: response.result.isSuccess, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: AppConstants.HOME_CAROUSAL)
        }
    }
    
    func parseCarousalJson(jsonData: JSON){
        autoreleasepool{
            let homeCarousalDB = HomeCarousalDB()
            if(homeCarousalDB.findAll().count != 0){
                homeCarousalDB.delete()
            }
            do {
                try homeCarousalDB.realm.write {
                    let homeCarousalDao = HomeCarousalDao()
                    // homeCarousalDao.id = HomeCarousalDao.getNextKey()
                    homeCarousalDao.status = jsonData["status"].boolValue
                    homeCarousalDao.message = jsonData["message"].stringValue
                    for imageData in jsonData["data"].arrayValue{
                        let imagesDao = ImagesDao()
                        imagesDao.imageURL = "\(RestService.URL)"+"\(imageData["image"].stringValue)"
                        imagesDao.text = imageData["text"].stringValue
                        homeCarousalDao.imagesDaoList.append(imagesDao)
                    }
                    
                    homeCarousalDB.realm.add(homeCarousalDao, update: true)
                }
            }
            catch let error as NSError
            {
                print("Error in parseCarousalJson while saving data :\(error)")
            }
            
        }
    }
}
