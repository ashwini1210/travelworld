//
//  EditProfileModel.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 10/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import Alamofire

class EditProfileModel: BaseModel {
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    /*
     * Method to hit on server for EditProfile
     */
    func requestForEditProfile(data: [String:String]?){
        print("Dict: \(data!)")
        Alamofire.request(RestService.edit_profile, method: .post, parameters: data!).responseJSON { response in switch response.result{
        case .success(let data1):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data1)
            
            print("requestForEditProfile: \(self.responseJSONData )")
            if self.responseJSONData["status"].boolValue{
                self.parseEditProfileJson(jsonData: self.responseJSONData, data: data)
            }
            
        case .failure(let error):
            
            print("Error in EditProfileModel \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: self.responseJSONData["status"].boolValue, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: AppConstants.EDIT_PROFILE)
        }
    }
    
    func parseEditProfileJson(jsonData: JSON,data : [String:String]?){
        autoreleasepool{
            if(data != nil){
            AppPreference.getInstance().setString(AppConstants.LOGIN_ID, value: data!["id"]!)
            AppPreference.getInstance().setString(AppConstants.FIRST_NAME, value: data!["first_name"]!)
            AppPreference.getInstance().setString(AppConstants.LAST_NAME, value: data!["last_name"]!)
            AppPreference.getInstance().setString(AppConstants.GENDER, value: data!["gender"]!)
            AppPreference.getInstance().setString(AppConstants.EMAIL, value: data!["email"]!)
            AppPreference.getInstance().setString(AppConstants.PHONE, value: data!["phone"]!)
            AppPreference.getInstance().setString(AppConstants.HOME_CITY, value: data!["home_city"]!)
            AppPreference.getInstance().setString(AppConstants.AGE, value: data!["age"]!)
            }
        }
    }
}
