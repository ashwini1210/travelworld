//
//  ExperienceModel.swift
//  LeamigoiOS
//
//  Created by Apple on 10/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ExperienceModel: BaseModel {

    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    
    func requestForExperience(){
        
        Alamofire.request(RestService.topExperience, method: .get, encoding: JSONEncoding.default).responseJSON { response in switch response.result {
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            if self.responseJSONData["status"].boolValue{
                self.parseTopExperienceJson(jsonData: self.responseJSONData)
            }
            
        case .failure(let error):
            
            print("Error in ExperienceModel \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: response.result.isSuccess, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: AppConstants.TOP_EXPERIENCE)
        }
    }

    
    func parseTopExperienceJson(jsonData: JSON){
        autoreleasepool{
            let topExperienceDB = TopExperienceDB()
            if(topExperienceDB.findAll().count != 0){
                topExperienceDB.delete()
            }
            
            do {
                try topExperienceDB.realm.write {
                    let topExperienceDao = TopExperienceDao()
                    topExperienceDao.status = jsonData["status"].boolValue
                    for experienceDaoList in jsonData["data"]["data"].arrayValue{
                        let experienceListDao = ExperienceListDao()
                        experienceListDao.id = experienceDaoList["id"].stringValue
                        experienceListDao.heading = experienceDaoList["heading"].stringValue
                        experienceListDao.slug = experienceDaoList["slug"].stringValue
                        experienceListDao.city = experienceDaoList["city"].stringValue
                        experienceListDao.countryId = experienceDaoList["country_id"].stringValue
                        experienceListDao.tagLine = experienceDaoList["tag_line"].stringValue
                        experienceListDao.priceStart = experienceDaoList["price_start"].stringValue
                        experienceListDao.currencyId = experienceDaoList["currency_id"].stringValue
                        experienceListDao.status = experienceDaoList["status"].stringValue
                        experienceListDao.isTop = experienceDaoList["is_top"].stringValue
                        experienceListDao.currencySign = experienceDaoList["currency_sign"].stringValue
                        experienceListDao.currencyName = experienceDaoList["currency_name"].stringValue
                        experienceListDao.abbreviation = experienceDaoList["abbreviation"].stringValue
                        experienceListDao.ratings = experienceDaoList["ratings"].stringValue
                        experienceListDao.countryName = experienceDaoList["country_name"].stringValue
                        experienceListDao.liked = experienceDaoList["liked"].stringValue
                        experienceListDao.cardimageURL = "\(RestService.URL)"+"\(experienceDaoList["card_image_url"].stringValue)"
                        topExperienceDao.experienceListDao.append(experienceListDao)
                    }
                    
                    topExperienceDB.realm.add(topExperienceDao, update: true)
                }
            }
            catch let error as NSError
            {
                print("Error in parseTopDestinationJson while saving data :\(error)")
            }
            
        }
    }
}
