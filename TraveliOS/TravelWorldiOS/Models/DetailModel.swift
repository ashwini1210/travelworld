//
//  DetailModel.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 13/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON

class DetailModel: BaseModel {
    
    var majorAttractionArray = [JSON]()
    var includedArray = [JSON]()
    var notIncludedArray = [JSON]()
    var languageArray = [JSON]()
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    func requestForDetail(expId : Int){
        
        let  url = "\(RestService.detail)\(expId)"
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).responseJSON { response in switch response.result {
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            print("requestForDetail: \(self.responseJSONData)")
            
            if self.responseJSONData["status"].boolValue{
                self.parseDetailJson(jsonData: self.responseJSONData)
            }
            
        case .failure(let error):
            
            print("Error in DetailModel \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: response.result.isSuccess, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: AppConstants.DETAIL)
        }
    }
    
    func parseDetailJson(jsonData: JSON){
        autoreleasepool{
            let detailDB = DetailDB()
            if(detailDB.findAll().count != 0){
                detailDB.delete()
            }
            do {
                try detailDB.realm.write {
                    let detailDao = DetailDao()
                    let jsonDict : Dictionary? = jsonData["data"].dictionaryValue
                    if(jsonDict != nil){
                        detailDao.city = jsonDict!["data"]!["city"].stringValue
                        detailDao.heading = jsonDict!["data"]!["heading"].stringValue
                        detailDao.slug = jsonDict!["data"]!["slug"].stringValue
                        detailDao.tag_line = jsonDict!["data"]!["tag_line"].stringValue
                        detailDao.city_id = jsonDict!["data"]!["city_id"].stringValue
                        detailDao.category_id = jsonDict!["data"]!["category_id"].stringValue
                        detailDao.desc = jsonDict!["data"]!["desc"].stringValue
                        
                        majorAttractionArray = jsonDict!["data"]!["major_attraction"].arrayValue
                        includedArray = jsonDict!["data"]!["included"].arrayValue
                        notIncludedArray = jsonDict!["data"]!["not_included"].arrayValue
                        languageArray = jsonDict!["data"]!["lanuages"].arrayValue
                        
                        detailDao.about_amigo = jsonDict!["data"]!["about_amigo"].stringValue
                        detailDao.duration_min = jsonDict!["data"]!["duration_min"].stringValue
                        detailDao.duration_max = jsonDict!["data"]!["duration_max"].stringValue
                        detailDao.duration_type = jsonDict!["data"]!["duration_type"].stringValue
                        detailDao.min_guests = jsonDict!["data"]!["min_guests"].stringValue
                        detailDao.max_guests = jsonDict!["data"]!["max_guests"].stringValue
                        detailDao.base_rate = jsonDict!["data"]!["base_rate"].stringValue
                        detailDao.base_currency_id = jsonDict!["data"]!["base_currency_id"].stringValue
                        detailDao.confirmation_policy = jsonDict!["data"]!["confirmation_policy"].stringValue
                        detailDao.cancellation_policy = jsonDict!["data"]!["cancellation_policy"].stringValue
                        detailDao.pickup_point = jsonDict!["data"]!["pickup_point"].stringValue
                        detailDao.mode_of_transport = jsonDict!["data"]!["mode_of_transport"].stringValue
                        detailDao.currency_sign = jsonDict!["data"]!["currency_sign"].stringValue
                        detailDao.abbreviation = jsonDict!["data"]!["abbreviation"].stringValue
                        detailDao.category_name = jsonDict!["data"]!["category_name"].stringValue
                        
                        let imagesDaoList = jsonDict!["data"]!["images"].arrayValue
                        for imagesDao in imagesDaoList{
                            let detailImagesDao = DetailImagesDao()
                            detailImagesDao.id = imagesDao["id"].stringValue
                            detailImagesDao.image_url = "\(RestService.URL)"+"\(imagesDao["image_url"].stringValue)"
                            detailDao.detailImagesDao.append(detailImagesDao)
                        }
                        
                        let similarDetailDaoList = jsonDict!["data"]!["similar_exp"].arrayValue
                        for similarDetailvalue in similarDetailDaoList{
                            let similarDetailDao = SimilarDetailDao()
                            similarDetailDao.id = similarDetailvalue["id"].stringValue
                            similarDetailDao.slug = similarDetailvalue["slug"].stringValue
                            similarDetailDao.heading = similarDetailvalue["heading"].stringValue
                            similarDetailDao.city = similarDetailvalue["city"].stringValue
                            similarDetailDao.country_id = similarDetailvalue["country_id"].stringValue
                            similarDetailDao.tag_line = similarDetailvalue["tag_line"].stringValue
                            similarDetailDao.price_start = similarDetailvalue["price_start"].stringValue
                            similarDetailDao.currency_id = similarDetailvalue["currency_id"].stringValue
                            similarDetailDao.status = similarDetailvalue["status"].stringValue
                            similarDetailDao.is_top = similarDetailvalue["is_top"].stringValue
                            similarDetailDao.card_image_url = "\(RestService.URL)"+"\(similarDetailvalue["card_image_url"].stringValue)"
                            similarDetailDao.country_name = similarDetailvalue["country_name"].stringValue
                            detailDao.similarDetailDao.append(similarDetailDao)
                        }
                    }
                    detailDB.realm.add(detailDao, update: true)
                }
            }
            catch let error as NSError
            {
                print("Error in parseDetailJson while saving data :\(error)")
            }
            
        }
    }
}
