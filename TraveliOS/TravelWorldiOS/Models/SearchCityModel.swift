//
//  SearchCityModel.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 09/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import Alamofire

class SearchCityModel: BaseModel {
    
    var SearchCityPojoArray = [SearchCityPojo]()
    var cityArray = [String]()
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    /*
     * Method to hit on server for images
     */
    func requestForCity(searchChar: [String:Any]?){
        print("searchChar: \(searchChar!)")
        Alamofire.request(RestService.city, method: .post, parameters: searchChar!).responseJSON { response in switch response.result{
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            print("requestForCity: \(self.responseJSONData )")
            self.parsePlacesJson(jsonData: self.responseJSONData)
            
        case .failure(let error):
            
            print("Error in SearchCityModel \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: response.result.isSuccess, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: AppConstants.PLACES)
        }
    }
    
    func parsePlacesJson(jsonData: JSON){
        autoreleasepool{
            if(cityArray.count != 0){
             cityArray.removeAll()
            }
            for data in jsonData.arrayValue{
                let searchCityPojo = SearchCityPojo()
                searchCityPojo.title = data["title"].stringValue
                searchCityPojo.flag_url = data["flag_url"].stringValue
                searchCityPojo.cities_id = data["cities_id"].stringValue
                searchCityPojo.asci_code = data["asci_code"].stringValue
                searchCityPojo.country_name = data["country_name"].stringValue
                searchCityPojo.country_id = data["country_id"].stringValue
                searchCityPojo.nick_name = data["nick_name"].stringValue
                SearchCityPojoArray.append(searchCityPojo)
                
                let str = "\(data["title"].stringValue), \(data["country_name"].stringValue)"
                cityArray.append(str)
            }
        }
    }
}
