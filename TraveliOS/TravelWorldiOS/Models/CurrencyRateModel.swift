//
//  CurrencyRateModel.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 15/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON

class CurrencyRateModel: BaseModel {
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    /*
     * Method to hit on server for Currency Rate
     */
    func requestForCurrencyRate(){
        Alamofire.request(RestService.currency_rate, method: .get, encoding: JSONEncoding.default).responseJSON { response in switch response.result{
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            if self.responseJSONData["status"].boolValue{
                AppPreference.getInstance().setInt(AppConstants.CURRENCY_RATE, value:  self.responseJSONData["currency_rate"].intValue)
            }
            
        case .failure(let error):
            
            print("Error in CurrencyRateModel \(error.localizedDescription)")
            
            }
        }
    }
    
    
}
