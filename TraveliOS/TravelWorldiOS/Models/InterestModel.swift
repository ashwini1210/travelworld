//
//  InterestModel.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 19/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//


import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON

class InterestModel: BaseModel {
    
    var interest = [String]()
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    func requestForInterest(){
        
        Alamofire.request(RestService.interest, method: .get, encoding: JSONEncoding.default).responseJSON { response in switch response.result {
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            if self.responseJSONData["status"].boolValue{
                for item in self.responseJSONData["data"].arrayValue{
                    print(item)
                    self.interest.append(item["interest"].stringValue)
                }
            }
            
        case .failure(let error):
            
            print("Error in INTERESTMODEL \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: response.result.isSuccess, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: AppConstants.INTEREST)
        }
    }
}
