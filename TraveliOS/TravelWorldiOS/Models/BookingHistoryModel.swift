import UIKit
import SwiftyJSON
import RealmSwift
import Alamofire

class BookingHistoryModel: BaseModel {
    
     let currencyRate = AppPreference.getInstance().getInt(AppConstants.CURRENCY_RATE, defaultvalue: 0)
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    /*
     * Method to hit on server for booking
     */
    func requestForBooking(identifier: String){
        Alamofire.request(RestService.bookingHistory, method: .get, encoding: JSONEncoding.default).responseJSON { response in switch response.result{
        case .success(let data):
            print(RestService.bookingHistory)
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            print("requestForBooking: \(self.responseJSONData )")
            
            if self.responseJSONData["status"].boolValue{
                self.parseBookingJson(jsonData: self.responseJSONData)
            }
            
        case .failure(let error):
            
            print("Error in BookingHistoryModel \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: self.responseJSONData["status"].boolValue, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: identifier)
        }
    }
    
    
    func parseBookingJson(jsonData: JSON){
        autoreleasepool{
            let bookingDB = BookingDB()
            if(bookingDB.findAll().count != 0){
                bookingDB.delete()
            }
            do {
                try bookingDB.realm.write {
                    if(jsonData["status"].boolValue){
                        let bookingDao = BookingDao()
                        for bookingDaoList in jsonData["data"]["booking_history"].arrayValue{
                            let bookingListDao = BookingListDao()
                            bookingListDao.product_id = bookingDaoList["product_id"].stringValue
                            bookingListDao.duration = bookingDaoList["duration"].stringValue
                            bookingListDao.vertical_id = bookingDaoList["vertical_id"].stringValue
                            bookingListDao.assigned_to = bookingDaoList["assigned_to"].stringValue
                            bookingListDao.booking_id = bookingDaoList["booking_id"].stringValue
                            bookingListDao.booking_date = bookingDaoList["booking_date"].stringValue
                            bookingListDao.booking_time = bookingDaoList["booking_time"].stringValue
                            bookingListDao.meetup_point = bookingDaoList["meetup_point"].stringValue
                            bookingListDao.payment_status =  getPaymentStatus(status: bookingDaoList["payment_status"].stringValue)
                            bookingListDao.pax = bookingDaoList["pax"].stringValue
                            bookingListDao.amigo_status = getAmigoStatus(status: bookingDaoList["amigo_status"].stringValue)
                            bookingListDao.total_amount = bookingDaoList["total_amount"].stringValue
                            bookingListDao.first_name = bookingDaoList["first_name"].stringValue
                            bookingListDao.last_name = bookingDaoList["last_name"].stringValue
                            bookingListDao.gender = bookingDaoList["gender"].stringValue
                            bookingListDao.image_url = bookingDaoList["image_url"].stringValue
                            bookingListDao.interests = bookingDaoList["interests"].stringValue
                            bookingListDao.city_name = bookingDaoList["city_name"].stringValue
                            if(bookingDaoList["experience_name"].exists()){
                                bookingListDao.experience_name = bookingDaoList["experience_name"].stringValue
                            }
                            if(bookingDaoList["card_image_url"].exists()){
                                bookingListDao.card_image_url = "\(RestService.URL)"+"\(bookingDaoList["card_image_url"].stringValue)"
                            }
                            if(bookingDaoList["amigo_name"].exists()){
                                bookingListDao.amigo_name = bookingDaoList["amigo_name"].stringValue
                            }
                            bookingDao.bookingHistoryDao.append(bookingListDao)
                        }
                        for bookingDaoList in jsonData["data"]["upcoming_bookings"].arrayValue{
                            let bookingListDao = BookingListDao()
                            bookingListDao.product_id = bookingDaoList["product_id"].stringValue
                            bookingListDao.duration = bookingDaoList["duration"].stringValue
                            bookingListDao.vertical_id = bookingDaoList["vertical_id"].stringValue
                            bookingListDao.assigned_to = bookingDaoList["assigned_to"].stringValue
                            bookingListDao.booking_id = bookingDaoList["booking_id"].stringValue
                            bookingListDao.booking_date = bookingDaoList["booking_date"].stringValue
                            bookingListDao.booking_time = bookingDaoList["booking_time"].stringValue
                            bookingListDao.meetup_point = bookingDaoList["meetup_point"].stringValue
                            bookingListDao.payment_status = getPaymentStatus(status: bookingDaoList["payment_status"].stringValue)
                            bookingListDao.pax = bookingDaoList["pax"].stringValue
                            bookingListDao.amigo_status = getAmigoStatus(status: bookingDaoList["amigo_status"].stringValue)
                            bookingListDao.total_amount = bookingDaoList["total_amount"].stringValue
                            bookingListDao.first_name = bookingDaoList["first_name"].stringValue
                            bookingListDao.last_name = bookingDaoList["last_name"].stringValue
                            bookingListDao.gender = bookingDaoList["gender"].stringValue
                            bookingListDao.image_url = bookingDaoList["image_url"].stringValue
                            bookingListDao.interests = bookingDaoList["interests"].stringValue
                            bookingListDao.city_name = bookingDaoList["city_name"].stringValue
                            if(bookingDaoList["experience_name"].exists()){
                                bookingListDao.experience_name = bookingDaoList["experience_name"].stringValue
                            }
                            if(bookingDaoList["card_image_url"].exists()){
                                bookingListDao.card_image_url = "\(RestService.URL)"+"\(bookingDaoList["card_image_url"].stringValue)"
                            }
                            if(bookingDaoList["amigo_name"].exists()){
                                bookingListDao.amigo_name = bookingDaoList["amigo_name"].stringValue
                            }
                            bookingDao.bookingUpcomingDao.append(bookingListDao)
                        }
                        bookingDB.realm.add(bookingDao, update: true)
                    }
                }
            }catch let error as NSError
            {
                print("Error in parseFestivalJson while saving data :\(error)")
            }
        }
    }
    
    
    func getPaymentStatus(status: String) -> String{
        var str = ""
        if(status == "2"){
            str = "Booked & Paid"
        }else{
            str = "Booked"
        }
        return str
    }
    
    func getAmigoStatus(status: String) -> String{
        var str = ""
        if(status == "5"){
           str = "Refund in process"
        }
        else if(status == "6"){
            str = "Refund done"
        }
        else if(status == "3"){
            str = "Completed"
        }
        else {
            str = "Cancelled"
        }
        return str
    }
}
