//
//  CityCountryModel.swift
//  LeamigoiOS
//
//  Created by Apple on 14/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//
import UIKit
import SwiftyJSON
import RealmSwift
import Alamofire

class CityCountryModel: BaseModel {
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    func requestForCityCountryData(){
        
        Alamofire.request(RestService.cityCountry, method: .get, encoding: JSONEncoding.default).responseJSON { response in switch response.result {
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            if self.responseJSONData["status"].boolValue{
                self.parseCityCountryJson(jsonData: self.responseJSONData)
            }
            
        case .failure(let error):
            
            print("Error in BlogsModel \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: response.result.isSuccess, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: AppConstants.CITY_COUNTRY)
        }
    }
    
    
    func parseCityCountryJson(jsonData: JSON){
        autoreleasepool{
            let countryDB = CountryDB()
            if(countryDB.findAll().count != 0){
                countryDB.delete()
            }
            do {
                try countryDB.realm.write {
                    let countryDao = CountryDao()
                    countryDao.status = jsonData["status"].boolValue
                    
                    for cityDaoObj in jsonData["data"]["Australia"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.AustraliacityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Austria"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.AustriacityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Belgium"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.BelgiumcityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Cambodia"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.CambodiacityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["England"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.EnglandcityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["France"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.FrancecityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Germany"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.GermanycityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Greece"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.GreececityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Hong Kong"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.HongKongcityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Hungary"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.HungarycityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["India"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.IndiacityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Indonesia"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.IndonesiacityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Italy"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.ItalycityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Japan"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.JapancityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Malaysia"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.MalaysiacityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Mexico"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.MexicocityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Netherlands"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.NetherlandscityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["New Zealand"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.NewZealandcityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Portugal"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.PortugalcityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Russia"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.RussiacityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Singapore"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.SingaporecityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Spain"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.SpaincityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Sri Lanka"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.SriLankacityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Sweden"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.SwedencityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Thailand"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.ThailandcityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Turkey"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.TurkeycityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["USA"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.USAcityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["United Arab Emirates"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.UnitedArabEmiratescityDaoList.append(cityDao!)
                        }
                    }
                    for cityDaoObj in jsonData["data"]["Vietnam"].arrayValue{
                        let cityDao = parseCityJson(cityDaoObj: cityDaoObj)
                        if(cityDao != nil){
                            countryDao.VietnamcityDaoList.append(cityDao!)
                        }
                    }
                    countryDB.realm.add(countryDao, update: true)
                }
            }
            catch let error as NSError
            {
                print("Error in parseCityCountryJson while saving data :\(error)")
            }
            
        }
    }
    
    func parseCityJson(cityDaoObj : JSON) -> CityDao?{
        let cityDao = CityDao()
        cityDao.city_id = cityDaoObj["city_id"].stringValue
        cityDao.city_name = cityDaoObj["city_name"].stringValue
        cityDao.country_name = cityDaoObj["country_name"].stringValue
        cityDao.flag_url = cityDaoObj["flag_url"].stringValue
        cityDao.slug = cityDaoObj["slug"].stringValue
        return cityDao
    }
    
    func removeListData(countryDao : CountryDao){
        if(countryDao.AustraliacityDaoList.count != 0){
            countryDao.AustraliacityDaoList.removeAll()
        }
        if(countryDao.AustriacityDaoList.count != 0){
            countryDao.AustriacityDaoList.removeAll()
        }
        if(countryDao.BelgiumcityDaoList.count != 0){
            countryDao.BelgiumcityDaoList.removeAll()
        }
        if(countryDao.CambodiacityDaoList.count != 0){
            countryDao.CambodiacityDaoList.removeAll()
        }
        if(countryDao.EnglandcityDaoList.count != 0){
            countryDao.EnglandcityDaoList.removeAll()
        }
        if(countryDao.FrancecityDaoList.count != 0){
            countryDao.FrancecityDaoList.removeAll()
        }
        if(countryDao.GermanycityDaoList.count != 0){
            countryDao.GermanycityDaoList.removeAll()
        }
        if(countryDao.HongKongcityDaoList.count != 0){
            countryDao.HongKongcityDaoList.removeAll()
        }
        if(countryDao.HungarycityDaoList.count != 0){
            countryDao.HungarycityDaoList.removeAll()
        }
        if(countryDao.IndiacityDaoList.count != 0){
            countryDao.IndiacityDaoList.removeAll()
        }
        if(countryDao.IndonesiacityDaoList.count != 0){
            countryDao.IndonesiacityDaoList.removeAll()
        }
        if(countryDao.ItalycityDaoList.count != 0){
            countryDao.ItalycityDaoList.removeAll()
        }
        if(countryDao.JapancityDaoList.count != 0){
            countryDao.JapancityDaoList.removeAll()
        }
        if(countryDao.MalaysiacityDaoList.count != 0){
            countryDao.MalaysiacityDaoList.removeAll()
        }
        if(countryDao.MexicocityDaoList.count != 0){
            countryDao.MexicocityDaoList.removeAll()
        }
        if(countryDao.NetherlandscityDaoList.count != 0){
            countryDao.NetherlandscityDaoList.removeAll()
        }
        if(countryDao.NewZealandcityDaoList.count != 0){
            countryDao.NewZealandcityDaoList.removeAll()
        }
        if(countryDao.PortugalcityDaoList.count != 0){
            countryDao.PortugalcityDaoList.removeAll()
        }
        if(countryDao.RussiacityDaoList.count != 0){
            countryDao.RussiacityDaoList.removeAll()
        }
        if(countryDao.SingaporecityDaoList.count != 0){
            countryDao.SingaporecityDaoList.removeAll()
        }
        if(countryDao.SpaincityDaoList.count != 0){
            countryDao.SpaincityDaoList.removeAll()
        }
        if(countryDao.SriLankacityDaoList.count != 0){
            countryDao.SriLankacityDaoList.removeAll()
        }
        if(countryDao.SwedencityDaoList.count != 0){
            countryDao.SwedencityDaoList.removeAll()
        }
        if(countryDao.ThailandcityDaoList.count != 0){
            countryDao.ThailandcityDaoList.removeAll()
        }
        if(countryDao.TurkeycityDaoList.count != 0){
            countryDao.TurkeycityDaoList.removeAll()
        }
        if(countryDao.USAcityDaoList.count != 0){
            countryDao.USAcityDaoList.removeAll()
        }
        if(countryDao.UnitedArabEmiratescityDaoList.count != 0){
            countryDao.UnitedArabEmiratescityDaoList.removeAll()
        }
        if(countryDao.VietnamcityDaoList.count != 0){
            countryDao.VietnamcityDaoList.removeAll()
        }
    }
}
