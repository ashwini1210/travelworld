//
//  ForgotPasswordModel.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 10/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//


import UIKit
import SwiftyJSON
import RealmSwift
import Alamofire

class ForgotPasswordModel: BaseModel {
    override init() {
        super.init()
    }
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    /*
     * Method to hit on server for ForgotPassword
     */
    func requestForForgotPassword(data: [String:String]?){
        Alamofire.request(RestService.forgot_password, method: .post, parameters: data!).responseJSON { response in switch response.result{
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            print("requestForForgotPassword: \(self.responseJSONData )")
            
        case .failure(let error):
            print("Error in ForgotPasswordModel \(error.localizedDescription)")
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: self.responseJSONData["status"].boolValue, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: AppConstants.FORGOT_PASSWORD)
        }
    }
}
