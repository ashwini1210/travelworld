//
//  OffersModel.swift
//  LeamigoiOS
//
//  Created by Apple on 11/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON


class OffersModel: BaseModel {
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    func requestForOffers(){
        
        Alamofire.request(RestService.offers, method: .get, encoding: JSONEncoding.default).responseJSON { response in switch response.result {
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            if self.responseJSONData["status"].boolValue{
                self.parseOffersJson(jsonData: self.responseJSONData)
            }
            
        case .failure(let error):
            
            print("Error in TrendingActivityModel \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: response.result.isSuccess, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: AppConstants.OFFERS)
        }
    }
    
    func parseOffersJson(jsonData: JSON){
        autoreleasepool{
            let offersDB = OffersDB()
            if(offersDB.findAll().count != 0){
                offersDB.delete()
            }
            do {
                try offersDB.realm.write {
                    let offersDAO = OffersDao()
                    if(offersDAO.offersListDao.count != 0){
                        offersDAO.offersListDao.removeAll()
                    }
                    offersDAO.status = jsonData["status"].boolValue
                    offersDAO.message = jsonData["message"].stringValue
                    for offersDao in jsonData["data"].arrayValue{
                        let offersListDao = OffersListDao()
                        offersListDao.id = offersDao["id"].stringValue
                        offersListDao.name = offersDao["name"].stringValue
                        offersListDao.desc = offersDao["desc"].stringValue
                        offersListDao.usage_type = offersDao["usage_type"].stringValue
                        offersListDao.value = offersDao["value"].stringValue
                        offersListDao.value_type = offersDao["value_type"].stringValue
                        offersListDao.status = offersDao["status"].stringValue
                        offersListDao.added_on = offersDao["added_on"].stringValue
                        offersListDao.added_by = offersDao["added_by"].stringValue
                        offersListDao.cardImage = "\(RestService.URL)"+"\(offersDao["card_image_url"].stringValue)"
                        offersListDao.updated_on = offersDao["updated_on"].stringValue
                        offersListDao.updated_by = offersDao["updated_by"].stringValue
                        offersDAO.offersListDao.append(offersListDao)
                    }
                    offersDB.realm.add(offersDAO, update: true)
                }
            }
            catch let error as NSError
            {
                print("Error in parseFestivalJson while saving data :\(error)")
            }
            
        }
    }
}
