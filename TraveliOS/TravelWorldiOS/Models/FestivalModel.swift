//
//  FestivalModel.swift
//  LeamigoiOS
//
//  Created by Apple on 10/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON

class FestivalModel: BaseModel {
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    func requestForFestival(){
        
        Alamofire.request(RestService.festivals, method: .get, encoding: JSONEncoding.default).responseJSON { response in switch response.result {
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            if self.responseJSONData["status"].boolValue{
                self.parseFestivalJson(jsonData: self.responseJSONData)
            }
            
        case .failure(let error):
            
            print("Error in FestivalModel \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: response.result.isSuccess, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: AppConstants.FESTIVAL)
        }
    }
    
    func parseFestivalJson(jsonData: JSON){
        autoreleasepool{
            let festivalDB = FestivalDB()
            if(festivalDB.findAll().count != 0){
                festivalDB.delete()
            }
            do {
                try festivalDB.realm.write {
                    let festivalDao = FestivalDao()
                    if(festivalDao.festivalListDao.count != 0){
                        festivalDao.festivalListDao.removeAll()
                    }
                    festivalDao.status = jsonData["status"].boolValue
                    for festivalsDao in jsonData["data"].arrayValue{
                        let festivalListDao = FestivalListDao()
                        festivalListDao.month = festivalsDao["month"].stringValue
                        festivalListDao.day = festivalsDao["day"].stringValue
                        festivalListDao.festivalName = festivalsDao["festival_name"].stringValue
                        festivalListDao.id = festivalsDao["id"].stringValue
                        festivalListDao.tagLine = festivalsDao["tag_line"].stringValue
                        festivalListDao.cityId = festivalsDao["city_id"].stringValue
                        festivalListDao.priceStart = festivalsDao["price_start"].stringValue
                        festivalListDao.currencyId = festivalsDao["currency_id"].stringValue
                        festivalListDao.cardimageURL = "\(RestService.URL)"+"\(festivalsDao["card_image_url"].stringValue)"
                        festivalListDao.categoryId = festivalsDao["category_id"].stringValue
                        festivalListDao.currencySign = festivalsDao["currency_sign"].stringValue
                        festivalListDao.cityName = festivalsDao["city_name"].stringValue
                        festivalListDao.abbreviation = festivalsDao["abbreviation"].stringValue
                        festivalListDao.ratings = festivalsDao["ratings"].stringValue
                        festivalListDao.countryName = festivalsDao["country_name"].stringValue
                        festivalListDao.liked = festivalsDao["liked"].stringValue
                        festivalDao.festivalListDao.append(festivalListDao)
                    }
                    
                    festivalDB.realm.add(festivalDao, update: true)
                }
            }
            catch let error as NSError
            {
                print("Error in parseFestivalJson while saving data :\(error)")
            }
            
        }
    }
}
