//
//  TrendingActivityModel.swift
//  LeamigoiOS
//
//  Created by Apple on 11/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON

class TrendingActivityModel: BaseModel {
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    func requestForTrendingActivity(){
        
        Alamofire.request(RestService.trendingACtivity, method: .get, encoding: JSONEncoding.default).responseJSON { response in switch response.result {
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            if self.responseJSONData["status"].boolValue{
                self.parseActivityJson(jsonData: self.responseJSONData)
            }
            
        case .failure(let error):
            
            print("Error in TrendingActivityModel \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: response.result.isSuccess, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: AppConstants.TRENDING_ACTIVITY)
        }
    }
    
    
    func parseActivityJson(jsonData: JSON){
        autoreleasepool{
            let activityDB = ActivityDB()
            if(activityDB.findAll().count != 0){
                activityDB.delete()
            }
            do {
                try activityDB.realm.write {
                    let activityDAO = ActivityDao()
                    if(activityDAO.activityListDao.count != 0){
                        activityDAO.activityListDao.removeAll()
                    }
                    activityDAO.status = jsonData["status"].boolValue
                    for activityDao in jsonData["data"]["data"].arrayValue{
                        let activityListDao = ActivityListDao()
                        activityListDao.id = activityDao["id"].stringValue
                        activityListDao.heading = activityDao["heading"].stringValue
                        activityListDao.cityName = activityDao["city"].stringValue
                        activityListDao.countryId = activityDao["country_id"].stringValue
                        activityListDao.tagLine = activityDao["tag_line"].stringValue
                        activityListDao.priceStart = activityDao["price_start"].stringValue
                        activityListDao.currencyId = activityDao["currency_id"].stringValue
                        activityListDao.priceStart = activityDao["status"].stringValue
                        activityListDao.isTop = activityDao["is_top"].stringValue
                        activityListDao.cardImage = "\(RestService.URL)"+"\(activityDao["card_image_url"].stringValue)"
                        activityListDao.currencySign = activityDao["currency_sign"].stringValue
                        activityListDao.currencyName = activityDao["currency_name"].stringValue
                        activityListDao.abbreviation = activityDao["abbreviation"].stringValue
                        activityListDao.ratings = activityDao["ratings"].stringValue
                        activityListDao.countryName = activityDao["country_name"].stringValue
                        activityListDao.liked = activityDao["liked"].stringValue
                        activityDAO.activityListDao.append(activityListDao)
                    }                    
                    activityDB.realm.add(activityDAO, update: true)
                }
            }
            catch let error as NSError
            {
                print("Error in parseFestivalJson while saving data :\(error)")
            }
            
        }
    }
}
