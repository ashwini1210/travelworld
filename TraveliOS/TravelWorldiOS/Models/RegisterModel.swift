
import UIKit
import SwiftyJSON
import RealmSwift
import Alamofire

class RegisterModel: BaseModel {
    
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    /*
     * Method to hit on server for signup
     */
    func requestForReagister(data: [String:Any]?){
        print("Dict: \(data!)")
        Alamofire.request(RestService.signup, method: .post, parameters: data!).responseJSON { response in switch response.result{
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            
            print("requestForReagister: \(self.responseJSONData )")
            if self.responseJSONData["status"].boolValue{
                self.parseRegisterJson(jsonData: self.responseJSONData)
            }
            
        case .failure(let error):
            
            print("Error in RegisterModel \(error.localizedDescription)")
            
            }
            
            //Callback in viewcontroller for further functionality
            self.delegate.onHttpResponse(isSuccess: self.responseJSONData["status"].boolValue, responseJson: self.responseJSONData, error: response.result.error as AnyObject, anyIdentifier: AppConstants.SIGNUP)
        }
    }
    
    func parseRegisterJson(jsonData: JSON){
        autoreleasepool{
            AppPreference.getInstance().setString(AppConstants.LOGIN_ID, value: jsonData["data"]["id"].stringValue)
            AppPreference.getInstance().setString(AppConstants.FIRST_NAME, value: jsonData["data"]["first_name"].stringValue)
            AppPreference.getInstance().setString(AppConstants.LAST_NAME, value: jsonData["data"]["last_name"].stringValue)
            AppPreference.getInstance().setString(AppConstants.GENDER, value: jsonData["data"]["gender"].stringValue)
            AppPreference.getInstance().setString(AppConstants.EMAIL, value: jsonData["data"]["email"].stringValue)
            AppPreference.getInstance().setString(AppConstants.PHONE, value: jsonData["data"]["phone"].stringValue)
            AppPreference.getInstance().setString(AppConstants.IMAGE_URL, value: jsonData["data"]["image_url"].stringValue)
            AppPreference.getInstance().setString(AppConstants.SOURCE, value: jsonData["data"]["source"].stringValue)
            AppPreference.getInstance().setString(AppConstants.HOME_CITY, value: jsonData["data"]["home_city"].stringValue)
        }
    }
}
