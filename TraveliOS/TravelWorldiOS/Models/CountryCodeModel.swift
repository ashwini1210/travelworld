//
//  CountryCodeModel.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 15/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON

class CountryCodeModel: BaseModel {
    override init(delegate : HttpResponseProtocol){
        super.init(delegate: delegate)
    }
    
    /*
     * Method to hit on server for Currency Rate
     */
    func requestForCountryCode(){
        Alamofire.request(RestService.country_code, method: .get, encoding: JSONEncoding.default).responseJSON { response in switch response.result{
        case .success(let data):
            
            //Store json response in a global varialble for future use
            self.responseJSONData = JSON(data)
            AppPreference.getInstance().setString(AppConstants.CITY, value: self.responseJSONData["city"].stringValue)
            AppPreference.getInstance().setString(AppConstants.COUNTRY, value: self.responseJSONData["country"].stringValue)
            AppPreference.getInstance().setString(AppConstants.COUNTRY_CODE, value: self.responseJSONData["countryCode"].stringValue)
            AppPreference.getInstance().setString(AppConstants.ZIP, value: self.responseJSONData["zip"].stringValue)
            
            if(self.responseJSONData["countryCode"].stringValue == "IN"){
                AppPreference.getInstance().setString(AppConstants.CURRENCY_SIGN, value: "₹")
            }else{
                AppPreference.getInstance().setString(AppConstants.CURRENCY_SIGN, value: "$")
            }
            
        case .failure(let error):
            
            print("Error in CurrencyRateModel \(error.localizedDescription)")
            
            }
        }
    }
}
