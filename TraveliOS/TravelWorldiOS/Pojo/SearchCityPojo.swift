//
//  SearchCityPojo.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 11/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit

class SearchCityPojo: NSObject {
    var title : String? = ""
    var flag_url : String? = ""
    var cities_id : String? = ""
    var asci_code : String? = ""
    var country_name : String? = ""
    var country_id : String? = ""
    var nick_name : String? = ""
}
