//
//  PushNotification.swift
//  LeamigoiOS
//
//  Created by Apple on 05/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import Foundation
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import UIKit


extension AppDelegate{
            
    /*
     * This shows a permission dialog on first run, to
     * show the dialog at a more appropriate time move this registration accordingly.
     */
    func showNotifyPermissionDialog(_ application: UIApplication){
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
    }
    
    /*
     * Method to configure PushNotification
     */
    func configurePushNotification(){
        
        FirebaseConfiguration.shared.setLoggerLevel(.min)
        FirebaseApp.configure()
        Messaging.messaging().delegate = self as MessagingDelegate
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
    }
    
    
    func connectToFcm() {
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
               // self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
            }
        }

        
        // Disconnect previous FCM connection if it exists.
        Messaging.messaging().shouldEstablishDirectChannel = false
        
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        Messaging.messaging().apnsToken = deviceToken
     print("FCM token: \(String(describing: Messaging.messaging().apnsToken))")
        
          //  initializeNotificationModel(deviceToken: deviceToken)
      
        
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("didReceiveRemoteNotification Message ID: \(messageID)")
        }
        // Print full message.
        print(userInfo)
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("didReceiveRemoteNotification fetchCompletionHandler Message ID: \(messageID)")
            getNotificationData(userInfo: userInfo)
            
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    /*
     * Method to initialize NotificationModel to register device token
     */
    func initializeNotificationModel(deviceToken:String){
        
    }
    
    
    /*
     * Method gets called once device token is refresh
     */
  @objc func tokenRefreshNotification(notification: NSNotification) {
        let token = InstanceID.instanceID().token()
        if token != nil {
            initializeNotificationModel(deviceToken: token!)
        }
    }
    
}


extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("willPresent Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("didReceive Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
    
    /*
     [AnyHashable("message"): {"type":"text","title":"Hello","message":"ejfgjwegfjwegfjwegsfk"},
     AnyHashable("gcm.message_id"): 0:1502447518660765%c027baa7c027baa7,
     AnyHashable("key"): GENERIC,
     AnyHashable("aps"): {"content-available" = 1; }]
     */
    
    func getNotificationData(userInfo : [AnyHashable : Any]){
        
        var message : String? = nil
        var notificationKey : String? = nil
        print(userInfo)
        
        // print(userInfo[AnyHashable("message")]!)
        
        if let aps = userInfo[AnyHashable("message")] {
            message = aps as? String
        }
        
        if let key = userInfo[AnyHashable("key")] as? String{
            notificationKey = key
        }
        
        //Save Notification data in Dao
        saveDataInNotificationDao(message: message, notifcationkey: notificationKey)
        
        if notificationKey == "GENERIC"{
            
        }
        
    }
   
    /*
     * Method to save all Notification related data in DB.
     */
    func saveDataInNotificationDao(message: String?, notifcationkey: String? ){
        
        
    }
   
    
}

extension AppDelegate : MessagingDelegate {
    
    // // TODO: If necessary send token to application server.
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        self.initializeNotificationModel(deviceToken: fcmToken)
    }
    
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    
}

