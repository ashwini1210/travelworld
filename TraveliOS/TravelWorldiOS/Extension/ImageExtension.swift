//
//  ImageExtension.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 01/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    func makeImageWithColorAndSize(color: UIColor, size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
}
