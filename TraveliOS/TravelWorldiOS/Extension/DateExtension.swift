//
//  DateExtension.swift
//  TechnoPurple
//
//  Created by Apple on 20/02/18.
//  Copyright © 2018 Appleechnopurple.com. All rights reserved.
//

import Foundation
extension Date {
    
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
    
    //Function will return the current year
    func  get_year() -> Int {
        let calendar: Calendar = Calendar.current
        return calendar.component(.year, from: self);
    }
    
    //Function will reutrn the current month Number
    func  get_Month() -> Int {
        let calendar: Calendar = Calendar.current
        return calendar.component(.month, from: self);
    }
    
    //Function Will return the number of day of the month.
    func  get_Day() -> Int {
        let calendar: Calendar = Calendar.current
        return calendar.component(.day, from: self);
    }
    
    //Will return the timezone.
    func  get_TimeZone() -> Int {
        let calendar: Calendar = Calendar.current
        return calendar.component(.timeZone, from: self);
    }
    
    //Will return the Hour.
    func  get_Hour() -> Int {
        let calendar: Calendar = Calendar.current
        return calendar.component(.hour, from: self);
    }
    
    //Will return the Minute.
    func  get_Minute() -> Int {
        let calendar: Calendar = Calendar.current
        return calendar.component(.minute, from: self);
    }
    
    //Will return the Second.
    func  get_Second() -> Int {
        let calendar: Calendar = Calendar.current
        return calendar.component(.second, from: self);
    }
    
    //Will return the number day of week. like Sunday =1 , Monday = 2
    func  get_WeekDay() -> Int {
        let calendar: Calendar = Calendar.current
        return calendar.component(.weekday, from: self);
    }
    
    func dayNumberOfWeek() -> Int? {
        return Calendar.current.dateComponents([.weekday], from: self).weekday
    }
    
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
}
