//
//  BookExperienceViewController.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 16/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SwiftyJSON

class BookExperienceViewController: BaseVC {
    
    @IBOutlet weak var calenderView: UIView!
    @IBOutlet weak var view2: UIView! //Booking time view
    @IBOutlet weak var view3: UIView! //adult view
    @IBOutlet weak var view4: UIView! //childeren view
    @IBOutlet weak var calenderButton: UIButton!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var bookingTimeButton: UIButton!
    @IBOutlet weak var adultsLabel: UILabel!
    @IBOutlet weak var childrenLabel: UILabel!
    @IBOutlet weak var totalCostLabel: UILabel!
    @IBOutlet weak var dec1: UIButton!
    @IBOutlet weak var inc1: UIButton!
    @IBOutlet weak var dec2: UIButton!
    @IBOutlet weak var inc2: UIButton!
    @IBOutlet weak var bookALocalLabel: UILabel!
    
    var dataFronDetail : [String:Any]?
    var pickerView: PickerView!
    var timePickerView: TimePickerView!
    var guest = ""
    let currency = AppPreference.getInstance().getString(AppConstants.CURRENCY_SIGN, defaultvalue: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        borderViews()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()        
        if(dataFronDetail != nil){
            if(dataFronDetail!["identifier"] as! String == AppConstants.TOP_EXPERIENCE){
                bookALocalLabel.text = "Book Experience"
            }else if(dataFronDetail!["identifier"] as! String == AppConstants.FESTIVAL){
                bookALocalLabel.text = "Book Festival"
            }
        }
    }
    
    func initializeView(){
        
        //Initialize Custom Pickerview
        pickerView = (PickerView.instanceFromNib() as! PickerView)
        pickerView.bookExperienceViewController = self
        self.view.addSubview(pickerView)
        pickerView.isHidden = true //Initially hide calender view
        
        //Initialize Custom TimePickerview
        timePickerView = (TimePickerView.instanceFromNib() as! TimePickerView)
        timePickerView.bookExperienceViewController = self
        self.view.addSubview(timePickerView)
        timePickerView.isHidden = true //Initially hide timepicker view
        
        guest = (self.dataFronDetail!["min_guest"] as? String)!
        adultsLabel.text = dataFronDetail!["min_guest"] as? String
        totalCostLabel.text = "Total Price: \(currency)\(calculatePrice(guest: Int(guest)!))"
        childrenLabel.text = "1"
        
    }
    
    /*
     * Method to Highlight and border view
     */
    func borderViews(){
        AppUtils.shadowView(view: calenderView, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        AppUtils.shadowView(view: view2, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        AppUtils.shadowView(view: view3, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        AppUtils.shadowView(view: view4, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        AppUtils.cornerRadius(view: proceedButton, roundCornerWidth: 5)
        AppUtils.cornerRadius(view: backButton, roundCornerWidth: 5)
        
        let colour = AppUtils.hexStringToUIColor(hex: "#0F6A5B")
        AppUtils.borderView(view: dec1, borderWidth: 1, borderColour: colour.cgColor)
        AppUtils.borderView(view: dec2, borderWidth: 1, borderColour: colour.cgColor)
        AppUtils.borderView(view: inc1, borderWidth: 1, borderColour: colour.cgColor)
        AppUtils.borderView(view: inc2, borderWidth: 1, borderColour: colour.cgColor)
    }
    
    /*
     * Button to show calender view
     */
    @IBAction func showCalenderBtnClk(_ sender: Any) {
        self.pickerView.getYear()
        self.pickerView.getDayMonth()
        self.pickerView.isHidden = false
    }
    
    @IBAction func bookingTimeBtnClk(_ sender: Any) {
        self.timePickerView.getTime()
        self.timePickerView.isHidden = false
    }
    
    @IBAction func decreaseButtonCliked(_ sender: Any) {
        let button = sender as! UIButton
        if(button.tag == 2){
            var adultsCount = Int(adultsLabel.text!)
            if(adultsCount! > 2){
                adultsCount = adultsCount! - 1
            }
            adultsLabel.text! = "\(adultsCount!)"
            totalCostLabel.text = "Total Price: \(currency)\(calculatePrice(guest: Int(adultsLabel.text!)!))"
        }
            //Decrease count of Children
        else if(button.tag == 3){
            var childrenCount = Int(childrenLabel.text!)
            if(childrenCount! > 1){
                childrenCount = childrenCount! - 1
            }
            childrenLabel.text! = "\(childrenCount!)"
        }
    }
    
    @IBAction func increaseButtonClicked(_ sender: Any) {
        let button = sender as! UIButton
        if(button.tag == 2){
            
            var adultsCount = Int(adultsLabel.text!)
            if(adultsCount! >= Int(guest)!){
                adultsCount = adultsCount! + 1
            }
            adultsLabel.text! = "\(adultsCount!)"
            totalCostLabel.text = "Total Price: \(currency)\(calculatePrice(guest: Int(adultsLabel.text!)!))"
        }
            //Increase count of Children
        else if(button.tag == 3){
            var childrenCount = Int(childrenLabel.text!)
            if(childrenCount! >= 1){
                childrenCount = childrenCount! + 1
            }
            childrenLabel.text! = "\(childrenCount!)"
        }
        
        
    }
    
    @IBAction func proceedButtonClicked(_ sender: Any) {
        if(calenderButton.titleLabel!.text == "Please select a Date"){
            self.displayAlert(title: "", message: AppConstants.SELECT_PROPER_ERROR_MSG + " date", btn1: AppConstants.OK_BUTTON)
        }else if (bookingTimeButton.titleLabel!.text == "Booking Time"){
            self.displayAlert(title: "", message: AppConstants.SELECT_PROPER_ERROR_MSG + " booking time", btn1: AppConstants.OK_BUTTON)
        }else{
            let isLogin = AppPreference.getInstance().getBoolean(AppConstants.IS_LOGIN, defaultvalue: false)
            if(!isLogin){
                self.launchViewController(viewControllerName: AppConstants.LoginViewController)
            }else{
                self.displayConfirmationAlert(title: AppConstants.BOOKING_CONFIRM_TITLE, message: AppConstants.BOOKING_CONFIRM_MSG, btn1: "NO", btn2: "YES", handler: confirmBooking)
            }
        }
    }
    
    func confirmBooking(action: UIAlertAction) {
        //Hit on server for booking confirm
        var data = [String:Any]()
       data["booking_date"] = calenderButton.titleLabel?.text
        data["booking_time"] = AppUtils.convertTimeTo24Hrs(time12Hrs: bookingTimeButton.titleLabel!.text)
        let price = totalCostLabel.text?.components(separatedBy: "Total Price: ₹")
        if(price!.count > 1){
            data["total_amount"] = price![1]
        }
        let detailDao = DetailDB().findAll().first
        if(detailDao != nil){
            data["exp_id"] = dataFronDetail!["exp_id"] as? String
            data["child"] = childrenLabel.text
            data["adult"] = adultsLabel.text
            data["city_id"] = detailDao!.city_id
            data["product_id"] = dataFronDetail!["exp_id"] as? String
            data["currency_id"] = detailDao!.base_currency_id
            data["base_price"] = detailDao!.base_rate
            data["duration_type"] = detailDao!.duration_type
        }
        
        if(dataFronDetail!["identifier"] as? String == AppConstants.TOP_EXPERIENCE){
            data["vertical_id"] = AppConstants.WISHLIST_EXPERIENCE
        }
        else if(dataFronDetail!["identifier"] as? String == AppConstants.FESTIVAL){
            data["vertical_id"] = AppConstants.WISHLIST_FESTIVAL
        }
        else if(dataFronDetail!["identifier"] as? String == AppConstants.TRENDING_ACTIVITY){
            data["vertical_id"] = AppConstants.WISHLIST_TRENDING_ACTIVITY
        }
        let addBookingModel = AddBookingModel(delegate: self)
        addBookingModel.requestForAddBooking(data: data)
    }
    
    @IBAction func onBackButtonClicked(_ sender: Any) {
        self.onBackNavigation()
    }
    
    func calculatePrice(guest : Int) -> Double{
        let currencyRate = AppPreference.getInstance().getInt(AppConstants.CURRENCY_RATE, defaultvalue: 0)
        let basePrice = Int(dataFronDetail!["base_price"] as! String)
        let price = currencyRate * basePrice! * guest
        return Double(price)
    }
    
    override func onHttpResponse(isSuccess: Bool, responseJson: JSON, error: AnyObject!, anyIdentifier: String) {
        if(isSuccess){
            if(anyIdentifier == AppConstants.ADD_BOOKING){
                self.displayAlert(title: "", message: AppConstants.BOOKING_SUCCESS_MSG, btn1: AppConstants.OK_BUTTON)
                let price = totalCostLabel.text?.components(separatedBy: "Total Price: ₹")
                if(price!.count > 1){
                    var data = [String:Any]()
                    data["bookingId"] = responseJson["data"]["data"]["booking_id"].stringValue
                    data["price"] = price![1]
                    
                     DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) {
                    self.launchViewController(viewControllerName: AppConstants.PaymentViewController, dataToPass: data)
                    }
                }
            }
        }else{
            self.showToast(message: AppConstants.TRY_AGAIN_MSG)
        }
    }
}
