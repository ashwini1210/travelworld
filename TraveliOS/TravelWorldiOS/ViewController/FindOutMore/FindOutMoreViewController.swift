//
//  FindOutMoreViewController.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 25/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit

class FindOutMoreViewController: BaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func onBackButtonClicked(_ sender: Any) {
        self.onBackNavigation()
    }
}
