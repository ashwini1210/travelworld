//
//  AmigoCollectionViewCell.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 30/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SwiftyJSON

class AmigoCollectionViewCell: UICollectionViewCell, HttpResponseProtocol {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var interestsLabel: UILabel!
    @IBOutlet weak var dayTimeLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var amigoLastLabel: UILabel!
    @IBOutlet weak var amigoLabelFirst: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var payButton: UIButton!
    
    var upcomingBookingViewController: UpcomingBookingViewController? = nil
    let bookingDB = BookingDB()
    var bookingid = ""
    // this will be our "call back" action
    var cancelBtnTapAction : (()->())?
    var payBtnTapAction : (()->())?
    
    var day : String?
    var date : String?
    
    let currencySign = AppPreference.getInstance().getString(AppConstants.CURRENCY_SIGN, defaultvalue: "")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    func initializeView(){
        AppUtils.borderView(view: cancelButton, borderWidth: 1, borderColour: AppUtils.hexStringToUIColor(hex: "#FF6666").cgColor)
        AppUtils.borderView(view: amigoLabelFirst, borderWidth: 1, borderColour: UIColor.gray.cgColor)
        AppUtils.borderView(view: self.contentView, borderWidth: 1, borderColour: UIColor.lightGray.cgColor)
    }
    
    /*
     * Method to set cell Data
     */
    func setAmigoUpcomingDataInCell(indexPath: IndexPath){
        let bookingUpcomingDao = BookingDB().findAll().first
        if(bookingUpcomingDao != nil){
            let upcomingDao = bookingUpcomingDao!.bookingUpcomingDao
            self.cityLabel.text = upcomingDao[indexPath.row].city_name
            self.interestsLabel.text = "Interests: \(upcomingDao[indexPath.row].interests)"
            day = AppUtils.getDayOfWeek(date: upcomingDao[indexPath.row].booking_date)
            //"\(upcomingDao[indexPath.row].booking_date) \(upcomingDao[indexPath.row].booking_time)"
            date = AppUtils.formatDateString(dateString: "\(upcomingDao[indexPath.row].booking_date) \(upcomingDao[indexPath.row].booking_time)")
            self.dayTimeLabel.text = "\(day!) \(date!)"
            self.durationLabel.text = "Duration: \(upcomingDao[indexPath.row].duration) Hrs"
            self.amountLabel.text = "Total Amount: \(currencySign)\(upcomingDao[indexPath.row].total_amount)"
            self.statusLabel.text = "Status: \(upcomingDao[indexPath.row].payment_status)"
            self.amigoLastLabel.text = "Amigo: \(upcomingDao[indexPath.row].amigo_name)"
            
            initializeCancelModel(booking_id: upcomingDao[indexPath.row].booking_id, identifier: AppConstants.UPCOMING_BOOKING_CANCEL)
            proceedToPay(bookingId: upcomingDao[indexPath.row].booking_id)
        }
    }
    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        cancelBtnTapAction?()
    }
    
    func initializeCancelModel(booking_id :String, identifier: String){
        // set a "Callback Closure" in the cell
        self.cancelBtnTapAction = {
            () in
            self.upcomingBookingViewController?.displayConfirmationAlert(title: "", message: AppConstants.CANCEL_BOOKING, btn1: AppConstants.CANCEL_BUTTON, btn2: AppConstants.OK_BUTTON, handler: self.confirmBooking)
            // self.upcomingBookingViewController!.displayAlert(title: "", message: AppConstants.CANCEL_BOOKING, btn1: AppConstants.OK_BUTTON)
            self.bookingid = booking_id
        }
    }
    
    func confirmBooking(action: UIAlertAction) {
        var data = [
            "booking_id" : bookingid as Any
            ] as [String:Any]?
        let cancelBookingModel = CancelBookingModel.init(delegate: self)
        cancelBookingModel.requestForCancelBooking(data: data, identifier: AppConstants.UPCOMING_BOOKING_CANCEL)
        data = nil
    }
    
    func onHttpResponse(isSuccess: Bool, responseJson: JSON, error: AnyObject!, anyIdentifier: String) {
        if(anyIdentifier == AppConstants.UPCOMING_BOOKING_CANCEL){
            //to do after cancel response
            
            let bookingUpcomingDaoList = bookingDB.findAll().first!.bookingUpcomingDao
            let resultPredicate = NSPredicate(format: "booking_id = %@", responseJson["booking_id"].stringValue)
            let index = bookingUpcomingDaoList.index(matching: resultPredicate)
            if(index != nil){
                bookingDB.realm.beginWrite()
                bookingUpcomingDaoList.realm?.delete(bookingUpcomingDaoList[index!])
                bookingid = ""
                try! bookingDB.realm.commitWrite()
                bookingDB.realm.refresh()
            }
            upcomingBookingViewController?.upcomingCollectionview.reloadData()
        }
    }
    
    @IBAction func payButtonClicked(_ sender: Any) {
        payBtnTapAction?()
    }
    
    func proceedToPay(bookingId:String?){
        self.payBtnTapAction = {
            if(self.statusLabel.text == "Booked & Paid"){
                self.upcomingBookingViewController!.showToast(message: "Payment Already Done!")
            }else{
                let price = self.amountLabel.text?.components(separatedBy: "Total Amount: ₹")
                if(price!.count > 1){
                    var data = [String:Any]()
                    data["bookingId"] = bookingId
                    data["price"] = price![1]
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) {
                        self.upcomingBookingViewController!.launchViewController(viewControllerName: AppConstants.PaymentViewController, dataToPass: data)
                    }
                }
            }
        }
    }
}
