//
//  UpcomingCollectionViewCell.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 01/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SwiftyJSON

class UpcomingCollectionViewCell: UICollectionViewCell, HttpResponseProtocol {
    
    
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var descriptionLbael: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var payButton: UIButton!
    var upcomingBookingViewController: UpcomingBookingViewController? = nil
    var day : String?
    var date : String?
    var bookingid = ""
    
    // this will be our "call back" action
    var cancelBtnTapAction : (()->())?
    var payBtnTapAction : (()->())?
    
    let currency = AppPreference.getInstance().getInt(AppConstants.CURRENCY_RATE, defaultvalue: 0)
    let currencySign = AppPreference.getInstance().getString(AppConstants.CURRENCY_SIGN, defaultvalue: "")
    let bookingDB = BookingDB()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeViews()
    }
    
    func initializeViews(){
        AppUtils.borderView(view: cancelButton, borderWidth: 1, borderColour: AppUtils.hexStringToUIColor(hex: "#FF6666").cgColor)
        AppUtils.borderView(view: self.contentView, borderWidth: 1, borderColour: UIColor.lightGray.cgColor)
    }
    
    /*
     * Method to set cell Data
     */
    func setUpcomingDataInCell(indexPath: IndexPath){
        if(upcomingBookingViewController!.upcomingImagesArray.count != 0){
            self.imageview.image = upcomingBookingViewController!.upcomingImagesArray[indexPath.row]
            let bookingUpcomingDao = bookingDB.findAll().first
            if(bookingUpcomingDao != nil){
                let upcomingDao = bookingUpcomingDao!.bookingUpcomingDao
                self.descriptionLbael.text = upcomingDao[indexPath.row].experience_name
                self.placeLabel.text = upcomingDao[indexPath.row].city_name
                day = AppUtils.getDayOfWeek(date: upcomingDao[indexPath.row].booking_date)
                date = AppUtils.formatDateString(dateString: "\(upcomingDao[indexPath.row].booking_date) \(upcomingDao[indexPath.row].booking_time)")
                self.dateTimeLabel.text = "\(day!) \(date!)"
                let amount = Double(Int(upcomingDao[indexPath.row].total_amount)! * currency)
                self.amountLabel.text = "Total Amount: \(currencySign)\(amount)"
                self.statusLabel.text = "Status: \(upcomingDao[indexPath.row].payment_status)"
                
                //                if(self.statusLabel.text == "Booked & Paid"){
                //                    payButton.isHidden = true
                //                    cancelButton.frame = CGRect(x: cancelButton.frame.origin.x, y: cancelButton.frame.origin.y, width: cancelButton.frame.size.width + payButton.frame.size.width, height: cancelButton.frame.size.height)
                //                }
                
                initializeCancelModel(booking_id: upcomingDao[indexPath.row].booking_id, identifier: AppConstants.UPCOMING_BOOKING_CANCEL)
                proceedToPay(bookingId: upcomingDao[indexPath.row].booking_id)
            }
        }
    }
    
    
    
    /*
     * Method to set cell Data
     */
    func setHistoryDataInCell(indexPath: IndexPath,bookingHistoryViewController: BookingHistoryViewController){
        if(bookingHistoryViewController.historyImagesArray.count != 0){
            self.imageview.image = bookingHistoryViewController.historyImagesArray[indexPath.row]
            let bookingUpcomingDao = bookingDB.findAll().first
            if(bookingUpcomingDao != nil){
                let historyDao = bookingUpcomingDao!.bookingHistoryDao
                self.descriptionLbael.text = historyDao[indexPath.row].experience_name
                self.placeLabel.text = historyDao[indexPath.row].city_name
                day = AppUtils.getDayOfWeek(date: historyDao[indexPath.row].booking_date)
                date = AppUtils.formatDateString(dateString: "\(historyDao[indexPath.row].booking_date) \(historyDao[indexPath.row].booking_time)")
                self.dateTimeLabel.text = "\(day!) \(date!)"
                let amount = Double(Int(historyDao[indexPath.row].total_amount)! * currency)
                self.amountLabel.text = "Total Amount: \(currencySign)\(amount)"
                self.statusLabel.text = "Status: \(historyDao[indexPath.row].payment_status)"
                self.cancelButton.isHidden = true
                self.payButton.isHidden = true
            }
        }
    }
    
    
    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        cancelBtnTapAction?()
    }
    
    func initializeCancelModel(booking_id :String, identifier: String){
        // set a "Callback Closure" in the cell
        self.cancelBtnTapAction = {
            () in
            self.upcomingBookingViewController?.displayConfirmationAlert(title: "", message: AppConstants.CANCEL_BOOKING, btn1: AppConstants.CANCEL_BUTTON, btn2: AppConstants.OK_BUTTON, handler: self.confirmBooking)
            // self.upcomingBookingViewController!.displayAlert(title: "", message: AppConstants.CANCEL_BOOKING, btn1: AppConstants.OK_BUTTON)
            self.bookingid = booking_id
        }
    }
    
    func confirmBooking(action: UIAlertAction) {
        var data = [
            "booking_id" : bookingid as Any
            ] as [String:Any]?
        let cancelBookingModel = CancelBookingModel.init(delegate: self)
        cancelBookingModel.requestForCancelBooking(data: data, identifier: AppConstants.UPCOMING_BOOKING_CANCEL)
        data = nil
    }
    
    func onHttpResponse(isSuccess: Bool, responseJson: JSON, error: AnyObject!, anyIdentifier: String) {
        if(anyIdentifier == AppConstants.UPCOMING_BOOKING_CANCEL){
            //to do after cancel response
            
            let bookingUpcomingDaoList = bookingDB.findAll().first!.bookingUpcomingDao
            let resultPredicate = NSPredicate(format: "booking_id = %@", responseJson["booking_id"].stringValue)
            let index = bookingUpcomingDaoList.index(matching: resultPredicate)
            if(index != nil){
                bookingDB.realm.beginWrite()
                bookingUpcomingDaoList.realm?.delete(bookingUpcomingDaoList[index!])
                bookingid = ""
                try! bookingDB.realm.commitWrite()
                bookingDB.realm.refresh()
            }
            upcomingBookingViewController?.upcomingCollectionview.reloadData()
        }
    }
    
    @IBAction func payButtonClicked(_ sender: Any) {
        payBtnTapAction?()
    }
    
    func proceedToPay(bookingId:String?){
        self.payBtnTapAction = {
            if(self.statusLabel.text == "Booked & Paid"){
                self.upcomingBookingViewController!.showToast(message: "Payment Already Done!")
            }else{
                let price = self.amountLabel.text?.components(separatedBy: "Total Amount: ₹")
                if(price!.count > 1){
                    var data = [String:Any]()
                    data["bookingId"] = bookingId
                    data["price"] = price![1]
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) {
                        self.upcomingBookingViewController!.launchViewController(viewControllerName: AppConstants.PaymentViewController, dataToPass: data)
                    }
                }
            }
        }
    }
}
