//
//  UpcomingBookingViewController.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 01/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SwiftyJSON

class UpcomingBookingViewController: BaseVC, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var exploreWorldView: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var upcomingCollectionview: UICollectionView!
    
    var parentTabBarViewController : ParentTabBarViewController?
    let reuseIdentifier = "amigoCollectionViewCell"
    let reuseIdentifier2 = "upcomingCell"
    var upcomingImagesArray = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCollectionCell()
        self.exploreWorldView.isHidden = false
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        hideView()
        initializeModel()
    }
    
    func initializeModel(){
        let bookingHistoryModel =  BookingHistoryModel.init(delegate: self)
        bookingHistoryModel.requestForBooking(identifier: AppConstants.UPCOMING_BOOKING)
    }
    
    override func onHttpResponse(isSuccess: Bool, responseJson: JSON, error: AnyObject!, anyIdentifier: String) {
        if(anyIdentifier == AppConstants.UPCOMING_BOOKING){
            onHistoryResponse(isSuccess: isSuccess, responseJson: responseJson)
        }else{
             self.showToast(message: AppConstants.TRY_AGAIN_MSG)
        }
    }
    
    
    func onHistoryResponse(isSuccess: Bool, responseJson: JSON!){
        if isSuccess {
            if responseJson["status"].boolValue{
                let bookingDao = BookingDB().findAll().first
                if(bookingDao != nil){
                    let bookingUpcomingListDao = bookingDao!.bookingUpcomingDao
                    for bookingUpcomingDao in bookingUpcomingListDao{
                       // if(bookingUpcomingDao.card_image_url != ""){
                        let downloadImages = DownloadImages.init(delegate: self)
                        downloadImages.downloadImageAsync(URLString: bookingUpcomingDao.card_image_url, placeHolderImage: UIImage(named: "blog1.jpg")!,identifier: AppConstants.UPCOMING_BOOKING, id: bookingUpcomingDao.booking_id)
                        upcomingImagesArray.append(UIImage(named: "offer1.jpg")!)
                       // }
                    }
                    upcomingCollectionview.reloadData()
                }
            }
            
            hideView()
        }
        else {
            print("Error to onHistoryResponse: \(responseJson["message"].stringValue)")
        }
    }
    
    override func onImageResponse(isSuccess: Bool?, image: UIImage?, error: AnyObject?, anyIdentifier: String?, id: String?) {
        if(anyIdentifier == AppConstants.UPCOMING_BOOKING){
            if(image != nil){
                let imageDaoList = BookingDB().findAll().first!.bookingUpcomingDao
                let resultPredicate = NSPredicate(format: "booking_id = %@", id!)
                let index = imageDaoList.index(matching: resultPredicate)
                if(index != nil){
                    upcomingImagesArray[index!] = image!
                }

            }
            upcomingCollectionview.reloadData()
        }
    }
    
    @IBAction func exploreWorldButtonClicked(_ sender: Any) {
        parentTabBarViewController!.selectedViewController =  parentTabBarViewController!.destinationViewController
    }
    
    func registerCollectionCell(){
        upcomingCollectionview.register(UINib.init(nibName: "AmigoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        upcomingCollectionview.register(UINib.init(nibName: "UpcomingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier2)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = getCountOfCell()
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let bookingHistoryDao = BookingDB().findAll().first!.bookingUpcomingDao[indexPath.row]
        hideView()
        if(bookingHistoryDao.vertical_id == "1" || bookingHistoryDao.vertical_id == "2"){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier2, for: indexPath) as! UpcomingCollectionViewCell
            cell.upcomingBookingViewController = self
            cell.setUpcomingDataInCell(indexPath: indexPath)
            AppUtils.shadowView(view: cell, shadowColour: UIColor.lightGray.cgColor, shadowRadius: 3)
            return cell
        }else{
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! AmigoCollectionViewCell
            cell1.upcomingBookingViewController = self
            cell1.setAmigoUpcomingDataInCell(indexPath: indexPath)
            AppUtils.shadowView(view: cell1, shadowColour: UIColor.lightGray.cgColor, shadowRadius: 3)
            return cell1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
    /*
     * Method to hide exploreWorldView when data is present collection view
     */
    func hideView(){
        //Check data in array. If array count in not 0, then hide exploreWorldView
        if(upcomingImagesArray.count == 0){
            self.exploreWorldView.isHidden = false
            self.upcomingCollectionview.isHidden = true
        }else{
            self.exploreWorldView.isHidden = true
            self.upcomingCollectionview.isHidden = false
        }
    }
    
    /*
     * Get count of collection view cell
     */
    func getCountOfCell() -> Int{
        let bookingDao = BookingDB().findAll().first
        if(bookingDao != nil){
            let bookingHistoryDao =  bookingDao!.bookingUpcomingDao
            return bookingHistoryDao.count
        }
        return 0
    }
    
}
