//
//  bookingCollectionViewCell.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 01/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit

class bookingCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var interestsLabel: UILabel!
    @IBOutlet weak var dayTimeLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var amigoLastLabel: UILabel!
    @IBOutlet weak var amigoLabelFirst: UILabel!
    var day : String?
    var date : String?
    
    let currencySign = AppPreference.getInstance().getString(AppConstants.CURRENCY_SIGN, defaultvalue: "")

    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    func initializeView(){
        AppUtils.borderView(view: amigoLabelFirst, borderWidth: 1, borderColour: UIColor.gray.cgColor)
        AppUtils.borderView(view: self.contentView, borderWidth: 1, borderColour: UIColor.lightGray.cgColor)
    }

    /*
     * Method to set cell Data
     */
    func setDataInCell(indexPath: IndexPath){
       let bookingUpcomingDao = BookingDB().findAll().first
        if(bookingUpcomingDao != nil){
            let historyDao = bookingUpcomingDao!.bookingHistoryDao
            self.cityLabel.text = historyDao[indexPath.row].city_name
            self.interestsLabel.text = "Interests: \(historyDao[indexPath.row].interests)"
            day = AppUtils.getDayOfWeek(date: historyDao[indexPath.row].booking_date)
            date = AppUtils.formatDateString(dateString: "\(historyDao[indexPath.row].booking_date) \(historyDao[indexPath.row].booking_time)")
            self.dayTimeLabel.text = "\(day!) \(date!)"
            self.durationLabel.text = "Duration: \(historyDao[indexPath.row].duration) Hrs"
            self.amountLabel.text = "Total Amount: \(currencySign)\(historyDao[indexPath.row].total_amount)"
            self.statusLabel.text = "Status: \(historyDao[indexPath.row].amigo_status)"
            self.amigoLastLabel.text = "Amigo: \(historyDao[indexPath.row].amigo_name)"
        }
    }
    
    /*
     * Method to set cell Data
     */
    func setAmigoUpcomingDataInCell(indexPath: IndexPath){
        let bookingUpcomingDao = BookingDB().findAll().first
        if(bookingUpcomingDao != nil){
            let upcomingDao = bookingUpcomingDao!.bookingUpcomingDao
            self.cityLabel.text = upcomingDao[indexPath.row].city_name
            self.interestsLabel.text = "Interests: \(upcomingDao[indexPath.row].interests)"
            day = AppUtils.getDayOfWeek(date: upcomingDao[indexPath.row].booking_date)
            date = AppUtils.formatDateString(dateString: "\(upcomingDao[indexPath.row].booking_date) \(upcomingDao[indexPath.row].booking_time)")
            self.dayTimeLabel.text = "\(day!) \(date!)"
            self.durationLabel.text = "Duration: \(upcomingDao[indexPath.row].duration) Hrs"
            self.amountLabel.text = "Total Amount: \(currencySign)\(upcomingDao[indexPath.row].total_amount)"
            self.statusLabel.text = "Status: \(upcomingDao[indexPath.row].amigo_status)"
            self.amigoLastLabel.text = "Amigo: \(upcomingDao[indexPath.row].amigo_name)"
            
        }
    }
}
