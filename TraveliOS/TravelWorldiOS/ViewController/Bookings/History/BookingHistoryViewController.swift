//
//  BookingHistoryViewController.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 01/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SwiftyJSON

class BookingHistoryViewController: BaseVC, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var exploreWorldView: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var historyCollectionView: UICollectionView!
    
    var parentTabBarViewController : ParentTabBarViewController?
    let reuseIdentifier = "bookinghistoryCell"
    let reuseIdentifier2 = "upcomingCell"
    var historyImagesArray = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCollectionCell()
        initializeViews()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    
    func initializeViews(){
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        downloadBookingImages()
        hideView()
    }
    
    
    func registerCollectionCell(){
        historyCollectionView.register(UINib.init(nibName: "bookingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        historyCollectionView.register(UINib.init(nibName: "UpcomingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier2)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = getCountOfCell()
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let bookingHistoryDao = BookingDB().findAll().first!.bookingHistoryDao[indexPath.row]
        hideView()
        if(bookingHistoryDao.vertical_id == "1" || bookingHistoryDao.vertical_id == "2"){
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier2, for: indexPath) as! UpcomingCollectionViewCell
            cell2.setHistoryDataInCell(indexPath: indexPath, bookingHistoryViewController: self)
            AppUtils.shadowView(view: cell2, shadowColour: UIColor.lightGray.cgColor, shadowRadius: 3)
            return cell2
        }else{
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! bookingCollectionViewCell
            cell1.setDataInCell(indexPath: indexPath)
            AppUtils.shadowView(view: cell1, shadowColour: UIColor.lightGray.cgColor, shadowRadius: 3)
            return cell1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
    /*
     * Get count of collection view cell
     */
    func getCountOfCell() -> Int{
        let bookingDao = BookingDB().findAll().first
        if(bookingDao != nil){
            let bookingHistoryDao =  bookingDao!.bookingHistoryDao
            return bookingHistoryDao.count
        }
        return 0
    }
    
    func downloadBookingImages(){
        let bookingDao = BookingDB().findAll().first
        if(bookingDao != nil){
            let bookingUpcomingListDao = bookingDao!.bookingHistoryDao
            for bookingUpcomingDao in bookingUpcomingListDao{
                let downloadImages = DownloadImages.init(delegate: self)
                downloadImages.downloadImageAsync(URLString: bookingUpcomingDao.card_image_url, placeHolderImage: UIImage(named: "blog1.jpg")!,identifier: AppConstants.BOOKING_HISTORY, id: bookingUpcomingDao.product_id)
                historyImagesArray.append(UIImage(named: "offer1.jpg")!)
            }
            historyCollectionView.reloadData()
        }
    }
    
    override func onImageResponse(isSuccess: Bool?, image: UIImage?, error: AnyObject?, anyIdentifier: String?, id: String?) {
        if(anyIdentifier == AppConstants.BOOKING_HISTORY){
            if(image != nil){
                let imageDaoList = BookingDB().findAll().first!.bookingHistoryDao
                let resultPredicate = NSPredicate(format: "product_id = %@", id!)
                let index = imageDaoList.index(matching: resultPredicate)
                if(index != nil){
                    historyImagesArray[index!] = image!
                }
            }
    
            historyCollectionView.reloadData()
        }
    }
    
    @IBAction func exploreWorldButtonClicked(_ sender: Any) {
        parentTabBarViewController!.selectedViewController =  parentTabBarViewController!.destinationViewController
    }
    
    /*
     * Method to hide exploreWorldView when data is present collection view
     */
    func hideView(){
        
        //Check data in array. If array count in not 0, then hide exploreWorldView
        if(historyImagesArray.count == 0){
            self.exploreWorldView.isHidden = false
            historyCollectionView.isHidden = true
        }else{
            self.exploreWorldView.isHidden = true
            historyCollectionView.isHidden = false
        }
    }
}
