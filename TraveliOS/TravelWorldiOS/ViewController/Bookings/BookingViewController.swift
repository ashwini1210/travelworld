//
//  BookingViewController.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 01/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit

class BookingViewController: BaseVC {
    
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var upcomingButton: UIButton!
    @IBOutlet weak var bookingHistoryButton: UIButton!
    @IBOutlet weak var viewControllerView: UIView!
    
    //Views for green line below tab bar
    var lineParentView = UIView()
    var lineSubView1 = UIView()
    var lineSubView2 = UIView()
    
    var upcomingBookingViewController: UpcomingBookingViewController?
    var bookingHistoryViewController : BookingHistoryViewController?
    var parentTabBarViewController : ParentTabBarViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLineView()
        registerChildController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    
    /*
     * Set chield view of tab bar controller
     */
    func setUpLineView(){
        //Parent view of Green line
        let screenSize: CGRect = UIScreen.main.bounds
        lineParentView = UIView(frame: CGRect(x: 0, y: buttonsView.frame.origin.y + buttonsView.frame.size.height, width: screenSize.width, height: 3))
        
        //Child view of Parent  Green line view
        lineSubView1 = UIView(frame: CGRect(x: 0, y:0, width: screenSize.width/2, height: 3))
        lineSubView1.backgroundColor = AppUtils.hexStringToUIColor(hex: "#0F6A5B")
        lineParentView.addSubview(lineSubView1)
        
        //Child view of Parent  Green line view
        lineSubView2 = UIView(frame: CGRect(x: lineSubView1.frame.size.width, y:0, width: screenSize.width/2, height: 3))
        lineSubView2.backgroundColor = UIColor.clear
        lineParentView.addSubview(lineSubView2)
        
        self.view.addSubview(lineParentView)
    }
    
    /*
     * Register two viewcontroller
     */
    func registerChildController(){
        upcomingBookingViewController = UpcomingBookingViewController()
        upcomingBookingViewController!.view.frame = viewControllerView.bounds
        viewControllerView.addSubview(upcomingBookingViewController!.view)
        addChild(upcomingBookingViewController!)
        upcomingBookingViewController!.parentTabBarViewController = parentTabBarViewController
        upcomingBookingViewController!.didMove(toParent: self)
        
        bookingHistoryViewController = BookingHistoryViewController()
        bookingHistoryViewController!.view.frame = viewControllerView.bounds
        viewControllerView.addSubview(bookingHistoryViewController!.view)
        addChild(bookingHistoryViewController!)
        bookingHistoryViewController!.didMove(toParent: self)
        bookingHistoryViewController!.parentTabBarViewController = parentTabBarViewController
        bookingHistoryViewController!.view.isHidden = true
    }
    
    @IBAction func upcomingButtonClicked(_ sender: Any) {
        bookingHistoryViewController!.view.isHidden = true //Hide bookingHistoryViewController
        upcomingBookingViewController!.view.isHidden = false //show upcomingBookingViewController
        upcomingButton.titleLabel?.textColor = AppUtils.hexStringToUIColor(hex: "#0F6A5B") // Change upcomingButton text colour to green
        bookingHistoryButton.titleLabel?.textColor = UIColor.black // Change bookingHistoryButton text colour to black
        lineSubView1.backgroundColor = AppUtils.hexStringToUIColor(hex: "#0F6A5B") //chenge bottom first line view colour to green
        lineSubView2.backgroundColor = UIColor.clear //chenge bottom second line view colour to clear
        
    }
    
    @IBAction func bookingHistoryButtonClicked(_ sender: Any) {
        upcomingBookingViewController!.view.isHidden = true //Hide upcomingBookingViewController
        bookingHistoryViewController!.view.isHidden = false //show bookingHistoryViewController
        bookingHistoryButton.titleLabel?.textColor = AppUtils.hexStringToUIColor(hex: "#0F6A5B") // Change bookingHistoryButton text colour to green
        upcomingButton.titleLabel?.textColor = UIColor.black // Change upcomingButton text colour to black
        lineSubView2.backgroundColor = AppUtils.hexStringToUIColor(hex: "#0F6A5B") //chenge bottom second line view colour to green
        lineSubView1.backgroundColor = UIColor.clear //chenge bottom first line view colour to clear
        bookingHistoryViewController!.hideView()
        bookingHistoryViewController!.downloadBookingImages()
       
    }
}
