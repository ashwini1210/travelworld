//
//  SplashViewController.swift
//  LeamingoiOS
//
//  Created by Apple on 03/08/18.
//  Copyright © 2018 Appleechnopurple.com. All rights reserved.
//

import UIKit

class SplashViewController: BaseVC {
    
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        animateImage()
        if(AppUtils.isConnectedToInternet()){
            initializeCurrencyModel()
            initializeCountryCodeModel()
            launchNextVC(VCName: AppConstants.ParentTabBarViewController)
        }else{
            self.displayAlert(title: "", message: AppConstants.INTERNET_MESSAGE, btn1: AppConstants.OK_BUTTON)
        }
    }
    
    func animateImage(){
        fadeOut()
        fadeIn()
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    /*
     * Method to launch View COntroller according to assign string
     */
    func launchNextVC (VCName:String){
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
            self.launchViewController(viewControllerName: VCName)
        }
    }
    
    func initializeCurrencyModel(){
        let currencyRateModel = CurrencyRateModel.init(delegate: self)
        currencyRateModel.requestForCurrencyRate()
    }
    
    func initializeCountryCodeModel(){
        let currencyRateModel = CountryCodeModel.init(delegate: self)
        currencyRateModel.requestForCountryCode()
    }
    
    func fadeIn(_ duration: TimeInterval = 0.1, delay: TimeInterval = 0.5, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.showHideTransitionViews, animations: {
            self.imageView.alpha = 1.0
        }, completion: completion)  }
    
    
    func fadeOut(_ duration: TimeInterval = 0.1, delay: TimeInterval = 0.3, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.showHideTransitionViews, animations: {
            self.imageView.alpha = 0.5
        }, completion: completion)
    }
}
