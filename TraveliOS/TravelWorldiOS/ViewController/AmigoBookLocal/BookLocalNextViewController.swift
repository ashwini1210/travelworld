//
//  BookLocalNextViewController.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 19/08/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SwiftyJSON

class BookLocalNextViewController: BaseVC,UITextFieldDelegate,UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var bookingTimeButton: UIButton!
    @IBOutlet weak var view1: UIView! //Interested view
    @IBOutlet weak var view2: UIView! //Booking time view
    @IBOutlet weak var view3: UIView! //duration view
    @IBOutlet weak var view4: UIView! //adult view
    @IBOutlet weak var view5: UIView! //childeren view
    @IBOutlet weak var selectedCity: UILabel!
    @IBOutlet weak var selectedDate: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var adultsLabel: UILabel!
    @IBOutlet weak var childrenLabel: UILabel!
    @IBOutlet weak var hrsLabel: UILabel!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var totalCostLabel: UILabel!
    @IBOutlet weak var dec1: UIButton!
    @IBOutlet weak var inc1: UIButton!
    @IBOutlet weak var dec2: UIButton!
    @IBOutlet weak var inc2: UIButton!
    @IBOutlet weak var dec3: UIButton!
    @IBOutlet weak var inc3: UIButton!
    @IBOutlet weak var interestTableview: UITableView!
    @IBOutlet weak var interestTextView: UITextField!
    
    var timePickerView: TimePickerView!
    var cityTimeDictionary : Dictionary<String, Any>?
    let currency = AppPreference.getInstance().getString(AppConstants.CURRENCY_SIGN, defaultvalue: "")
    let currencyRate = AppPreference.getInstance().getInt(AppConstants.CURRENCY_RATE, defaultvalue: 0)
    var basePrice : Int = 0
    var interest = [String]()
    var filterInterest = [String]()
    var interestModel : InterestModel? = nil
    var frame : CGRect?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeViews()
        registerTableView()
        initializeModel()
        borderViews()
        hideKeyboardWhenTappedAround()
        showTextInSuperScript()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     * Method to Register Table view
     */
    func registerTableView(){
        interestTableview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        interestTableview.isHidden = true //Initially hide tableview
    }
    
    /*
     * Method to initialize all views
     */
    func initializeViews(){
        //Increase scroll view size as per screen
        self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height + 10)
        frame = self.interestTableview.frame
        interestTableview.isHidden = true
        AppUtils.setBottomLine(borderColor: UIColor.black, uiView: interestTextView)
        
        //Initialize Custom TimePickerview
        timePickerView = (TimePickerView.instanceFromNib() as! TimePickerView)
        timePickerView.bookLocalNextViewController = self
        self.view.addSubview(timePickerView)
        timePickerView.isHidden = true //Initially hide timepicker view
        
        //Set date and City from previous screen
        selectedCity.text = cityTimeDictionary!["city"] as? String
        selectedDate.text = cityTimeDictionary!["time"] as? String
        
        //Set initial values of label
        durationLabel.text = "2"
        adultsLabel.text = "1"
        childrenLabel.text = "1"
        basePrice = 15 * currencyRate
        totalCostLabel.text = "Total Price: \(Double(30 * currencyRate))"
    }
    
    func initializeModel(){
        interestModel = InterestModel.init(delegate: self)
        interestModel!.requestForInterest()
    }
    
    /*
     * Method to Highlight and border view
     */
    func borderViews(){
        AppUtils.shadowView(view: view1, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        AppUtils.shadowView(view: view2, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        AppUtils.shadowView(view: view3, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        AppUtils.shadowView(view: view4, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        AppUtils.shadowView(view: view5, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        
        let colour = AppUtils.hexStringToUIColor(hex: "#0F6A5B")
        AppUtils.borderView(view: dec1, borderWidth: 1, borderColour: colour.cgColor)
        AppUtils.borderView(view: dec2, borderWidth: 1, borderColour: colour.cgColor)
        AppUtils.borderView(view: dec3, borderWidth: 1, borderColour: colour.cgColor)
        AppUtils.borderView(view: inc1, borderWidth: 1, borderColour: colour.cgColor)
        AppUtils.borderView(view: inc2, borderWidth: 1, borderColour: colour.cgColor)
        AppUtils.borderView(view: inc3, borderWidth: 1, borderColour: colour.cgColor)
        AppUtils.cornerRadius(view: backButton, roundCornerWidth: 5)
        AppUtils.cornerRadius(view: proceedButton, roundCornerWidth: 5)
    }
    
    func showTextInSuperScript(){
        let font:UIFont? = UIFont(name: "Helvetica", size:0)
        let fontSuper:UIFont? = UIFont(name: "Helvetica", size:10)
        let attString:NSMutableAttributedString = NSMutableAttributedString(string: ".hrs", attributes: [.font:font!])
        attString.setAttributes([.font:fontSuper!,.baselineOffset:10], range: NSRange(location:1,length:3))
        hrsLabel.attributedText = attString
    }
    
    
    @IBAction func bookingTimeBtnClk(_ sender: Any) {
        self.timePickerView.getTime()
        self.timePickerView.isHidden = false
    }
    
    @IBAction func decreaseButtonCliked(_ sender: Any) {
        let button = sender as! UIButton
        //Decrease count of Tour Duration
        if(button.tag == 1){
            var tourDuration : Int = Int(durationLabel.text!)!
            if(tourDuration > 2){
                tourDuration = tourDuration - 1
            }
            durationLabel.text! = "\(tourDuration)"
            calculatePrice()
        }
            //Decrease count of Adults
        else if(button.tag == 2){
            var adultsCount = Int(adultsLabel.text!)
            if(adultsCount! > 1){
                adultsCount = adultsCount! - 1
            }
            adultsLabel.text! = "\(adultsCount!)"
            calculatePrice()
        }
            //Decrease count of Children
        else if(button.tag == 3){
            var childrenCount = Int(childrenLabel.text!)
            if(childrenCount! > 1){
                childrenCount = childrenCount! - 1
            }
            childrenLabel.text! = "\(childrenCount!)"
        }
    }
    
    @IBAction func increaseButtonClicked(_ sender: Any) {
        let button = sender as! UIButton
        //Increase count of Tour Duration
        if(button.tag == 1){
            var tourDuration : Int = Int(durationLabel.text!)!
            if(tourDuration >= 2){
                tourDuration = tourDuration + 1
            }
            durationLabel.text! = "\(tourDuration)"
            calculatePrice()
        }
            //Increase count of Adults
        else if(button.tag == 2){
            var adultsCount = Int(adultsLabel.text!)
            if(adultsCount! >= 1){
                adultsCount = adultsCount! + 1
            }
            adultsLabel.text! = "\(adultsCount!)"
            calculatePrice()
        }
            //Increase count of Children
        else if(button.tag == 3){
            var childrenCount = Int(childrenLabel.text!)
            if(childrenCount! >= 1){
                childrenCount = childrenCount! + 1
            }
            childrenLabel.text! = "\(childrenCount!)"
        }
        
    }
    
    func calculatePrice(){
        if(Int(adultsLabel.text!) == 1){
            totalCostLabel.text = "Total Price: \(Double(Int(durationLabel.text!)! * basePrice))"
        }else{
            let price = (Int(durationLabel.text!)! * basePrice) + (basePrice/2 * (Int(adultsLabel.text!)!-1)*(Int(durationLabel.text!)!))
            totalCostLabel.text = "Total Price: \(Double(price))" 
        }
    }
    
    @IBAction func proceedButtonClicked(_ sender: Any) {
        if(interestTextView.text == ""){
            self.displayAlert(title: "", message: AppConstants.ENTER_INTEREST_ERROR_MSG, btn1: AppConstants.OK_BUTTON)
        }else if (bookingTimeButton.titleLabel!.text == "Booking Time"){
            self.displayAlert(title: "", message: AppConstants.SELECT_PROPER_ERROR_MSG + " booking time", btn1: AppConstants.OK_BUTTON)
        }else{
            self.displayConfirmationAlert(title: AppConstants.BOOKING_CONFIRM_TITLE, message: AppConstants.BOOKING_CONFIRM_MSG, btn1: "NO", btn2: "YES", handler: confirmBooking)
        }
    }
    
    
    func confirmBooking(action: UIAlertAction) {
        //Hit on server for booking confirm
        var cityId = ""
        let searchCityModel = ApplicationManager.getInstance.searchCityModel
        if(searchCityModel != nil){
            let index = cityTimeDictionary!["index"] as? Int
            let searchCityPojo = searchCityModel!.SearchCityPojoArray[index!]
            cityId = searchCityPojo.cities_id!
        }
        
        var data = [String:String]()
        data["interests"] = "music"
        data["duration"] = durationLabel.text
        data["child"] = childrenLabel.text
        data["adult"] = adultsLabel.text
        data["booking_date"] = cityTimeDictionary!["time"] as? String
        data["booking_time"] = bookingTimeButton.titleLabel!.text
        data["city_id"] = cityId
        
        let price = totalCostLabel.text?.components(separatedBy: "Total Price: ")
        if(price!.count > 1){
            data["total_amount"] = price![1]
        }
        
        let addBookingModel = AddAmigoBookingModel(delegate: self)
        addBookingModel.requestForAddBooking(data: data)
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        self.onBackNavigation()
    }
    
    override func onHttpResponse(isSuccess: Bool, responseJson: JSON, error: AnyObject!, anyIdentifier: String) {
        if(isSuccess){
            if(anyIdentifier == AppConstants.ADD_AMIGO_BOOKING){
                self.displayAlert(title: "", message: AppConstants.BOOKING_SUCCESS_MSG, btn1: AppConstants.OK_BUTTON)
                let price = totalCostLabel.text?.components(separatedBy: "Total Price: ")
                if(price!.count > 1){
                    var data = [String:Any]()
                    data["bookingId"] = responseJson["data"]["data"]["booking_id"].stringValue
                    data["price"] = price![1]
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                        self.launchViewController(viewControllerName: AppConstants.PaymentViewController, dataToPass: data)
                    }
                }
            }
            else if(anyIdentifier == AppConstants.INTEREST){
                interest = interestModel!.interest
            }
        }else{
            if(anyIdentifier == AppConstants.ADD_AMIGO_BOOKING){
                self.showToast(message: AppConstants.TRY_AGAIN_MSG)
                
            }
        }
    }
    
    /*
     * Hide keyboard on outer touch
     */
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    /*
     * Close keyboard on return button
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        interestTextView.resignFirstResponder()
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        filterContentForSearchText(searchText: textField.text!)
        interestTableview.isHidden = false
        return true
    }
    
    
    func filterContentForSearchText(searchText: String) {
        filterInterest = interest.filter { team in
            let contain = team.lowercased().contains(searchText.lowercased())
            return contain
        }
        adjustTableViewHeightForSearch()
        interestTableview.reloadData()
    }
    
    /*
     * Table view callback
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(filterInterest.count == 0){
            return 0
        }else{
            return filterInterest.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if(filterInterest.count != 0){
            cell.textLabel?.text = filterInterest[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(filterInterest.count != 0){
            interestTextView.text = filterInterest[indexPath.row]
        }
        interestTableview.isHidden = true
    }
    
    /*
     * Method to adjust tableview height as per search field data
     */
    func adjustTableViewHeightForSearch(){
        if(filterInterest.count<5){
            DispatchQueue.main.async {
                //This code will run in the main thread:
                var frame1 = self.frame
                frame1!.size.height = self.interestTableview.contentSize.height
                self.interestTableview.frame = frame1!
            }
        }else{
            self.interestTableview.frame = frame!
        }
    }
}
