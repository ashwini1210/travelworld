//
//  BookLocalViewController.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 07/08/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON

class BookLocalViewController: BaseVC, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var calenderView: UIView!
    @IBOutlet weak var calenderButton: UIButton!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    var pickerView: PickerView!
    var searchedCountry = [String]()
    var searching = false
    var placesModel : SearchCityModel? = nil
    var isResponseFromModel = false
    var frame : CGRect?
    var indexOfCityArray = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableView()
        initailizeViews()
        borderViews()
        hideKeyboardWhenTappedAround()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    /*
     * Initialize all view on screen load
     */
    func initailizeViews(){
        frame = self.tableView.frame
        searchBar.becomeFirstResponder() //Focus on searchbar
        AppUtils.cornerRadius(view: proceedButton, roundCornerWidth: 5)
        AppUtils.cornerRadius(view: backButton, roundCornerWidth: 5)
        
        //Initialize Custom Pickerview
        pickerView = (PickerView.instanceFromNib() as! PickerView)
        pickerView.bookLocalViewController = self
        self.view.addSubview(pickerView)
        pickerView.isHidden = true //Initially hide calender view
    }
    
    /*
     * Method to Register Table view
     */
    func registerTableView(){
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.isHidden = true //Initially hide tableview
    }
    
    /*
     * Method to Highlight and border view
     */
    func borderViews(){
        AppUtils.shadowView(view: searchBar, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        AppUtils.shadowView(view: calenderView, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        AppUtils.shadowView(view: tableView, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        AppUtils.cornerRadius(view: tableView, roundCornerWidth: 5)
    }
    
    /*
     * Table view callback
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching {
            return searchedCountry.count
        } else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if searching {
            cell.textLabel?.text = searchedCountry[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchBar.text = searchedCountry[indexPath.row]
        indexOfCityArray = indexPath.row
        tableView.isHidden = true 
    }
    
    /*
     * Method to adjust tableview height as per search field data
     */
    func adjustTableViewHeightForSearch(){
        if(searchedCountry.count<5){
            DispatchQueue.main.async {
                //This code will run in the main thread:
                var frame1 = self.frame
                frame1!.size.height = self.tableView.contentSize.height
                self.tableView.frame = frame1!
            }
        }else{
             self.tableView.frame = frame!
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        searchBar.text = ""
        tableView.isHidden = true
    }
    
    /*
     * Search Bar Callback
     */
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if(searchText.count > 1){
            tableView.isHidden = false //Show tableview while searching
            searching = true
            if(!isResponseFromModel){
                isResponseFromModel = true
                initalizeModel(searchChar: searchText)
                tableView.reloadData()
            }
        }
    }
    
    func initalizeModel(searchChar: String?){
        placesModel = SearchCityModel.init(delegate: self)
        ApplicationManager.getInstance.searchCityModel = placesModel
        if(searchChar != nil){
            var data = [
                "q" : searchChar! as Any,
                ] as [String:Any]?
            placesModel!.requestForCity(searchChar: data)
            data = nil
        }
    }
    
    override func onHttpResponse(isSuccess: Bool, responseJson: JSON, error: AnyObject!, anyIdentifier: String) {
        searchedCountry = placesModel!.cityArray
        adjustTableViewHeightForSearch()
        isResponseFromModel = false
        tableView.reloadData()
    }
    
    
    /*
     * Button to show calender view
     */
    @IBAction func showCalenderBtnClk(_ sender: Any) {
        self.pickerView.getYear()
        self.pickerView.getDayMonth()
        self.pickerView.isHidden = false
    }
    
    /*
     * Hide keyboard on outer touch
     */
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func proceedButtonClicked(_ sender: Any) {
        if(searchBar.text == ""){
            self.displayAlert(title: "", message: AppConstants.SELECT_PROPER_ERROR_MSG + " city", btn1: AppConstants.OK_BUTTON)
        }
        else if(calenderButton.titleLabel!.text == "Choose Date"){
            self.displayAlert(title: "", message: AppConstants.SELECT_PROPER_ERROR_MSG + " date", btn1: AppConstants.OK_BUTTON)
        }
        else{
            let isLogin = AppPreference.getInstance().getBoolean(AppConstants.IS_LOGIN, defaultvalue: false)
            if(!isLogin){
                self.launchViewController(viewControllerName: AppConstants.LoginViewController)
            }else{
                let dict = [
                    "city" : searchBar.text!,
                    "time" : calenderButton.titleLabel!.text!,
                    "index" : indexOfCityArray
                    ] as [String:Any]?
                self.launchViewController(viewControllerName: AppConstants.BookLocalNextViewController,dataToPass: dict)
            }
        }
        
    }
    
    @IBAction func onBackButtonClicked(_ sender: Any) {
        self.onBackNavigation()
    }
}

