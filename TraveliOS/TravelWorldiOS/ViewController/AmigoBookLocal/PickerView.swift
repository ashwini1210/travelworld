//
//  PickerView.swift
//  LeamigoiOS
//
//  Created by Apple on 09/08/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit

class PickerView: UIView {
    
    @IBOutlet weak var pickerBackgroundView: UIView!
    @IBOutlet weak var pickerManiView: UIView!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var selectedDate : String?
    var bookLocalViewController : BookLocalViewController?
    var bookExperienceViewController : BookExperienceViewController?
    
    var dateFormatter = DateFormatter()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        getYear()
        getDayMonth()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "PickerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    @IBAction func onPickerCancelBtnClk(_ sender: Any) {
        self.isHidden = true
    }
    
    @IBAction func onPickrOkBtnClk(_ sender: Any) {
        self.isHidden = true
        if(selectedDate != nil){
            if(self.bookLocalViewController != nil){
                self.bookLocalViewController!.calenderButton.setTitle(selectedDate,for: .normal)
            }else if(self.bookExperienceViewController != nil){
                 self.bookExperienceViewController!.calenderButton.setTitle(selectedDate,for: .normal)
            }
        }else{
            getSelectedDate()
            if(self.bookLocalViewController != nil){
                self.bookLocalViewController!.calenderButton.setTitle(selectedDate,for: .normal)
            }
            else if(self.bookExperienceViewController != nil){
                 self.bookExperienceViewController!.calenderButton.setTitle(selectedDate,for: .normal)
            }
        }
    }
    
    @IBAction func onPickerDateChange(_ sender: Any) {
        getYear()
        getDayMonth()
        getSelectedDate()
    }
    
    func getYear(){
        dateFormatter.dateFormat = "yyyy"
        yearLabel.text = dateFormatter.string(from: datePicker.date)
    }
    
    func getDayMonth(){
        dateFormatter.dateFormat = "EEE, dd MMM"
        dateLabel.text = dateFormatter.string(from: datePicker.date)
    }
    
    func getSelectedDate(){
        dateFormatter.dateFormat = "yyyy-MM-dd"
        selectedDate = dateFormatter.string(from: datePicker.date)
    }
    
}
