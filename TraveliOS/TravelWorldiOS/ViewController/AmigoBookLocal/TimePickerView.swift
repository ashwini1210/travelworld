//
//  TimePickerView.swift
//  LeamigoiOS
//
//  Created by Apple on 09/08/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit

class TimePickerView: UIView {
    
    @IBOutlet weak var pickerBackgroundView: UIView!
    @IBOutlet weak var pickerManiView: UIView!
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var timeLabel: UILabel!
    
    var selectedTime : String?
    var bookLocalNextViewController : BookLocalNextViewController?
    var bookExperienceViewController : BookExperienceViewController?
    var dateFormatter = DateFormatter()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        print("Init of TimePickerView")
        getTime()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "TimePickerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    @IBAction func onPickerCancelBtnClk(_ sender: Any) {
        self.isHidden = true
    }
    
    @IBAction func onPickrOkBtnClk(_ sender: Any) {
        self.isHidden = true
        if(selectedTime != nil){
            if(self.bookLocalNextViewController != nil){
                self.bookLocalNextViewController!.bookingTimeButton.setTitle(selectedTime,for: .normal)
            }else if(self.bookExperienceViewController != nil){
                self.bookExperienceViewController!.bookingTimeButton.setTitle(selectedTime,for: .normal)
            }
        }else{
            if(self.bookLocalNextViewController != nil){
                self.bookLocalNextViewController!.bookingTimeButton.setTitle(selectedTime,for: .normal)
            }else if(self.bookExperienceViewController != nil){
                self.bookExperienceViewController!.bookingTimeButton.setTitle(selectedTime,for: .normal)
            }
        }
    }
    
    @IBAction func onPickerDateChange(_ sender: Any) {
        getTime()
    }
    
    func getTime(){
        dateFormatter.dateFormat = "hh:mm a"
        selectedTime = dateFormatter.string(from: timePicker.date)
        timeLabel.text = selectedTime
    }
    
    
    
}
