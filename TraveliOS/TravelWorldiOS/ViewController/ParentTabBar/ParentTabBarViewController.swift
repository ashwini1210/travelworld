//
//  ParentTabBarViewController.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 28/08/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit

class ParentTabBarViewController: UITabBarController, UITabBarControllerDelegate {
    
    var homeViewController : HomeViewController?
    var destinationViewController : DestinationViewController?
    var wishListViewController : WishListViewController?
    var bookingViewController : BookingViewController?
    var profileViewController : ProfileViewController?
    var subViewController : [UIViewController] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // changeStatusBarColour()
        initailizeViews()
        setTabBarImages()
        passDataInBookingViewController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initailizeViews(){
        homeViewController = HomeViewController()
        destinationViewController = DestinationViewController()
        wishListViewController = WishListViewController()
        bookingViewController = BookingViewController()
        profileViewController = ProfileViewController()
        
        subViewController.append(homeViewController!)
        subViewController.append(destinationViewController!)
        subViewController.append(wishListViewController!)
        subViewController.append(bookingViewController!)
        subViewController.append(profileViewController!)
        
        self.setViewControllers(subViewController, animated: true)
        self.selectedIndex = 1
        self.selectedViewController = homeViewController
        tabBar.tintColor =  AppUtils.hexStringToUIColor(hex: "#0F6A5B")
    }
    
    /*
     * Method to change colour of top status bar
     */
    func changeStatusBarColour(){
        let statusBarBGView = UIView(frame: UIApplication.shared.statusBarFrame)
        statusBarBGView.backgroundColor = AppUtils.hexStringToUIColor(hex: "#0F6A5B") //color of your choice
        view.addSubview(statusBarBGView)
    }
    /*
     * Change tab bar images
     */
    func setTabBarImages(){
        
        homeViewController?.tabBarItem = UITabBarItem(title: "Home", image: UIImage(named: "home.png"), selectedImage: UIImage(named: "home.png"))
        destinationViewController?.tabBarItem = UITabBarItem(title: "Destination", image: UIImage(named: "destination.png"), selectedImage: UIImage(named: "destination.png"))
//        destinationViewController?.tabBarItem = UITabBarItem(title: "Destination", image: UIImage(named: "dest.png")!.withRenderingMode(UIImage.RenderingMode.automatic), selectedImage: UIImage(named: "destination.png"))
        wishListViewController?.tabBarItem = UITabBarItem(title: "Wishlist", image: UIImage(named: "wishlist.png"), selectedImage: UIImage(named: "wishlist.png"))
        bookingViewController?.tabBarItem = UITabBarItem(title: "Bookings", image: UIImage(named: "booking.png"), selectedImage: UIImage(named: "booking.png"))
        profileViewController?.tabBarItem = UITabBarItem(title: "Profile", image: UIImage(named: "profile.png"), selectedImage: UIImage(named: "profile.png"))
    }
    
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 1:
            tabBar.tintColor =  AppUtils.hexStringToUIColor(hex: "#0F6A5B")
        case 2:
            tabBar.tintColor =  AppUtils.hexStringToUIColor(hex: "#0F6A5B")
        case 3:
            tabBar.tintColor =  AppUtils.hexStringToUIColor(hex: "#0F6A5B")
        case 4:
            tabBar.tintColor =  AppUtils.hexStringToUIColor(hex: "#0F6A5B")
        case 5:
            tabBar.tintColor =  AppUtils.hexStringToUIColor(hex: "#0F6A5B")
        default:
            tabBar.tintColor =  AppUtils.hexStringToUIColor(hex: "#0F6A5B")
        }
    }
    
    /*
     * Pass parent class object in BookingViewController fro further use
     */
    func passDataInBookingViewController(){
        self.homeViewController?.parentTabBarViewController = self
        self.bookingViewController?.parentTabBarViewController = self
    }
}
