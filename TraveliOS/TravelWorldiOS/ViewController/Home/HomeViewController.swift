//
//  HomeViewController.swift
//  LeamingoiOS
//
//  Created by Apple on 03/08/18.
//  Copyright © 2018 Appleechnopurple.com. All rights reserved.
//

import UIKit
import SwiftyJSON

class HomeViewController: BaseVC, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var bookLocalButton: UIButton!
    @IBOutlet weak var findOutMoreButton: UIButton!
    @IBOutlet weak var imagesImageView: UIImageView!
    
    @IBOutlet weak var topDestinationcollectionView: UICollectionView!
    @IBOutlet weak var recommendedCollcetionView: UICollectionView!
    @IBOutlet weak var festivalsCollectionView: UICollectionView!
    @IBOutlet weak var engagingCollectionView: UICollectionView!
    @IBOutlet weak var activityCollectionView: UICollectionView!
    @IBOutlet weak var offerCollectionView: UICollectionView!
    
    var carousalImagesArray = [UIImage]()
    var destinationImagesArray = [UIImage]()
    var recommendedImagesArray = [UIImage]()
    var festivalImagesArray = [UIImage]()
    var engagingImagesArray = [UIImage]()
    var activityImagesArray = [UIImage]()
    var offerImagesArray = [UIImage]()
    
    let reuseTopDestinationIdentifier = "topDestinationCell"
    let reuseRecommendesIdentifier = "recommendedCell"
    let reuseEngagingIdentifier = "engagingCell"
    let reuseOfferIdentifier = "offerCell"
    
    @IBOutlet weak var topDestinationLoader: UIActivityIndicatorView!
    @IBOutlet weak var recommendedLoader: UIActivityIndicatorView!
    @IBOutlet weak var festivalLoader: UIActivityIndicatorView!
    @IBOutlet weak var blogLoader: UIActivityIndicatorView!
    @IBOutlet weak var activityLoder: UIActivityIndicatorView!
    @IBOutlet weak var offerLoader: UIActivityIndicatorView!
    
    var mainFrameSizeMaintain : CGRect?
    var parentTabBarViewController : ParentTabBarViewController?
    let offersDB = OffersDB()
    let activityDB = ActivityDB()
    let blogsDB = BlogsDB()
    let festivalDB = FestivalDB()
    let topExperienceDB = TopExperienceDB()
    let topDestinationDB = TopDestinationDB()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCollectionCell()
        startLoader()
        initializeModels()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillLayoutSubviews() {
        //super .viewWillLayoutSubviews()
        
        setImageWithTimer()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        initializeViews()
        recommendedCollcetionView.reloadData()
        festivalsCollectionView.reloadData()
        activityCollectionView.reloadData()        
    }
    
    func registerCollectionCell(){     
        topDestinationcollectionView.delegate = self
        topDestinationcollectionView.dataSource = self
        topDestinationcollectionView.register(UINib.init(nibName: "TopDestinationCollectionCell", bundle: nil), forCellWithReuseIdentifier: reuseTopDestinationIdentifier)
        
        recommendedCollcetionView.delegate = self
        recommendedCollcetionView.dataSource = self
        recommendedCollcetionView.register(UINib.init(nibName: "RecommendedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseRecommendesIdentifier)
        
        festivalsCollectionView.delegate = self
        festivalsCollectionView.dataSource = self
        festivalsCollectionView.register(UINib.init(nibName: "RecommendedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseRecommendesIdentifier)
        
        engagingCollectionView.delegate = self
        engagingCollectionView.dataSource = self
        engagingCollectionView.register(UINib.init(nibName: "EngagingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseEngagingIdentifier)
        
        activityCollectionView.delegate = self
        activityCollectionView.dataSource = self
        activityCollectionView.register(UINib.init(nibName: "RecommendedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseRecommendesIdentifier)
        
        offerCollectionView.delegate = self
        offerCollectionView.dataSource = self
        offerCollectionView.register(UINib.init(nibName: "OfferCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseOfferIdentifier)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //This code is written, bcz on normal scrolling all cell cut half even though it is set to full screen width. It actually stops any running animation.
        // scrollView.setContentOffset(scrollView.contentOffset, animated: false)
    }
    

    /*
     * Method to initialize all views
     */
    func initializeViews(){
        //Increase scroll view size as per screen
        self.mainView.frame = CGRect(x: self.mainView.frame.origin.x, y: self.mainView.frame.origin.y, width: self.mainView.frame.width, height: self.mainView.frame.height - 50.0)
        mainFrameSizeMaintain = self.mainView.frame
        self.scrollview.contentSize = CGSize(width: self.view.frame.width, height: self.mainView.frame.height)
        
        //Change corner of button
        AppUtils.cornerRadius(view: bookLocalButton, roundCornerWidth: 7)
        AppUtils.cornerRadius(view: findOutMoreButton, roundCornerWidth: 7)
        AppUtils.borderView(view: findOutMoreButton, borderWidth: 1, borderColour: UIColor.white.cgColor)
        
    }
    
    func startLoader(){
        self.topDestinationLoader.startAnimating()
        recommendedLoader.startAnimating()
        festivalLoader.startAnimating()
        blogLoader.startAnimating()
        activityLoder.startAnimating()
        offerLoader.startAnimating()
    }
    
    
    func initializeModels(){
        initializeTopdestinationModel()
        initializeTopExperienceModel()
        initializeFestivalModel()
        initializeBlogsModel()
        initializeActivityModel()
        initializeOffersModel()
        initializeHomeCarousalModel()
        
    }
    
    /*
     * Method to set different images every second
     */
    func setImageWithTimer(){
        imagesImageView.animationImages = carousalImagesArray
        imagesImageView.animationDuration = 10
        imagesImageView.startAnimating()        
    }
    
    @IBAction func bookLabelButtonClicked(_ sender: Any) {
        self.launchViewController(viewControllerName: AppConstants.BookLocalViewController)
    }
    
    @IBAction func findOutMoreButtonClicked(_ sender: Any) {
        self.launchViewController(viewControllerName: AppConstants.FindOutMoreViewController)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == topDestinationcollectionView){
            return destinationImagesArray.count
        }
        else if(collectionView == recommendedCollcetionView){
            return recommendedImagesArray.count
        }
        else if(collectionView == festivalsCollectionView){
            return festivalImagesArray.count
        }
        else if(collectionView == engagingCollectionView){
            return engagingImagesArray.count
        }
        else if(collectionView == activityCollectionView){
            return activityImagesArray.count
        }
        else if(collectionView == offerCollectionView){
            return offerImagesArray.count
        }
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == topDestinationcollectionView){
            let itemWidth = collectionView.bounds.size.width/2 
            let itemHeight = collectionView.bounds.size.height
            return CGSize(width: itemWidth, height: itemHeight)
        }
        else if(collectionView == recommendedCollcetionView || collectionView == festivalsCollectionView || collectionView == engagingCollectionView || collectionView == activityCollectionView || collectionView == offerCollectionView){
            let itemWidth = collectionView.bounds.size.width
            let itemHeight = collectionView.bounds.size.height
            return CGSize(width: itemWidth, height: itemHeight)
        }
        return CGSize(width: 0, height: 0)
    }
    
    
    var topDestinationCollectionCell : TopDestinationCollectionCell? = nil
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : UICollectionViewCell? = nil
        
        //TOP DESTINATION
        if(collectionView == topDestinationcollectionView){
            topDestinationCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseTopDestinationIdentifier, for: indexPath) as? TopDestinationCollectionCell
            topDestinationCollectionCell!.setDataInCell(homeViewController: self, indexPath: indexPath)
            return topDestinationCollectionCell!
        }
            
            //TOP EXPERIENCE
        else if (collectionView == recommendedCollcetionView){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseRecommendesIdentifier, for: indexPath) as! RecommendedCollectionViewCell
            cell.setExperienceDataInCell(homeViewController: self, indexPath: indexPath)
            
            return cell
        }
            
            //FESTIVAL
        else if (collectionView == festivalsCollectionView){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseRecommendesIdentifier, for: indexPath) as! RecommendedCollectionViewCell
            cell.setFestivalDataInCell(homeViewController: self, indexPath: indexPath)
            return cell
        }
            
            //BLOGS
        else if (collectionView == engagingCollectionView){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseEngagingIdentifier, for: indexPath) as! EngagingCollectionViewCell
            cell.setBlogsDataInCell(homeViewController: self, indexPath: indexPath)
            return cell
        }
            
            //ACTIVITY
        else if (collectionView == activityCollectionView){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseRecommendesIdentifier, for: indexPath) as! RecommendedCollectionViewCell
            cell.setActivityDataInCell(homeViewController: self, indexPath: indexPath)
            return cell
        }
            
            //OFFERS
        else if (collectionView == offerCollectionView){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseOfferIdentifier, for: indexPath) as! OfferCollectionViewCell
            cell.setOffersDataInCell(homeViewController: self, indexPath: indexPath)
            return cell
        }
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var data = [String:Any]()
        
        //TOP DESTINATION 
        if(collectionView == topDestinationcollectionView){
            data["identifier"] = AppConstants.TOP_DESTINATION as String
            let topDestinationDao =  topDestinationDB.findAll().first
            if(topDestinationDao != nil){
                let destinationListDao = topDestinationDao?.destinationListDao[indexPath.row]
                data["city"] = destinationListDao!.text
                parentTabBarViewController!.destinationViewController?.dataFromHome = data
                parentTabBarViewController!.selectedViewController =  parentTabBarViewController!.destinationViewController
            }
        }
            
            //TOP EXPERIENCE
        else if (collectionView == recommendedCollcetionView){
            data["identifier"] = AppConstants.TOP_EXPERIENCE as String
            let topExperienceDao =  topExperienceDB.findAll().first
            if(topExperienceDao != nil){
                let esperienceDao = topExperienceDao?.experienceListDao[indexPath.row]
                data["id"] = esperienceDao!.id
            }
            self.launchViewController(viewControllerName: AppConstants.DetailViewController,dataToPass: data)
        }
            //FESTIVAL
        else if (collectionView == festivalsCollectionView){
            data["identifier"] = AppConstants.FESTIVAL as String
            let festivalDao =  festivalDB.findAll().first
            if(festivalDao != nil){
                let festivalListDao = festivalDao!.festivalListDao[indexPath.row]
                data["id"] = festivalListDao.id
            }
            self.launchViewController(viewControllerName: AppConstants.FestivalDetailViewController,dataToPass: data)
        }
            //BLOGS remain
        else if (collectionView == engagingCollectionView){
            data["identifier"] = AppConstants.BLOGS as String
            let blogDao =  blogsDB.findAll().first
            if(blogDao != nil){
                let blogListDao = blogDao!.blogListDao[indexPath.row]
                data["id"] = blogListDao.id
            }
            
        }
            //ACTIVITY
        else if (collectionView == activityCollectionView){
            data["identifier"] = AppConstants.TRENDING_ACTIVITY as String
            let activityDao =  activityDB.findAll().first
            if(activityDao != nil){
                let activityListDao = activityDao!.activityListDao[indexPath.row]
                data["id"] = activityListDao.id
            }
            self.launchViewController(viewControllerName: AppConstants.DetailViewController,dataToPass: data)
        }
            
            //OFFERS
        else if (collectionView == offerCollectionView){
            data["identifier"] = AppConstants.OFFERS as String
            //            let cell = collectionView.cellForItem(at: indexPath) as? OfferCollectionViewCell
            //            let offerCode = cell!.offerCodeLabel
            self.showToast(message: "Offer Name Copied")
            self.mainView.frame = mainFrameSizeMaintain!
            
        }
        
    }
    
    
    @IBAction func searchButtonClicked(_ sender: Any) {
        parentTabBarViewController!.selectedViewController =  parentTabBarViewController!.destinationViewController
    }
    
    /*
     * Callback of HTTP response from respective Models
     */
    override func onHttpResponse(isSuccess: Bool, responseJson: JSON, error: AnyObject!, anyIdentifier: String) {
        if anyIdentifier == AppConstants.HOME_CAROUSAL {
            onHomeCarousalResponse(isSuccess: isSuccess, responseJson: responseJson)
        }
        else if anyIdentifier == AppConstants.TOP_DESTINATION {
            onTopDestinationResponse(isSuccess: isSuccess, responseJson: responseJson)
        }
        else if anyIdentifier == AppConstants.TOP_EXPERIENCE {
            onTopExperienceResponse(isSuccess: isSuccess, responseJson: responseJson)
        }
        else if anyIdentifier == AppConstants.FESTIVAL {
            onFestivalResponse(isSuccess: isSuccess, responseJson: responseJson)
        }
        else if anyIdentifier == AppConstants.BLOGS {
            onBlogsResponse(isSuccess: isSuccess, responseJson: responseJson)
        }
        else if anyIdentifier == AppConstants.TRENDING_ACTIVITY {
            onTrendingActivityResponse(isSuccess: isSuccess, responseJson: responseJson)
        }
        else if anyIdentifier == AppConstants.OFFERS {
            onOffersResponse(isSuccess: isSuccess, responseJson: responseJson)
        }
    }
    
    /*
     * Callback of Image Download response from respective Models
     */
    override func onImageResponse(isSuccess: Bool?, image: UIImage?, error: AnyObject?, anyIdentifier: String?,id:String?) {
        if(anyIdentifier == AppConstants.HOME_CAROUSAL){
            if(image != nil){
                carousalImagesArray.append(image!)
            }
            setImageWithTimer()
        }
            
            //TOP DESTINATION IMAGE RESPONSE
        else if(anyIdentifier == AppConstants.TOP_DESTINATION){
            if(image != nil){
                let imageDaoList = topDestinationDB.findAll().first!.destinationListDao
                let resultPredicate = NSPredicate(format: "id = %@", id!)
                let index = imageDaoList.index(matching: resultPredicate)
                if(index != nil){
                    destinationImagesArray[index!] = image!
                    topDestinationDB.realm.beginWrite()
                    imageDaoList[index!].image_base64 = AppUtils.convertImageToBase64(image: image!)
                    topDestinationDB.realm.add(imageDaoList[index!], update: true)
                    try! topDestinationDB.realm.commitWrite()
                }
            }
            topDestinationcollectionView.reloadData()
        }
            
            //TOP EXPERIENCE IMAGE RESPONSE
        else if(anyIdentifier == AppConstants.TOP_EXPERIENCE){
            if(image != nil){
                let experienceListDao = topExperienceDB.findAll().first!.experienceListDao
                let resultPredicate = NSPredicate(format: "id = %@", id!)
                let index = experienceListDao.index(matching: resultPredicate)
                if(index != nil){
                    recommendedImagesArray[index!] = image!
                    topExperienceDB.realm.beginWrite()
                    experienceListDao[index!].image_base64 = AppUtils.convertImageToBase64(image: image!)
                    topExperienceDB.realm.add(experienceListDao[index!], update: true)
                    try! topExperienceDB.realm.commitWrite()
                }
            }
            recommendedCollcetionView.reloadData()
        }
            
            //TOP FESTIVAL IMAGE RESPONSE
        else if(anyIdentifier == AppConstants.FESTIVAL){
            if(image != nil){
                let festivalListDao = festivalDB.findAll().first!.festivalListDao
                let resultPredicate = NSPredicate(format: "id = %@", id!)
                let index = festivalListDao.index(matching: resultPredicate)
                if(index != nil){
                    festivalImagesArray[index!] = image!
                    festivalDB.realm.beginWrite()
                    festivalListDao[index!].image_base64 = AppUtils.convertImageToBase64(image: image!)
                    festivalDB.realm.add(festivalListDao[index!], update: true)
                    try! festivalDB.realm.commitWrite()
                }
            }
            festivalsCollectionView.reloadData()
        }
            
            //BLOGS IMAGE RESPONSE
        else if(anyIdentifier == AppConstants.BLOGS){
            if(image != nil){
                let blogListDao = blogsDB.findAll().first!.blogListDao
                let resultPredicate = NSPredicate(format: "id = %@", id!)
                let index = blogListDao.index(matching: resultPredicate)
                if(index != nil){
                    engagingImagesArray[index!] = image!
                    blogsDB.realm.beginWrite()
                    blogListDao[index!].image_base64 = AppUtils.convertImageToBase64(image: image!)
                    blogsDB.realm.add(blogListDao[index!], update: true)
                    try! blogsDB.realm.commitWrite()
                }
            }
            engagingCollectionView.reloadData()
        }
            
            //TRENDING ACTIVITY IMAGE RESPONSE
        else if(anyIdentifier == AppConstants.TRENDING_ACTIVITY){
            if(image != nil){
                let activityListDao = activityDB.findAll().first!.activityListDao
                let resultPredicate = NSPredicate(format: "id = %@", id!)
                let index = activityListDao.index(matching: resultPredicate)
                if(index != nil){
                    activityImagesArray[index!] = image!
                    activityDB.realm.beginWrite()
                    activityListDao[index!].image_base64 = AppUtils.convertImageToBase64(image: image!)
                    activityDB.realm.add(activityListDao[index!], update: true)
                    try! activityDB.realm.commitWrite()
                }
            }
            activityCollectionView.reloadData()
        }
            
            //OFFERS IMAGE RESPONSE
        else if(anyIdentifier == AppConstants.OFFERS){
            if(image != nil){
                let offersListDao = offersDB.findAll().first!.offersListDao
                let resultPredicate = NSPredicate(format: "id = %@", id!)
                let index = offersListDao.index(matching: resultPredicate)
                if(index != nil){
                    offerImagesArray[index!] = image!
                    offersDB.realm.beginWrite()
                    offersListDao[index!].image_base64 = AppUtils.convertImageToBase64(image: image!)
                    offersDB.realm.add(offersListDao[index!], update: true)
                    try! offersDB.realm.commitWrite()
                }
            }
            offerCollectionView.reloadData()
        }
    }
    
    
}
