//
//  HomeViewControllerExtension.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 09/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON


extension HomeViewController{
    
    //INITIALIZE TOP CAROUSAL MODEL
    func initializeHomeCarousalModel(){
        let homeCarousal = HomeCarousalModel.init(delegate: self)
        homeCarousal.requestForCarousal()
    }
    
    func onHomeCarousalResponse(isSuccess: Bool, responseJson: JSON!){
        
        if isSuccess {
            if responseJson["status"].boolValue{
                let imagesArray = HomeCarousalDB().findAll().first
                if(imagesArray != nil){
                    let imagesList = imagesArray!.imagesDaoList
                    for imagesDao in imagesList{
                        let downloadImages = DownloadImages.init(delegate: self)
                        downloadImages.downloadImageAsync(URLString: imagesDao.imageURL, placeHolderImage: UIImage(named: "carousal1.jpg")!,identifier: AppConstants.HOME_CAROUSAL, id: nil)
                    }
                }
            }
            else {
                print("Error to set images in HomeCarousal: \(responseJson["message"].stringValue)")
            }
        }
    }
    
    
    //INITIALIZE TOP DESTINATION MODEL
    func initializeTopdestinationModel(){
        let topDestinationModel = TopDestinationModel.init(delegate: self)
        topDestinationModel.requestForTopDestination()
    }
    
    func onTopDestinationResponse(isSuccess: Bool, responseJson: JSON!){
        self.topDestinationLoader.stopAnimating()
        self.topDestinationLoader.isHidden = true
        if isSuccess {
            if responseJson["status"].boolValue{
                let topDestinationDao = TopDestinationDB().findAll().first
                if(topDestinationDao != nil){
                    let imagesList = topDestinationDao!.destinationListDao
                    for imagesDao in imagesList{
                        let downloadImages = DownloadImages.init(delegate: self)
                        downloadImages.downloadImageAsync(URLString: imagesDao.imageURL, placeHolderImage: UIImage(named: "topDest1.jpg")!,identifier: AppConstants.TOP_DESTINATION, id: imagesDao.id)
                        destinationImagesArray.append(UIImage(named: "topDest1.jpg")!)
                    }
                    topDestinationcollectionView.reloadData()
                }
            }
        }
        else {
            print("Error to onTopDestinationResponse: \(responseJson["message"].stringValue)")
        }
    }
    
    //INITIALIZE TOP EXPERIENCE MODEL
    func initializeTopExperienceModel(){
        let experienceModel = ExperienceModel.init(delegate: self)
        experienceModel.requestForExperience()
    }
    
    func onTopExperienceResponse(isSuccess: Bool, responseJson: JSON!){
        self.recommendedLoader.stopAnimating()
        self.recommendedLoader.isHidden = true
        if isSuccess {
            if responseJson["status"].boolValue{
                let topExperienceDB = TopExperienceDB().findAll().first
                if(topExperienceDB != nil){
                    let experienceListDao = topExperienceDB!.experienceListDao
                    for experienceDao in experienceListDao{
                        let downloadImages = DownloadImages.init(delegate: self)
                        downloadImages.downloadImageAsync(URLString: experienceDao.cardimageURL, placeHolderImage: UIImage(named: "exp1.jpg")!,identifier: AppConstants.TOP_EXPERIENCE, id: experienceDao.id)
                        recommendedImagesArray.append(UIImage(named: "exp1.jpg")!)
                    }
                    recommendedCollcetionView.reloadData()
                }
            }
        }
        else {
            //print("Error to onTopExperienceResponse: \(responseJson["message"].stringValue)")
        }
    }
    
    //INITIALIZE FESTIVAL MODEL
    func initializeFestivalModel(){
        let festivalModel = FestivalModel.init(delegate: self)
        festivalModel.requestForFestival()
    }
    
    func onFestivalResponse(isSuccess: Bool, responseJson: JSON!){
        self.festivalLoader.stopAnimating()
        self.festivalLoader.isHidden = true
        if isSuccess {
            if responseJson["status"].boolValue{
                let festivalDB = FestivalDB().findAll().first
                if(festivalDB != nil){
                    let festivalListDao = festivalDB!.festivalListDao
                    for festivalDao in festivalListDao{
                        let downloadImages = DownloadImages.init(delegate: self)
                        downloadImages.downloadImageAsync(URLString: festivalDao.cardimageURL, placeHolderImage: UIImage(named: "festival2.jpeg")!,identifier: AppConstants.FESTIVAL, id: festivalDao.id)
                        festivalImagesArray.append(UIImage(named: "festival2.jpeg")!)
                    }
                    festivalsCollectionView.reloadData()
                }
            }
        }
        else {
            //print("Error to onTopExperienceResponse: \(responseJson["message"].stringValue)")
        }
    }
    
    //INITIALIZE BLOGS MODEL
    func initializeBlogsModel(){
        let blogsModel = BlogsModel.init(delegate: self)
        blogsModel.requestForBlogs()
    }
    
    func onBlogsResponse(isSuccess: Bool, responseJson: JSON!){
        self.blogLoader.stopAnimating()
        self.blogLoader.isHidden = true
        if isSuccess {
            if responseJson["status"].boolValue{
                let blogsDB = BlogsDB().findAll().first
                if(blogsDB != nil){
                    let blogListDao = blogsDB!.blogListDao
                    for blogList in blogListDao{
                        let downloadImages = DownloadImages.init(delegate: self)
                        downloadImages.downloadImageAsync(URLString: blogList.cardimageURL, placeHolderImage: UIImage(named: "blog1.jpg")!,identifier: AppConstants.BLOGS, id: blogList.id)
                        engagingImagesArray.append(UIImage(named: "blog1.jpg")!)
                    }
                    engagingCollectionView.reloadData()
                }
            }
        }
        else {
            //print("Error to onTopExperienceResponse: \(responseJson["message"].stringValue)")
        }
    }
    
    //INITIALIZE TRENDING ACTIVITY MODEL
    func initializeActivityModel(){
        let trendingActivityModel = TrendingActivityModel.init(delegate: self)
        trendingActivityModel.requestForTrendingActivity()
    }
    
    func onTrendingActivityResponse(isSuccess: Bool, responseJson: JSON!){
        self.activityLoder.stopAnimating()
        self.activityLoder.isHidden = true
        if isSuccess {
            if responseJson["status"].boolValue{
                let activityDao = ActivityDB().findAll().first
                if(activityDao != nil){
                    let activityListDao = activityDao!.activityListDao
                    for activityList in activityListDao{
                        let downloadImages = DownloadImages.init(delegate: self)
                        downloadImages.downloadImageAsync(URLString: activityList.cardImage, placeHolderImage: UIImage(named: "blog1.jpg")!,identifier: AppConstants.TRENDING_ACTIVITY, id: activityList.id)
                        activityImagesArray.append(UIImage(named: "blog1.jpg")!)
                    }
                    activityCollectionView.reloadData()
                }
            }
        }
        else {
            //print("Error to onTopExperienceResponse: \(responseJson["message"].stringValue)")
        }
    }
    
    //INITIALIZE OFFERS MODEL
    func initializeOffersModel(){
        let offersModel = OffersModel.init(delegate: self)
        offersModel.requestForOffers()
    }
    
    func onOffersResponse(isSuccess: Bool, responseJson: JSON!){
        self.offerLoader.stopAnimating()
        self.offerLoader.isHidden = true
        if isSuccess {
            if responseJson["status"].boolValue{
                let offersDao = OffersDB().findAll().first
                if(offersDao != nil){
                    let offersListDao = offersDao!.offersListDao
                    for offersList in offersListDao{
                        let downloadImages = DownloadImages.init(delegate: self)
                        downloadImages.downloadImageAsync(URLString: offersList.cardImage, placeHolderImage: UIImage(named: "blog1.jpg")!,identifier: AppConstants.OFFERS, id: offersList.id)
                        offerImagesArray.append(UIImage(named: "offer1.jpg")!)
                    }
                    offerCollectionView.reloadData()
                }
            }
        }
        else {
            print("Error to onOffersResponse: \(responseJson["message"].stringValue)")
        }
    }

}
