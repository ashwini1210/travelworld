//
//  EngagingCollectionViewCell.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 05/08/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit

class EngagingCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var engagingImageView: UIImageView!
    @IBOutlet weak var longDescriptionLabel: UILabel!
    @IBOutlet weak var shortDescriptionlabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    /*
     * Method to set BLOGS cell Data
     */
    func setBlogsDataInCell(homeViewController: HomeViewController,indexPath: IndexPath){
        self.engagingImageView.image = homeViewController.engagingImagesArray[indexPath.row]
        let blogsDao = BlogsDB().findAll().first
        if(blogsDao != nil){
            let blogListDao = blogsDao!.blogListDao
            self.longDescriptionLabel.text = blogListDao[indexPath.row].title
            self.shortDescriptionlabel.text = blogListDao[indexPath.row].writtenBy
        }
    }
}
