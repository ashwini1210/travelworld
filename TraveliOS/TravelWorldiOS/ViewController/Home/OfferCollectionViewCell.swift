//
//  OfferCollectionViewCell.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 05/08/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit

class OfferCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var offerImageView: UIImageView!
    @IBOutlet weak var offerDescriptionLabel: UILabel!
    @IBOutlet weak var offerCodeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setOffersDataInCell(homeViewController: HomeViewController,indexPath: IndexPath){
        self.offerImageView.image = homeViewController.offerImagesArray[indexPath.row]
        let offersDao = OffersDB().findAll().first
        if(offersDao != nil){
            let offersListDao = offersDao!.offersListDao
            self.offerDescriptionLabel.text = offersListDao[indexPath.row].desc
            self.offerCodeLabel.text = offersListDao[indexPath.row].name
        }
    }
}
