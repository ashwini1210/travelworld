//
//  TopDestinationCollectionCell.swift
//  LeamingoiOS
//
//  Created by Apple on 03/08/18.
//  Copyright © 2018 Appleechnopurple.com. All rights reserved.
//

import UIKit

class TopDestinationCollectionCell: UICollectionViewCell {

    @IBOutlet var destinationImage: UIImageView!
    @IBOutlet var destinationLabel: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setDataInCell(homeViewController: HomeViewController,indexPath: IndexPath){
        self.destinationImage.image = homeViewController.destinationImagesArray[indexPath.row]
        let imagesArray = TopDestinationDB().findAll().first
        if(imagesArray != nil){
            let imagesList = imagesArray!.destinationListDao
            self.destinationLabel.text = imagesList[indexPath.row].text
        }
    }
}
