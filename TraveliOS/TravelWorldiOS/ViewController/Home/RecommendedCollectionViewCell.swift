//
//  RecommendedCollectionViewCell.swift
//  LeamingoiOS
//
//  Created by Ashwini Bankar on 04/08/18.
//  Copyright © 2018 Appleechnopurple.com. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON

class RecommendedCollectionViewCell: UICollectionViewCell,HttpResponseProtocol  {
    
    @IBOutlet weak var wishlistButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var generalDescriptionLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var recommendedImageview: UIImageView!
    @IBOutlet weak var rating1: UIImageView!
    @IBOutlet weak var rating2: UIImageView!
    @IBOutlet weak var rating3: UIImageView!
    @IBOutlet weak var rating4: UIImageView!
    @IBOutlet weak var rating5: UIImageView!
    
    
    // this will be our "call back" action
    var wishlistBtnTapAction : (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    @IBAction func wishlistButtonClicked(_ sender: Any) {
        
        // use our "call back" action to tell the controller the button was tapped
        wishlistBtnTapAction?()
    }
    
    /*
     * Method to set Experience cell Data
     */
    func setExperienceDataInCell(homeViewController: HomeViewController,indexPath: IndexPath){
        self.recommendedImageview.image = homeViewController.recommendedImagesArray[indexPath.row]
        let topExperienceDao = TopExperienceDB().findAll().first
        if(topExperienceDao != nil){
            let experienceDao = topExperienceDao!.experienceListDao
            self.generalDescriptionLabel.text = experienceDao[indexPath.row].tagLine
            self.descriptionLabel.text = experienceDao[indexPath.row].heading
            self.cityLabel.text = experienceDao[indexPath.row].city+","+experienceDao[indexPath.row].countryName
            self.priceLabel.text = "From \(AppUtils.getCurrencySign()+""+AppUtils.calculateTravelPrice(basePrice: Int(experienceDao[indexPath.row].priceStart)!))"
            displayRating(rating: experienceDao[indexPath.row].ratings)
            displayWishlistImage(productId: experienceDao[indexPath.row].id)
            initializeWishlistModel(product_id: experienceDao[indexPath.row].id, vertical_id: AppConstants.WISHLIST_EXPERIENCE, identifier: AppConstants.TOP_EXPERIENCE, image: self.recommendedImageview.image, vc: homeViewController)
        }
    }
    
    
    /*
     * Method to set Festival cell Data
     */
    func setFestivalDataInCell(homeViewController: HomeViewController,indexPath: IndexPath){
        self.recommendedImageview.image = homeViewController.festivalImagesArray[indexPath.row]
        let festivalDao = FestivalDB().findAll().first
        if(festivalDao != nil){
            let festivalListDao = festivalDao!.festivalListDao
            self.generalDescriptionLabel.text = festivalListDao[indexPath.row].tagLine
            self.descriptionLabel.text = festivalListDao[indexPath.row].festivalName
            self.cityLabel.text = festivalListDao[indexPath.row].cityName+","+festivalListDao[indexPath.row].countryName
            self.priceLabel.text = "From \(AppUtils.getCurrencySign()+""+AppUtils.calculateTravelPrice(basePrice: Int(festivalListDao[indexPath.row].priceStart)!))"
            displayRating(rating: festivalListDao[indexPath.row].ratings)
            displayWishlistImage(productId: festivalListDao[indexPath.row].id)
            initializeWishlistModel(product_id: festivalListDao[indexPath.row].id, vertical_id: AppConstants.WISHLIST_FESTIVAL, identifier: AppConstants.FESTIVAL, image: self.recommendedImageview.image, vc: homeViewController)
        }
    }
    
    /*
     * Method to set Festival cell Data
     */
    func setActivityDataInCell(homeViewController: HomeViewController,indexPath: IndexPath){
        self.recommendedImageview.image = homeViewController.activityImagesArray[indexPath.row]
        let activityDao = ActivityDB().findAll().first
        if(activityDao != nil){
            let activityListDao = activityDao!.activityListDao
            self.generalDescriptionLabel.text = activityListDao[indexPath.row].tagLine
            self.descriptionLabel.text = activityListDao[indexPath.row].heading
            self.cityLabel.text = activityListDao[indexPath.row].cityName+","+activityListDao[indexPath.row].countryName
            self.priceLabel.text = "From \(AppUtils.getCurrencySign()+""+AppUtils.calculateTravelPrice(basePrice: Int(activityListDao[indexPath.row].priceStart)!))"
            displayRating(rating: activityListDao[indexPath.row].ratings)
            displayWishlistImage(productId: activityListDao[indexPath.row].id)
            initializeWishlistModel(product_id: activityListDao[indexPath.row].id, vertical_id: AppConstants.WISHLIST_TRENDING_ACTIVITY, identifier: AppConstants.TRENDING_ACTIVITY, image: self.recommendedImageview.image, vc: homeViewController)
        }
    }
    
    /*
     * Method to set Festival cell Data
     */
    func setDetailScreenDataInCell(detailViewController: DetailViewController,indexPath: IndexPath){
        self.recommendedImageview.image = detailViewController.recommendedImagesArray[indexPath.row]
        let detailDao = DetailDB().findAll().first
        if(detailDao != nil){
            let similarListDao = detailDao!.similarDetailDao
            self.generalDescriptionLabel.text = similarListDao[indexPath.row].tag_line
            self.descriptionLabel.text = similarListDao[indexPath.row].heading
            self.cityLabel.text = similarListDao[indexPath.row].city+","+similarListDao[indexPath.row].country_name
            self.priceLabel.text = "From \(AppUtils.getCurrencySign()+""+AppUtils.calculateTravelPrice(basePrice: Int(similarListDao[indexPath.row].price_start)!))"
            displayRating(rating: "")
            self.wishlistButton.isHidden = true
        }
    }
    
    
    /*
     * Method to set Festival cell Data
     */
    func setFestivalDetailScreenDataInCell(detailViewController: FestivalDetailViewController,indexPath: IndexPath){
        self.recommendedImageview.image = detailViewController.recommendedImagesArray[indexPath.row]
        let detailDao = FestivalDetailDB().findAll().first
        if(detailDao != nil){
            let similarListDao = detailDao!.similarDetailDao
            self.generalDescriptionLabel.text = similarListDao[indexPath.row].tag_line
            self.descriptionLabel.text = similarListDao[indexPath.row].heading
            self.cityLabel.text = similarListDao[indexPath.row].city+","+similarListDao[indexPath.row].country_name
            self.priceLabel.text = "From \(AppUtils.getCurrencySign()+""+AppUtils.calculateTravelPrice(basePrice: Int(similarListDao[indexPath.row].price_start)!))"
            displayRating(rating: "")
            self.wishlistButton.isHidden = true
        }
    }
    
    
    func initializeWishlistModel(product_id :String, vertical_id : String, identifier: String, image: UIImage?, vc: HomeViewController){
        // set a "Callback Closure" in the cell
        self.wishlistBtnTapAction = {
            () in
            //self.wishlistButton.setImage(UIImage(named: "heart.png"), for: UIControl.State.normal)
            var str64 : String? = ""
            if(image != nil){
                str64 = AppUtils.convertImageToBase64(image: image!)
            }
            var data = [
                "product_id" : product_id as Any,
                "vertical_id" : vertical_id as Any,
                "imagebase64" : str64 as Any,
                "device_id" :  "ashwini@gmail.com" as Any,
                ] as [String:Any]?
            let wishlistModel = WishlistModel.init(delegate: self)
            wishlistModel.requestToAddWishlist(wishlistData: data, identifier: identifier)
            vc.showToast(message: AppConstants.ADDING_WISHLIST_MSG)
            data = nil
        }
    }
    
    func onHttpResponse(isSuccess: Bool, responseJson: JSON, error: AnyObject!, anyIdentifier: String) {
        if(anyIdentifier == AppConstants.TOP_EXPERIENCE){
            self.wishlistButton.setImage(UIImage(named: "heart.png"), for: UIControl.State.normal)
        }
        else if(anyIdentifier == AppConstants.FESTIVAL){
            self.wishlistButton.setImage(UIImage(named: "heart.png"), for: UIControl.State.normal)
        }
        else if(anyIdentifier == AppConstants.TRENDING_ACTIVITY){
            self.wishlistButton.setImage(UIImage(named: "heart.png"), for: UIControl.State.normal)
        }
    }
    
    /*
     * Method to show wishlist image as per wishlist status.
     */
    func displayWishlistImage(productId: String?){
        let wishlistdao = WishlistDB().findByProperty(property: "product_id", value: productId!).first
        if(wishlistdao != nil){
            if( wishlistdao!.addedWishlist){
                self.wishlistButton.setImage(UIImage(named: "heart.png"), for: UIControl.State.normal)
            }
        }else{
            self.wishlistButton.setImage(UIImage(named: "heart_white.png"), for: UIControl.State.normal)
        }
    }
    
    
    /*
     * Display images of rating
     */
    func displayRating(rating:String?){
        if(rating == "" || rating == "0"){
            rating1.isHidden = true
            rating2.isHidden = true
            rating3.isHidden = true
            rating4.isHidden = true
            rating5.isHidden = true
        }
        else{
        let imagesArray = AppUtils.displayRating(rating: rating)
            rating1.image = imagesArray![0]
            rating2.image = imagesArray![1]
            rating3.image = imagesArray![2]
            rating4.image = imagesArray![3]
            rating5.image = imagesArray![4]
        }
        
    }
    
    
    
}

