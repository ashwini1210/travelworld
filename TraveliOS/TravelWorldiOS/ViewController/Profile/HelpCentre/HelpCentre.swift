//
//  HelpCentre.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 02/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON

class HelpCentre: UIView, HttpResponseProtocol, UITextViewDelegate {
    
    @IBOutlet weak var subjectTextview: SkyFloatingLabelTextField!
    @IBOutlet weak var queryTextview: UITextView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sendQueryButton: UIButton!
    
    var profileViewController : ProfileViewController?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        AppUtils.borderView(view: cancelButton, borderWidth: 1, borderColour: UIColor.black.cgColor)
        AppUtils.borderView(view: sendQueryButton, borderWidth: 1, borderColour: UIColor.black.cgColor)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "HelpCentre", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        profileViewController!.helpCentreAlphaView.backgroundColor = UIColor(white: 0, alpha: 1)
        profileViewController!.helpCentreSubView.isHidden = true
        profileViewController!.helpCentreAlphaView.isHidden = true
    }
    
    @IBAction func sendQueryButtonClicked(_ sender: Any) {
        if(AppUtils.isConnectedToInternet()){
            var data = [String:String]()
            data["user_id"] = AppPreference.getInstance().getString(AppConstants.LOGIN_ID, defaultvalue: "")
            data["subject"] = subjectTextview.text
            data["query"] = queryTextview.text
            let sendQueryModel = SendQueryModel.init(delegate: self)
            sendQueryModel.requestForQuery(data: data)
        }else{
            profileViewController!.displayAlert(title: "", message: AppConstants.INTERNET_MESSAGE, btn1: AppConstants.OK_BUTTON)
        }
        
        profileViewController!.helpCentreAlphaView.backgroundColor = UIColor(white: 0, alpha: 1)
        profileViewController!.helpCentreSubView.isHidden = true
        profileViewController!.helpCentreAlphaView.isHidden = true
        
    }
    
    func onHttpResponse(isSuccess: Bool, responseJson: JSON, error: AnyObject!, anyIdentifier: String) {
        if(isSuccess){
            if(anyIdentifier == AppConstants.SEND_QUERY){
                profileViewController!.showToast(message: AppConstants.QUERY_SUCCESS_MSG)
            }
        }
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        profileViewController!.activeField = textView
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        profileViewController?.activeField = nil
    }
    
}

