//
//  AboutUsViewController.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 02/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import WebKit

class AboutUsViewController: BaseVC , WKNavigationDelegate {
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        loader.startAnimating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadWebView()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    func loadWebView(){
        let url = URL(string: "https://leamigo.com/about")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        loader.stopAnimating()
        loader.hidesWhenStopped = true
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        loader.stopAnimating()
        loader.hidesWhenStopped = true
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load")
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
        loader.stopAnimating()
        loader.hidesWhenStopped = true
    }
    @IBAction func onBackbuttonClicked(_ sender: Any) {
        self.onBackNavigation()
    }
}
