//
//  LoginViewController.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 28/08/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON
import GoogleSignIn
import FacebookLogin
import FBSDKLoginKit

class LoginViewController: BaseVC, UITextFieldDelegate,GIDSignInDelegate, GIDSignInUIDelegate{
    
    @IBOutlet weak var alphaview: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var passwordTextview: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTextview: SkyFloatingLabelTextField!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    var activeField: UITextField?
    var previousActiveField: UITextField?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeGoogleSignIn()
        initializeViews()
        registerForKeyboardNotifications()
        hideKeyboardWhenTappedAround()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func initializeViews(){
        loader.stopAnimating()
        loader.isHidden = true
        alphaview.backgroundColor = UIColor.black.withAlphaComponent(0.15)
        alphaview.isOpaque = false
        AppUtils.cornerRadius(view: loginButton, roundCornerWidth: 5)
        self.scrollview.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height + 15)
        
    }
    
    @IBAction func loginButtonClicked(_ sender: Any) {
        if(emailTextview.text!.isEmpty || !(AppUtils.isValidEmail(testStr: emailTextview.text!))){
            emailTextview.errorMessage = AppConstants.EMAIL_ERROR_MESSAGE
        }
        else if(passwordTextview.text!.isEmpty){
            passwordTextview.errorMessage = AppConstants.PASSWORD_ERROR_MESSAGE
        }else{
            loader.isHidden = false
            loader.startAnimating()
            var data = [String:String]()
            data["email"] = emailTextview.text
            data["pass"] = passwordTextview.text
            let loginModel =  LoginModel(delegate: self)
            loginModel.requestForLogin(data: data, identifier: AppConstants.NORMAL_SIGNIN)
        }
    }
    
    
    
    @IBAction func registerButtonClicked(_ sender: Any) {
        self.launchViewController(viewControllerName: AppConstants.RegisterViewController)
    }
    
    override func onHttpResponse(isSuccess: Bool, responseJson: JSON, error: AnyObject!, anyIdentifier: String) {
        loader.stopAnimating()
        if(isSuccess){
            if(anyIdentifier == AppConstants.SIGNIN){
                if responseJson["status"].boolValue{
                    let previousScreen = AppPreference.getInstance().getString(AppConstants.AP_PREVIOUSLAUNCH_VC, defaultvalue: "")
                    if(previousScreen == AppConstants.BookLocalViewController || previousScreen == AppConstants.BookExperienceViewController){
                        self.onBackNavigation()
                    }else{
                        launchViewController(viewControllerName: AppConstants.ParentTabBarViewController)
                    }
                }else{
                    self.displayAlert(title: "", message: AppConstants.LOGIN_ERROR_MESSAGE, btn1: AppConstants.OK_BUTTON )
                }
            }
            else if (anyIdentifier == AppConstants.FORGOT_PASSWORD){
                self.displayAlert(title: "", message: AppConstants.FORGOT_PASSWORD_SUCCESS_MESSAGE, btn1: AppConstants.OK_BUTTON )
            }
        }else{
            self.showToast(message: AppConstants.TRY_AGAIN_MSG)
        }
    }
    
    @IBAction func forgotPasswordClicked(_ sender: Any) {
        var email: UITextField?
        let alertController = UIAlertController(title: "",message: AppConstants.FORGOT_PASSWORD_MESSAGE,preferredStyle: UIAlertController.Style.alert)
        let changeAction = UIAlertAction(title: AppConstants.SUBMIT_TITLE, style: UIAlertAction.Style.default){
            (action) -> Void in
            if(email?.text == ""){
                self.showToast(message: AppConstants.EMAIL_ERROR_MESSAGE)
            }
            else{
                //Hit for forgot password
                var data = [String:String]()
                data["email"] = email!.text
                let forgotPasswordModel =  ForgotPasswordModel(delegate: self)
                forgotPasswordModel.requestForForgotPassword(data: data)
            }
        }
        let cancelAction = UIAlertAction(title: AppConstants.CANCEL_BUTTON, style: UIAlertAction.Style.default)
        
        alertController.addTextField {(txtPassword) -> Void in
            email = txtPassword
            email!.placeholder = "Enter Email"
        }
        alertController.addAction(changeAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    /*************** GMAIL LOGIN CODE *******************/
    
    @IBAction func GogleButtonClicked(_ sender: Any) {
        if GIDSignIn.sharedInstance().hasAuthInKeychain() {
            GIDSignIn.sharedInstance().signOut()
        }
        GIDSignIn.sharedInstance().signIn()
    }
    
    func initializeGoogleSignIn(){
        // Initialize sign-in
        GIDSignIn.sharedInstance().clientID = "43903431623-828dtgo8fhquck8m9b5pmrkrc5s377nj.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        loader.isHidden = false
        loader.startAnimating()
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,withError error: Error!) {
        loader.stopAnimating()
        if let error = error {
            print("\(error.localizedDescription)")
            self.showToast(message: AppConstants.TRY_AGAIN_MSG)
        } else {
            print("Google user: \(user!)")
            var data = [String:String]()
            data["email"] = user.profile.email
            data["id"] = user.userID
            data["name"] = user.profile.name
            data["gender"] = ""
            let pic =  user.profile.imageURL(withDimension: 200)!.absoluteString
            data["pic"] = pic
            
            let downloadImages =  DownloadImages.init(delegate: self)
            downloadImages.downloadImageAsync(URLString: pic, placeHolderImage: UIImage.init(named: "child_icon.png"), identifier: AppConstants.PROFILE_IMAGE_URL, id: "1")
            
            let loginModel =  LoginModel(delegate: self)
            loginModel.requestForLogin(data: data, identifier: AppConstants.GOOGLE_SIGNIN)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        loader.stopAnimating()
    }
    
    /*************** FACEBOOK LOGIN CODE *******************/
    
    
    @IBAction func facebookButtonClicked(_ sender: Any) {
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        if FBSDKAccessToken.current() != nil {
            loginManager.logOut()
            //getFBUserData()
        }
        
        loginManager.logIn(withReadPermissions: ["email","public_profile"], from: self, handler: { (loginResults: FBSDKLoginManagerLoginResult?, error: Error?) -> Void in
            if !(loginResults?.isCancelled)! {
                self.getFBUserData()
            }
            else {    // Sign in request cancelled
                let err = NSError()
                print("FB error: \(err)")
                self.showToast(message: AppConstants.TRY_AGAIN_MSG)
            }
        })
        
    }
    
    //function is fetching the user data
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id,name,email,gender,picture.type(large),age_range,locale,link,cover,timezone"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    print(result as! [String : AnyObject])
                }
                
                // Details received successfully
                let dictionary = result as! [String: AnyObject]
                var data = [String:String]()
                data["email"] = dictionary["email"] as? String
                data["facebook_id"] = dictionary["id"] as? String
                data["first_name"] = dictionary["name"] as? String
                data["last_name"] = dictionary[""] as? String
                data["gender"] = dictionary[""] as? String
                data["home_city"] = dictionary[""] as? String
                let pic = dictionary["picture"] as?  [String: AnyObject]
                if(pic != nil){
                    let data1 = pic!["data"] as?  [String: AnyObject]
                    if(data1 != nil){
                        let pic = data1!["url"]as? String
                        data["picture"] = pic
                        
                        let downloadImages =  DownloadImages.init(delegate: self)
                        downloadImages.downloadImageAsync(URLString: pic!, placeHolderImage: UIImage.init(named: "child_icon.png"), identifier: AppConstants.PROFILE_IMAGE_URL, id: "1")
                    }
                }
                
                print(data)
                
                let loginModel =  LoginModel(delegate: self)
                loginModel.requestForLogin(data: data, identifier: AppConstants.FACEBOOK_SIGNIN)
            })
        }
    }
    
    override func onImageResponse(isSuccess: Bool?, image: UIImage?, error: AnyObject?, anyIdentifier: String?, id: String?) {
        if(isSuccess!){
            if(image != nil){
                let base64Str = AppUtils.convertImageToBase64(image: image!)
                AppPreference.getInstance().setString(AppConstants.PROFILE_IMAGE_URL, value: base64Str)
            }
        }
    }
    
    /*************** KEYBOARD CODE *******************/
    /*
     * Register keyboard open close notification
     */
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollview.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        
        self.scrollview.contentInset = contentInsets
        self.scrollview.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollview.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.scrollview.contentInset = contentInsets
        self.scrollview.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
    }
    
    /*
     * Hide keyboard on outer touch
     */
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        if let floatingLabelTextField = previousActiveField as? SkyFloatingLabelTextField {
            if(floatingLabelTextField.errorMessage != ""){
                floatingLabelTextField.errorMessage = ""
            }
        }
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        previousActiveField = activeField
        activeField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTextview:
            passwordTextview.becomeFirstResponder()
        default:
            passwordTextview.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let floatingLabelTextField = textField as? SkyFloatingLabelTextField {
            //Remove error message when going to correct it
            if(floatingLabelTextField.errorMessage != nil || floatingLabelTextField.errorMessage != "" || !(floatingLabelTextField.errorMessage!.isEmpty)){
                if let error = floatingLabelTextField.errorMessage{
                    print("Error in signup: \(error)")
                    floatingLabelTextField.errorMessage = ""
                }
            }
        }
        return true
    }
    
    deinit {
        deregisterFromKeyboardNotifications()
    }
}
