//
//  RegisterViewController.swift
//  LeamigoiOS
//
//  Created by Apple on 05/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON
import DLRadioButton

class RegisterViewController: BaseVC, UITextFieldDelegate,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var femaleBtn: DLRadioButton!
    @IBOutlet weak var maleBtn: DLRadioButton!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var emailTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var confirmPasswordTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var cityTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneNumTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var lastNameTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var firstNameTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var cityTableview: UITableView!
    @IBOutlet weak var alphaTableView: UIView!
    var gender : String? = "Male"
    var activeField: UITextField?
    var previousActiveField: UITextField?
    var placesModel : SearchCityModel? = nil
    var searchedCity = [String]()
    var isResponseFromModel = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerForKeyboardNotifications()
        hideKeyboardWhenTappedAround()
        initviews()
        registerTableView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.cityTableview.frame = CGRect(x: self.cityTableview.frame.origin.x, y: self.cityTableview.frame.origin.y, width: self.cityTableview.frame.size.width - 5, height: self.cityTableview.frame.size.height)
    }
    
    func initviews(){
        self.scrollview.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height + 10)
        alphaTableView.backgroundColor = UIColor(white: 0, alpha: 0.7)
        loader.stopAnimating()
        loader.isHidden = true
        maleBtn.isSelected = true //default select male btn
        alphaTableView.isHidden = true
        cityTableview.isHidden = true
    }
    
    /*
     * Method to Register Table view
     */
    func registerTableView(){
        cityTableview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        cityTableview.isHidden = true //Initially hide tableview
    }
    
    func initalizeCityModel(searchChar: String?){
        if(!isResponseFromModel){
            placesModel = SearchCityModel.init(delegate: self)
            ApplicationManager.getInstance.searchCityModel = placesModel
            if(searchChar != nil){
                var data = [
                    "q" : searchChar! as Any,
                    ] as [String:Any]?
                placesModel!.requestForCity(searchChar: data)
                data = nil
                isResponseFromModel = true
            }
        }
    }
    
    override func onHttpResponse(isSuccess: Bool, responseJson: JSON, error: AnyObject!, anyIdentifier: String) {
        loader.stopAnimating()
        if(anyIdentifier == AppConstants.PLACES){
            searchedCity = placesModel!.cityArray
            alphaTableView.isHidden = false
            cityTableview.isHidden = false
            isResponseFromModel = false
            cityTableview.reloadData()
            
        }
        else if(isSuccess){
            if(anyIdentifier == AppConstants.SIGNUP){
                if responseJson["status"].boolValue{
                    downloadProfileImage()
                    self.launchViewController(viewControllerName: AppConstants.LoginViewController)
                    self.displayAlert(title: "", message: AppConstants.SIGNUP_SUCCESSFUL_MESSAGE, btn1: AppConstants.OK_BUTTON )
                }else{
                    self.displayAlert(title: "", message: AppConstants.SIGNUP_ERROR_MESSAGE, btn1: AppConstants.OK_BUTTON )
                }
            }
        }
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        onBackNavigation()
    }
    
    @IBAction func radioButtonClicked(_ sender: DLRadioButton) {
        if(sender.tag == 1){
            gender = "Male"
            femaleBtn.isSelected = false
        }
        else if(sender.tag == 2){
            gender = "Female"
            maleBtn.isSelected = false
        }
    }
    
    
    @IBAction func submitButtonClicked(_ sender: Any) {
        if(emailTextfield.text!.isEmpty || !(AppUtils.isValidEmail(testStr: emailTextfield.text!))){
            emailTextfield.errorMessage = AppConstants.EMAIL_ERROR_MESSAGE
        }
        else if(passwordTextfield.text!.isEmpty){
            passwordTextfield.errorMessage = AppConstants.PASSWORD_ERROR_MESSAGE
        }
        else if(confirmPasswordTextfield.text!.isEmpty || (passwordTextfield.text != confirmPasswordTextfield.text)){
            confirmPasswordTextfield.errorMessage = AppConstants.CONFIRM_PASSWORD_ERROR_MESSAGE
        }
        else if(firstNameTextfield.text!.isEmpty){
            firstNameTextfield.errorMessage = AppConstants.FIRST_NAME_ERROR_MESSAGE
        }
        else if(lastNameTextfield.text!.isEmpty){
            lastNameTextfield.errorMessage = AppConstants.LAST_NAME_ERROR_MESSAGE
        }
        else if(phoneNumTextfield.text!.isEmpty){
            phoneNumTextfield.errorMessage = AppConstants.PHONE_ERROR_MESSAGE
        }
        else if(cityTextfield.text!.isEmpty){
            cityTextfield.errorMessage = AppConstants.HOME_CITY_ERROR_MESSAGE
        }
        else{
            var data = [String:Any]()
            data["first_name"] = firstNameTextfield.text
            data["last_name"] = lastNameTextfield.text
            data["gender"] = gender
            data["email"] = emailTextfield.text
            data["pass"] = passwordTextfield.text
            data["phone"] = phoneNumTextfield.text
            data["home_city"] = cityTextfield.text
            loader.isHidden = false
            loader.startAnimating()
            let registerModel = RegisterModel(delegate: self)
            registerModel.requestForReagister(data: data)
        }
    }
    
    func downloadProfileImage(){
        let profileImageUrl = AppPreference.getInstance().getString(AppConstants.IMAGE_URL, defaultvalue: "")
        if(profileImageUrl != ""){
            let downloadImages = DownloadImages.init(delegate: self)
            downloadImages.downloadImageAsync(URLString: profileImageUrl, placeHolderImage: UIImage(named: "child_icon.png"), identifier: AppConstants.PROFILE_IMAGE, id: "1")
        }
    }
    
    override func onImageResponse(isSuccess: Bool?, image: UIImage?, error: AnyObject?, anyIdentifier: String?, id: String?) {
        if(isSuccess!){
            if(anyIdentifier == AppConstants.PROFILE_IMAGE){
                let base64Str = AppUtils.convertImageToBase64(image: image!)
                AppPreference.getInstance().setString(AppConstants.PROFILE_IMAGE_URL, value: base64Str)
            }
        }
    }
    
    /*
     * Table view callback
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchedCity.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if searchedCity.count != 0 {
            cell.textLabel?.text = searchedCity[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        cityTextfield.text = searchedCity[indexPath.row]
        tableView.isHidden = true
        alphaTableView.isHidden = true
    }
    
    /*************** KEYBOARD CODE *******************/
    /*
     * Register keyboard open close notification
     */
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollview.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        
        self.scrollview.contentInset = contentInsets
        self.scrollview.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollview.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.scrollview.contentInset = contentInsets
        self.scrollview.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
    }
    
    /*
     * Hide keyboard on outer touch
     */
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        if let floatingLabelTextField = previousActiveField as? SkyFloatingLabelTextField {
            if(floatingLabelTextField.errorMessage != ""){
                floatingLabelTextField.errorMessage = ""
            }
        }
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        previousActiveField = activeField
        activeField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTextfield:
            passwordTextfield.becomeFirstResponder()
        case passwordTextfield:
            confirmPasswordTextfield.becomeFirstResponder()
        case confirmPasswordTextfield:
            firstNameTextfield.becomeFirstResponder()
        case firstNameTextfield:
            lastNameTextfield.becomeFirstResponder()
        case lastNameTextfield:
            phoneNumTextfield.becomeFirstResponder()
        case phoneNumTextfield:
            cityTextfield.becomeFirstResponder()
        default:
            cityTextfield.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let floatingLabelTextField = textField as? SkyFloatingLabelTextField {
            
            if(floatingLabelTextField == cityTextfield){
                if floatingLabelTextField.text!.count > 1{
                    initalizeCityModel(searchChar: floatingLabelTextField.text!)
                }
            }
                //Remove error message when going to correct it
            else if(floatingLabelTextField.errorMessage != nil || floatingLabelTextField.errorMessage != "" || !(floatingLabelTextField.errorMessage!.isEmpty)){
                if let error = floatingLabelTextField.errorMessage{
                    print("Error in signup: \(error)")
                    floatingLabelTextField.errorMessage = ""
                }
            }
        }
        return true
    }
    
    
    deinit {
        deregisterFromKeyboardNotifications()
    }
}
