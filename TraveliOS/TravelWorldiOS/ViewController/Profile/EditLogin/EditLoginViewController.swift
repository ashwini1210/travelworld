//
//  EditLoginViewController.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 01/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import DLRadioButton
import Toast_Swift
import SwiftyJSON

class EditLoginViewController: BaseVC, UITextFieldDelegate, UIImagePickerControllerDelegate, UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var firstName: SkyFloatingLabelTextField!
    @IBOutlet weak var lastName: SkyFloatingLabelTextField!
    @IBOutlet weak var email: SkyFloatingLabelTextField!
    @IBOutlet weak var mobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var city: SkyFloatingLabelTextField!
    @IBOutlet weak var age: SkyFloatingLabelTextField!
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var testFieldsView: UIView!
    @IBOutlet weak var cityTableview: UITableView!
    @IBOutlet weak var alphaTableView: UIView!
    @IBOutlet weak var femaleBtn: DLRadioButton!
    @IBOutlet weak var maleBtn: DLRadioButton!
    var imagePicker = UIImagePickerController()
    var gender : String? = "Male"
    var previousActiveField: UITextField?
    var activeField: UITextField?
    var placesModel : SearchCityModel? = nil
    var searchedCity = [String]()
    var isResponseFromModel = false
    var frame : CGRect?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeViews()
        makeCircularProfileImageview()
        registerForKeyboardNotifications()
        hideKeyboardWhenTappedAround()
        registerTableView()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.cityTableview.frame = CGRect(x: self.cityTableview.frame.origin.x, y: self.cityTableview.frame.origin.y, width: self.cityTableview.frame.size.width - 5, height: self.cityTableview.frame.size.height)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setDataInTextField()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initializeViews(){
        AppUtils.borderView(view: changePasswordButton, borderWidth: 1, borderColour: UIColor.black.cgColor)
        scrollView.contentSize = CGSize(width: self.testFieldsView.frame.size.width, height: self.view.frame.size.height + 10)
        self.alphaTableView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        frame = self.cityTableview.frame
        alphaTableView.isHidden = true
        cityTableview.isHidden = true
    }
    
    /*
     * Method to Register Table view
     */
    func registerTableView(){
        cityTableview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        cityTableview.isHidden = true //Initially hide tableview
    }
    
    func makeCircularProfileImageview(){
        let image = UIImage(named: "child_icon.png")
        profileImage.layer.borderWidth = 1.0
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.white.cgColor
        profileImage.layer.cornerRadius = profileImage.frame.size.width / 2
        profileImage.clipsToBounds = true
        profileImage.image = image
        
    }
    
    /*
     * Set data in all fields after login
     */
    func setDataInTextField(){
        let islogin = AppPreference.getInstance().getBoolean(AppConstants.IS_LOGIN, defaultvalue: false)
        if(islogin){
            firstName.text = AppPreference.getInstance().getString(AppConstants.FIRST_NAME, defaultvalue: "")
            lastName.text = AppPreference.getInstance().getString(AppConstants.LAST_NAME, defaultvalue: "")
            email.text = AppPreference.getInstance().getString(AppConstants.EMAIL, defaultvalue: "")
            mobileNumber.text = AppPreference.getInstance().getString(AppConstants.PHONE, defaultvalue: "")
            city.text = AppPreference.getInstance().getString(AppConstants.HOME_CITY, defaultvalue: "")
            
            let gender = AppPreference.getInstance().getString(AppConstants.GENDER, defaultvalue: "")
            if(gender == "Male"){
                maleBtn.isSelected = true
            }else{
                femaleBtn.isSelected = true
            }
            
            let ageStr = AppPreference.getInstance().getString(AppConstants.AGE, defaultvalue: "")
            if(ageStr != ""){
                age.text = ageStr
            }
            
            let imgStr = AppPreference.getInstance().getString(AppConstants.PROFILE_IMAGE_URL, defaultvalue: "")
            if(imgStr != ""){
                let img = AppUtils.convertBase64ToImage(imageString: imgStr)
                profileImage.image = img
            }
        }
    }
    
    @IBAction func radioButtonClicked(_ sender: DLRadioButton) {
        if(sender.tag == 1){
            gender = "Male"
            femaleBtn.isSelected = false
        }
        else if(sender.tag == 2){
            gender = "Female"
            maleBtn.isSelected = false
        }
    }
    
    @IBAction func changePasswordButtonClicked(_ sender: Any) {
        var oldPassword: UITextField?
        var newPassword: UITextField?
        var confirmPassword: UITextField?
        
        let alertController = UIAlertController(title: AppConstants.CHANGE_PASSWORD_TITLE,message: "",preferredStyle: UIAlertController.Style.alert)
        let changeAction = UIAlertAction(title: AppConstants.CHANGE_TITLE, style: UIAlertAction.Style.default){
            (action) -> Void in
            if(oldPassword?.text == ""){
                self.showToast(message: AppConstants.PASSWORD_NOT_UPDATE_MSG)
            }else if (newPassword?.text == ""){
                self.showToast(message: AppConstants.PASSWORD_NOT_UPDATE_MSG)
            }else if (confirmPassword?.text == ""){
                self.showToast(message: AppConstants.PASSWORD_NOT_UPDATE_MSG)
            }
            else if (confirmPassword?.text != newPassword?.text){
                self.showToast(message: AppConstants.CONFIRM_PASSWORD_ERROR_MESSAGE)
            }
            else{
                //Hit to change password
                var data = [String:String]()
                data["current_pass"] = oldPassword!.text
                data["new_pass"] = newPassword!.text
                let updatePasswordModel =  UpdatePasswordModel(delegate: self)
                updatePasswordModel.requestForPassword(data: data)
            }
        }
        let cancelAction = UIAlertAction(title: AppConstants.CANCEL_BUTTON, style: UIAlertAction.Style.default)
        
        alertController.addTextField {(txtPassword) -> Void in
            oldPassword = txtPassword
            oldPassword!.isSecureTextEntry = true
            oldPassword!.placeholder = AppConstants.CURRENT_PASSWORD_MSG
        }
        alertController.addTextField {(txtPassword) -> Void in
            newPassword = txtPassword
            newPassword!.isSecureTextEntry = true
            newPassword!.placeholder = AppConstants.NEW_PASSWORD_MSG
        }
        alertController.addTextField {(txtPassword) -> Void in
            confirmPassword = txtPassword
            confirmPassword!.isSecureTextEntry = true
            confirmPassword!.placeholder = AppConstants.CONFIRM_PASSWORD_MSG
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(changeAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func saveButtonClicked(_ sender: Any) {
        if(age.text!.isEmpty){
            age.errorMessage = AppConstants.AGE_ERROR_MESSAGE
        }else{
            if(profileImage.image != nil){
                let imgStr = AppUtils.convertImageToBase64(image: profileImage.image!)
                AppPreference.getInstance().setString(AppConstants.PROFILE_IMAGE_URL, value: imgStr)
            }
            
            //hit for save
            var data = [String:String]()
            data["id"] = AppPreference.getInstance().getString(AppConstants.LOGIN_ID, defaultvalue: "")
            data["first_name"] = firstName.text
            data["last_name"] = lastName.text
            data["gender"] = gender
            data["email"] = email.text
            data["phone"] = mobileNumber.text
            data["home_city"] = city.text
            data["age"] = age.text
            
            let editProfileModel = EditProfileModel.init(delegate: self)
            editProfileModel.requestForEditProfile(data: data)
        }
    }
    
    @IBAction func changeImageBtnCLicked(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        //profileImage.contentMode = .scaleAspectFit
        profileImage.image = chosenImage
        dismiss(animated:true, completion: nil)
    }
    
    
    @IBAction func onBackButtonClicked(_ sender: Any) {
        self.onBackNavigation()
    }
    
    func initalizeCityModel(searchChar: String?){
        if(!isResponseFromModel){
            placesModel = SearchCityModel.init(delegate: self)
            ApplicationManager.getInstance.searchCityModel = placesModel
            if(searchChar != nil){
                var data = [
                    "q" : searchChar! as Any,
                    ] as [String:Any]?
                placesModel!.requestForCity(searchChar: data)
                data = nil
                isResponseFromModel = true
            }
        }
    }
    
    override func onHttpResponse(isSuccess: Bool, responseJson: JSON, error: AnyObject!, anyIdentifier: String) {
        if(anyIdentifier == AppConstants.PLACES){
            searchedCity = placesModel!.cityArray
            alphaTableView.isHidden = false
            cityTableview.isHidden = false
            isResponseFromModel = false
            cityTableview.reloadData()
            adjustTableViewHeightForSearch()
            
        }
        else if(isSuccess){
            if(anyIdentifier == AppConstants.UPDATE_PASSWORD){
                self.showToast(message: AppConstants.PASSWORD_UPDATED)
            }
            else if(anyIdentifier == AppConstants.EDIT_PROFILE){
                setDataInTextField()
                self.showToast(message: AppConstants.PROFILE_UPDATED)
                launchViewController(viewControllerName: AppConstants.ParentTabBarViewController)
            }
        }else{
            self.showToast(message: AppConstants.TRY_AGAIN_MSG)
        }
    }
    
    /*
     * Table view callback
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchedCity.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if searchedCity.count != 0 {
            cell.textLabel?.text = searchedCity[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        city.text = searchedCity[indexPath.row]
        tableView.isHidden = true
        alphaTableView.isHidden = true
    }
    
    /*
     * Method to adjust tableview height as per search field data
     */
    func adjustTableViewHeightForSearch(){
        if(searchedCity.count<5){
            DispatchQueue.main.async {
                //This code will run in the main thread:
                var frame1 = self.frame
                frame1!.size.height = self.cityTableview.contentSize.height
                self.cityTableview.frame = frame1!
            }
        }else{
            self.cityTableview.frame = frame!
        }
    }
    
    /*************** KEYBOARD CODE *******************/
    /*
     * Register keyboard open close notification
     */
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
    }
    
    /*
     * Hide keyboard on outer touch
     */
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        if let floatingLabelTextField = previousActiveField as? SkyFloatingLabelTextField {
            if(floatingLabelTextField.errorMessage != ""){
                floatingLabelTextField.errorMessage = ""
            }
        }
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let floatingLabelTextField = textField as? SkyFloatingLabelTextField {
            
            if(floatingLabelTextField == city){
                if floatingLabelTextField.text!.count > 1{
                    initalizeCityModel(searchChar: floatingLabelTextField.text!)
                }
            }
                //Remove error message when going to correct it
            else if(floatingLabelTextField.errorMessage != nil || floatingLabelTextField.errorMessage != "" || !(floatingLabelTextField.errorMessage!.isEmpty)){
                if let error = floatingLabelTextField.errorMessage{
                    print("Error in signup: \(error)")
                    floatingLabelTextField.errorMessage = ""
                }
            }
        }
        return true
    }
    
    deinit {
        deregisterFromKeyboardNotifications()
    }
    
    
}
