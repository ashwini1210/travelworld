//
//  ProfileViewController.swift
//  DetailScreen
//
//  Created by Apple on 23/08/18.
//  Copyright © 2018 Appleechnopurple.com. All rights reserved.
//

import UIKit

class ProfileViewController: BaseVC {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var helpCenterView: UIView!
    @IBOutlet weak var aboutUsView: UIView!
    @IBOutlet weak var faqView: UIView!
    @IBOutlet weak var inviteFriendView: UIView!
    @IBOutlet weak var offersView: UIView!
    @IBOutlet weak var logoutView: UIView!
    @IBOutlet weak var helpCentreSubView: UIView!
    @IBOutlet weak var helpCentreAlphaView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var editImageView: UIImageView!
    @IBOutlet weak var helpCentreScrollview: UIScrollView!
    
    var helpCentre : HelpCentre?
    var activeField: UITextView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeViews()
        borderViews()
        hideKeyboardWhenTappedAround()
        registerForKeyboardNotifications()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        displayLoginData()
    }
    
    func initializeViews(){
        //Initialize HelpCentre view
        helpCentre = HelpCentre.instanceFromNib() as? HelpCentre
        helpCentre?.profileViewController = self
        self.helpCentreSubView.addSubview(helpCentre!)
        self.helpCentreSubView.isHidden = true //Initially hide HelpCentre view
        self.helpCentreAlphaView.isHidden = true
        editImageView.isHidden = true
    }
    
    func displayLoginData(){
        let islogin = AppPreference.getInstance().getBoolean(AppConstants.IS_LOGIN, defaultvalue: false)
        if(islogin){
            editImageView.isHidden = false
           let profileImageUrl = AppPreference.getInstance().getString(AppConstants.PROFILE_IMAGE_URL, defaultvalue: "")
            if(profileImageUrl != ""){
               profileImage.image = AppUtils.convertBase64ToImage(imageString: profileImageUrl)
            }
            let firstName = AppPreference.getInstance().getString(AppConstants.FIRST_NAME, defaultvalue: "")
            let lastName = AppPreference.getInstance().getString(AppConstants.LAST_NAME, defaultvalue: "")
            if(firstName != ""){
                nameLabel.text = firstName
                if(lastName != ""){
                    nameLabel.text = nameLabel.text! + " " + lastName
                }
            }
        }
    }
    /*
     * Apply border to all view
     */
    func borderViews(){
        AppUtils.shadowView(view: loginView, shadowColour: UIColor.gray.cgColor, shadowRadius: 1)
        AppUtils.shadowView(view: helpCenterView, shadowColour: UIColor.gray.cgColor, shadowRadius: 1)
        AppUtils.shadowView(view: aboutUsView, shadowColour: UIColor.gray.cgColor, shadowRadius: 1)
        AppUtils.shadowView(view: faqView, shadowColour: UIColor.gray.cgColor, shadowRadius: 1)
        AppUtils.shadowView(view: inviteFriendView, shadowColour: UIColor.gray.cgColor, shadowRadius: 1)
        AppUtils.shadowView(view: offersView, shadowColour: UIColor.gray.cgColor, shadowRadius: 1)
        AppUtils.shadowView(view: logoutView, shadowColour: UIColor.gray.cgColor, shadowRadius: 1)
    }
    
    
    @IBAction func loginButtonClicked(_ sender: Any) {
        let isLogin = AppPreference.getInstance().getBoolean(AppConstants.IS_LOGIN, defaultvalue: false)
        if(isLogin){
            self.launchViewController(viewControllerName: AppConstants.EditLoginViewController)
        }else{
            self.launchViewController(viewControllerName: AppConstants.LoginViewController)
        }
    }
    
    
    @IBAction func helpCentreButtonClicked(_ sender: Any) {
        self.helpCentreAlphaView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        self.helpCentreSubView.isHidden = false
        self.helpCentreAlphaView.isHidden = false
    }
    
    @IBAction func aboutUsButtonClicked(_ sender: Any) {
        self.launchViewController(viewControllerName: AppConstants.AboutUsViewController)
    }
    
    @IBAction func faqButtonClicked(_ sender: Any) {
       self.launchViewController(viewControllerName: AppConstants.FAQViewController)
    }
    
    @IBAction func inviteButtonClicked(_ sender: Any) {
        inviteFriend()
    }
    
    @IBAction func offersButtonClicked(_ sender: Any) {
        self.launchViewController(viewControllerName: AppConstants.OffersViewController)
    }
    
    @IBAction func logoutButtonClicked(_ sender: Any) {
        //clear preference
        AppPreference.getInstance().setBoolean(AppConstants.IS_LOGIN, value: false)
        self.launchViewController(viewControllerName: AppConstants.ParentTabBarViewController)
    }
    
    /*
     * Share our app to friend
     */
    func inviteFriend(){
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let textToShare = AppConstants.INVITE_FRIEND
        
        if let myWebsite = URL(string: "http://itunes.apple.com/app/idXXXXXXXXX") {//Enter link to your app here
            let objectsToShare = [textToShare, myWebsite, image ?? #imageLiteral(resourceName: "app-logo")] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            //Excluded Activities
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
            //
            
            activityVC.popoverPresentationController?.sourceView = self.view
            self.present(activityVC, animated: true, completion: nil)
        }
        
    }
    
    /*
     * Hide keyboard on outer touch
     */
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func onQueryTextFieldClick(){
        
    }
    
    /*************** KEYBOARD CODE *******************/
    /*
     * Register keyboard open close notification
     */
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.helpCentreScrollview.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        
        self.helpCentreScrollview.contentInset = contentInsets
        self.helpCentreScrollview.scrollIndicatorInsets = contentInsets
        
        self.activeField = helpCentre?.queryTextview
        
        var aRect : CGRect = self.helpCentreAlphaView.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)){
                self.helpCentreScrollview.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.helpCentreScrollview.contentInset = contentInsets
        self.helpCentreScrollview.scrollIndicatorInsets = contentInsets
        self.helpCentreAlphaView.endEditing(true)
    }
}
