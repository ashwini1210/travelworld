//
//  WishListViewController.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 10/08/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit

class WishListViewController: BaseVC, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var noWishlistView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    let reuseIdentifier = "wishlistCell"
    var wishlistDB : WishlistDB? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initailizeViews()
        registerCollectionCell()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     * Initialize all view on screen load
     */
    func initailizeViews(){
        noWishlistView.isHidden = true
        wishlistDB = WishlistDB()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //If no data available show view of "no wishlist"
        let count = wishlistDB!.findAll().count
        if(count == 0){
            self.mainView.isHidden = true
            self.noWishlistView.isHidden = false
        }else{
            self.mainView.isHidden = false
            self.noWishlistView.isHidden = true
        }
        collectionView.reloadData()
    }
    
    func registerCollectionCell(){
        collectionView.register(UINib.init(nibName: "WishlistCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(wishlistDB != nil){
            return wishlistDB!.findAll().count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! WishlistCollectionViewCell
        cell.setDataInCell(wishListViewController: self, indexPath: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        var data = [String:Any]()
        let wishlistDB = WishlistDB()
        let wishlistDaoList = wishlistDB.findAll()
        let categoryId = wishlistDaoList[indexPath.row].category_id
        if(categoryId == AppConstants.WISHLIST_EXPERIENCE){
            data["identifier"] = AppConstants.TOP_EXPERIENCE as String
            let topExperienceDao =  TopExperienceDB().findAll().first
            if(topExperienceDao != nil){
                let esperienceDao = topExperienceDao?.experienceListDao[indexPath.row]
                data["id"] = esperienceDao!.id
            }
            self.launchViewController(viewControllerName: AppConstants.DetailViewController,dataToPass: data)            
        }
        else if(categoryId == AppConstants.WISHLIST_FESTIVAL){
            data["identifier"] = AppConstants.FESTIVAL as String
            let festivalDao =  FestivalDB().findAll().first
            if(festivalDao != nil){
                let festivalListDao = festivalDao!.festivalListDao[indexPath.row]
                data["id"] = festivalListDao.id
            }
            self.launchViewController(viewControllerName: AppConstants.FestivalDetailViewController,dataToPass: data)
        }
        else if(categoryId == AppConstants.WISHLIST_TRENDING_ACTIVITY){
            data["identifier"] = AppConstants.TRENDING_ACTIVITY as String
            let activityDao =  ActivityDB().findAll().first
            if(activityDao != nil){
                let activityListDao = activityDao!.activityListDao[indexPath.row]
                data["id"] = activityListDao.id
            }
            self.launchViewController(viewControllerName: AppConstants.DetailViewController,dataToPass: data)
        }
        else if(categoryId == AppConstants.WISHLIST_DESTINATION){
            let searchExperienceModel = ApplicationManager.getInstance.searchExperienceModel
            if(searchExperienceModel != nil){
                if searchExperienceModel!.searchExpArray.count != 0{
                    let dao = searchExperienceModel!.searchExpArray[indexPath.row]
                    data["identifier"] = AppConstants.PLACES as String
                    data["id"] = dao.id
                    self.launchViewController(viewControllerName: AppConstants.DetailViewController,dataToPass: data)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = self.collectionView.bounds.size.width
        let itemHeight = self.collectionView.bounds.size.height
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
}
