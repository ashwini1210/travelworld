//
//  WishlistCollectionViewCell.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 20/08/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit

class WishlistCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var wishlistButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var generalDescriptionLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var rating1: UIImageView!
    @IBOutlet weak var rating2: UIImageView!
    @IBOutlet weak var rating3: UIImageView!
    @IBOutlet weak var rating4: UIImageView!
    @IBOutlet weak var rating5: UIImageView!
    @IBOutlet weak var recommendedImageview: UIImageView!
    
    // this will be our "call back" action
    var wishlistBtnTapAction : (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    @IBAction func wishlistButtonClicked(_ sender: Any) {
        // use our "call back" action to tell the controller the button was tapped
        wishlistBtnTapAction?()
    }
    
    
    /*
     * Method to set Experience cell Data
     */
    func setDataInCell(wishListViewController: WishListViewController,indexPath: IndexPath){
        var productid : String?
        let wishlistDB = WishlistDB()
        let wishlistDaoList = wishlistDB.findAll()
        self.wishlistButton.setImage(UIImage(named: "heart.png"), for: UIControl.State.normal)
        self.recommendedImageview.image = AppUtils.convertBase64ToImage(imageString: wishlistDaoList[indexPath.row].imagebase64)
        
        
        let categoryId = wishlistDaoList[indexPath.row].category_id
        if(categoryId == AppConstants.WISHLIST_EXPERIENCE){
            productid = wishlistDaoList[indexPath.row].product_id
            let topExperienceDao = TopExperienceDB().findAll().first
            if(topExperienceDao != nil){
                let experienceDao = topExperienceDao!.experienceListDao
                let expDao = experienceDao.filter({$0.id == productid}).first
                self.generalDescriptionLabel.text = expDao!.heading
                self.descriptionLabel.text = expDao!.tagLine
                self.cityLabel.text = expDao!.city+","+expDao!.countryName
                self.priceLabel.text = "From: \(expDao!.currencySign)\(Double(expDao!.priceStart)!)"
                displayRating(rating: expDao!.ratings)
            }
        }else if(categoryId == AppConstants.WISHLIST_FESTIVAL){
            productid = wishlistDaoList[indexPath.row].product_id
            let festivalDao = FestivalDB().findAll().first
            if(festivalDao != nil){
                let festivalListDao = festivalDao!.festivalListDao
                let fevDao = festivalListDao.filter({$0.id == productid}).first
                self.generalDescriptionLabel.text = fevDao!.festivalName
                self.descriptionLabel.text = fevDao!.tagLine
                self.cityLabel.text = fevDao!.cityName+","+fevDao!.countryName
                self.priceLabel.text = "From: \(fevDao!.currencySign)\(Double(fevDao!.priceStart)!)"
                displayRating(rating: fevDao!.ratings)
            }
            
        }else if(categoryId == AppConstants.WISHLIST_TRENDING_ACTIVITY){
            productid = wishlistDaoList[indexPath.row].product_id
            let activityDao = ActivityDB().findAll().first
            if(activityDao != nil){
                let activityListDao = activityDao!.activityListDao
                let actvDao = activityListDao.filter({$0.id == productid}).first
                self.generalDescriptionLabel.text = actvDao!.heading
                self.descriptionLabel.text = actvDao!.tagLine
                self.cityLabel.text = actvDao!.cityName+","+actvDao!.countryName
                self.priceLabel.text = "From: \(actvDao!.currencySign)\(Double(actvDao!.priceStart)!)"
                displayRating(rating: actvDao!.ratings)
            }            
        }
            
        else if(categoryId == AppConstants.WISHLIST_DESTINATION){
            productid = wishlistDaoList[indexPath.row].product_id
            let searchExperienceModel = ApplicationManager.getInstance.searchExperienceModel
            if(searchExperienceModel != nil){
                let array = searchExperienceModel!.searchExpArray
                let actvDao = array.filter({$0.id == productid}).first
                if(actvDao != nil){
                    self.generalDescriptionLabel.text = actvDao!.heading
                    self.descriptionLabel.text = actvDao!.tagLine
                    self.cityLabel.text = actvDao!.city+","+actvDao!.countryName
                    self.priceLabel.text = "From: \(actvDao!.currencySign)\(Double(actvDao!.priceStart)!)"
                    displayRating(rating: actvDao!.ratings)
                }
            }
            
        }
        
        removeFromWishlist(indexPath: indexPath, wishListViewController: wishListViewController)
        
    }
    
    /*
     * Remove from wishlist on wishlist button click
     */
    func removeFromWishlist(indexPath: IndexPath,wishListViewController: WishListViewController){
        self.wishlistBtnTapAction = {
            () in
            let wishlistDB = WishlistDB()
            let wishlistDaoList = wishlistDB.findAll()
            wishlistDB.delete(wishlistDao: wishlistDaoList[indexPath.row])
            wishListViewController.collectionView.reloadData()
            
            //If no data available show view of "no wishlist"
            let count = wishlistDB.findAll().count
            if(count == 0){
                wishListViewController.mainView.isHidden = true
                wishListViewController.noWishlistView.isHidden = false
            }
        }
    }
    
    /*
     * Display images of rating
     */
    func displayRating(rating:String?){
        if(rating == "" || rating == "0"){
            rating1.isHidden = true
            rating2.isHidden = true
            rating3.isHidden = true
            rating4.isHidden = true
            rating5.isHidden = true
        }
        else{
            let imagesArray = AppUtils.displayRating(rating: rating)
            rating1.image = imagesArray![0]
            rating2.image = imagesArray![1]
            rating3.image = imagesArray![2]
            rating4.image = imagesArray![3]
            rating5.image = imagesArray![4]
        }
        
    }
}
