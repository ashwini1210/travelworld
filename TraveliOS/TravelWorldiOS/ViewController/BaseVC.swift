import UIKit
import SwiftyJSON
import RealmSwift
import Alamofire
import Toast_Swift


class BaseVC: UIViewController,UINavigationControllerDelegate,HttpResponseProtocol,ImageResponseProtocol   {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var activityIndicator: UIActivityIndicatorView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        let statusBarBGView = UIView(frame: UIApplication.shared.statusBarFrame)
        statusBarBGView.backgroundColor = AppUtils.hexStringToUIColor(hex: "#0F6A5B") //color of your choice
        view.addSubview(statusBarBGView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // self.navigationController?.isNavigationBarHidden = true
    }
    
    /*
     * Callback method of HttpresponseProtocol
     */
    func onHttpResponse(isSuccess: Bool, responseJson: JSON, error: AnyObject!, anyIdentifier: String) {
        
    }
    
    func onImageResponse(isSuccess: Bool?, image: UIImage?, error: AnyObject?, anyIdentifier: String?,id:String?) {
        
    }
    
    /*
     * Method to launch ViewControllers
     */
    func launchViewController(viewControllerName: String, dataToPass: Any? = nil){
        
        //Store instance of launching vc.
        var viewControllerInstance : UIViewController!
        
        //Get current vc name and set it as previous vc before launching new vc
        let currentVC : String? = AppPreference.getInstance().getString(AppConstants.AP_CURRENTLAUNCH_VC, defaultvalue: "")
        if (currentVC != nil) {
            AppPreference.getInstance().setString(AppConstants.AP_PREVIOUSLAUNCH_VC, value: currentVC!)
        }
        
        switch viewControllerName {
            
        case AppConstants.ParentTabBarViewController:
            viewControllerInstance = ParentTabBarViewController()
            AppPreference.getInstance().setString(AppConstants.AP_CURRENTLAUNCH_VC, value: AppConstants.ParentTabBarViewController)
            break
            
        case AppConstants.BookLocalViewController:
            viewControllerInstance = BookLocalViewController()
            AppPreference.getInstance().setString(AppConstants.AP_CURRENTLAUNCH_VC, value: AppConstants.BookLocalViewController)
            break
            
        case AppConstants.BookLocalNextViewController:
            viewControllerInstance = BookLocalNextViewController()
            if(dataToPass != nil){
                let vc = viewControllerInstance as! BookLocalNextViewController
                vc.cityTimeDictionary = dataToPass as? Dictionary<String, Any>
            }
            AppPreference.getInstance().setString(AppConstants.AP_CURRENTLAUNCH_VC, value: AppConstants.BookLocalNextViewController)
            break
            
        case AppConstants.LoginViewController:
            viewControllerInstance = LoginViewController()
            AppPreference.getInstance().setString(AppConstants.AP_CURRENTLAUNCH_VC, value: AppConstants.LoginViewController)
            break
            
        case AppConstants.EditLoginViewController:
            viewControllerInstance = EditLoginViewController()
            AppPreference.getInstance().setString(AppConstants.AP_CURRENTLAUNCH_VC, value: AppConstants.EditLoginViewController)
            break
            
        case AppConstants.AboutUsViewController:
            viewControllerInstance = AboutUsViewController()
            AppPreference.getInstance().setString(AppConstants.AP_CURRENTLAUNCH_VC, value: AppConstants.AboutUsViewController)
            break
            
        case AppConstants.FAQViewController:
            viewControllerInstance = FAQViewController()
            AppPreference.getInstance().setString(AppConstants.AP_CURRENTLAUNCH_VC, value: AppConstants.FAQViewController)
            break
            
        case AppConstants.OffersViewController:
            viewControllerInstance = OffersViewController()
            AppPreference.getInstance().setString(AppConstants.AP_CURRENTLAUNCH_VC, value: AppConstants.OffersViewController)
            break
            
        case AppConstants.RegisterViewController:
            viewControllerInstance = RegisterViewController()
            AppPreference.getInstance().setString(AppConstants.AP_CURRENTLAUNCH_VC, value: AppConstants.RegisterViewController)
            break
            
        case AppConstants.DetailViewController:
            viewControllerInstance = DetailViewController()
            let vc = viewControllerInstance as! DetailViewController
            vc.data = dataToPass as! [String : Any]
            AppPreference.getInstance().setString(AppConstants.AP_CURRENTLAUNCH_VC, value: AppConstants.DetailViewController)
            break
       
        case AppConstants.FestivalDetailViewController:
            viewControllerInstance = FestivalDetailViewController()
            let vc = viewControllerInstance as! FestivalDetailViewController
            vc.data = dataToPass as! [String : Any]
            AppPreference.getInstance().setString(AppConstants.AP_CURRENTLAUNCH_VC, value: AppConstants.FestivalDetailViewController)
            break
            
        case AppConstants.BookExperienceViewController:
            viewControllerInstance = BookExperienceViewController()
            let vc = viewControllerInstance as! BookExperienceViewController
            vc.dataFronDetail = dataToPass as? [String:Any]
            AppPreference.getInstance().setString(AppConstants.AP_CURRENTLAUNCH_VC, value: AppConstants.BookExperienceViewController)
            break
            
        case AppConstants.PaymentViewController:
            viewControllerInstance = PaymentViewController()
            AppPreference.getInstance().setString(AppConstants.AP_CURRENTLAUNCH_VC, value: AppConstants.PaymentViewController)
            if(dataToPass != nil){
            let vc = viewControllerInstance as! PaymentViewController
            vc.dataFromBooking = dataToPass as? [String : Any]
            }
            break
            
        case AppConstants.FindOutMoreViewController:
            viewControllerInstance = FindOutMoreViewController()
            AppPreference.getInstance().setString(AppConstants.AP_CURRENTLAUNCH_VC, value: AppConstants.FindOutMoreViewController)
            break
            
        default: break
            
        }
        pushScreenInNavigationController(vc: viewControllerInstance)
        
    }
    
    /*
     * Method to push screen in navigation stack
     */
    func pushScreenInNavigationController(vc: UIViewController)
    {
        //Push all viewcontroller in Navigationcontroller
        appDelegate.navController?.pushViewController(vc, animated: true)
    }
    
    /*
     * Functionality on click of back button
     */
    func onBackNavigation(identifier : String? = nil){
        appDelegate.navController?.popViewController(animated: true)
    }
    
    func showSpinner()
    {
        activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        
        // Position Activity Indicator in the center of the main view
        activityIndicator!.center = view.center
        
        activityIndicator!.style = .whiteLarge
        
        activityIndicator!.color = UIColor.blue
        
        // If needed, you can prevent Acivity Indicator from hiding when stopAnimating() is called
        activityIndicator!.hidesWhenStopped = true
        
        // Start Activity Indicator
        activityIndicator!.startAnimating()
        
        view.addSubview(activityIndicator!)        
    }
    
    func hideSpinner(){
        // Call stopAnimating() when need to stop activity indicator
        activityIndicator!.isHidden = true
        activityIndicator!.stopAnimating()
    }
    
    
    func goToGPSSettings(){
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)! as URL, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
        }
    }
    
    /*
     * Display alert with two buttons
     */
    func displayAlert(title:String,message: String,btn1:String,btn2:String){
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: btn1, style: UIAlertAction.Style.default,handler: nil))
        alertController.addAction(UIAlertAction(title: btn2, style: UIAlertAction.Style.default,handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    /*
     * Display alert with single buttons
     */
    func displayAlert(title:String,message: String,btn1:String){
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: btn1, style: UIAlertAction.Style.default,handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func displayConfirmationAlert(title:String,message: String,btn1:String,btn2:String, handler : ((UIAlertAction) -> Void)? = nil){
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        if(handler != nil){
            alertController.addAction(UIAlertAction(title: btn1, style: UIAlertAction.Style.default,handler: nil))
            alertController.addAction(UIAlertAction(title: btn2, style: UIAlertAction.Style.default,handler: handler))
        }else{
            alertController.addAction(UIAlertAction(title: btn1, style: UIAlertAction.Style.default,handler: nil))
            alertController.addAction(UIAlertAction(title: btn2, style: UIAlertAction.Style.default,handler: nil))
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    /*
     * Display toast message
     * https://github.com/scalessec/Toast-Swift
     */
    func showToast(message: String){
        self.view.makeToast(message, duration: 3.0, position: .bottom)
    }
}
