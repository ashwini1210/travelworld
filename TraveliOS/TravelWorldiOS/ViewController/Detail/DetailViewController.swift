//
//  DetailViewController.swift
//  DetailScreen
//
//  Created by Apple on 21/08/18.
//  Copyright © 2018 Appleechnopurple.com. All rights reserved.
//

import UIKit
import SwiftyJSON

class DetailViewController: BaseVC, UITextViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var data = [String:Any]()
    var detailModel : DetailModel?
    let reuseRecommendesIdentifier = "recommendedCell"
    var recommendedImagesArray = [UIImage]()
    var detailImagesArray = [UIImage]()
    var detailDB = DetailDB()
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    
    //View1
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var venueNameLabel: UILabel!
    @IBOutlet weak var venueDescriptionLabel: UITextView!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var innerView: UIView!
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    //View2
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var highliteTextView: UITextView!
    
    //View3
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var aboutAmigoTextView: UITextView!
    
    //View4
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var includeTextview: UITextView!
    
    //View5
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var experienceDescriptionLabel: UILabel!
    @IBOutlet weak var experienceDescriptionTextview: UITextView!
    
    //View6
    @IBOutlet weak var view6: UIView!
    @IBOutlet weak var meetUpTextview: UITextView!
    
    //View7
    @IBOutlet weak var view7: UIView!
    @IBOutlet weak var confirmationPolicyTextview: UITextView!
    
    //View8
    @IBOutlet weak var view8: UIView!
    @IBOutlet weak var cancellationPolicyTextview: UITextView!
    
    //View9
    @IBOutlet weak var view9: UIView!
    @IBOutlet weak var similarExpCollectionview: UICollectionView!
    
    @IBOutlet weak var bookButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    let currencyrate = AppPreference.getInstance().getInt(AppConstants.CURRENCY_RATE, defaultvalue: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        borderViews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewWillLayoutSubviews() {
        super .viewWillLayoutSubviews()
        initializeViews()
        initModel()
        setImageWithTimer()
        registerCollectionCell()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    /*
     * Method to initialize all views
     */
    func initializeViews(){
        loader.startAnimating()
        AppUtils.cornerRadius(view: backButton, roundCornerWidth: 5)
        AppUtils.cornerRadius(view: bookButton, roundCornerWidth: 5)
    }
    
    func registerCollectionCell(){
        similarExpCollectionview.delegate = self
        similarExpCollectionview.dataSource = self
        similarExpCollectionview.register(UINib.init(nibName: "RecommendedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseRecommendesIdentifier)
    }
    
    
    func initModel(){
        detailModel = DetailModel(delegate: self)
        detailModel!.requestForDetail(expId: Int((data["id"] as? String)!)!)
    }
    
    func changeMainViewFrame(){
        //Change main view height
        var viewFrame = mainView.frame
        
        let height = view1.frame.size.height + view2.frame.size.height + view3.frame.size.height
        let height2 =   height + view4.frame.size.height + view5.frame.size.height + view6.frame.size.height
        let height3 =  height2 + view7.frame.size.height + view8.frame.size.height + view9.frame.size.height
        viewFrame.size = CGSize(width: self.view.frame.width , height: imageView.frame.size.height + height3 + 170)
        
        mainView.frame = viewFrame
        
        //Increase scroll view size as per screen
        self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: mainView.frame.size.height)
    }
    
    /*
     * Change position of Parent View as per increase size of Textview
     */
    func changePositionOfViews(){
        //Change Y coordinate of views
        self.view2.frame.origin.y = view1.frame.origin.y + (view1.frame.size.height ) + 20
        self.view3.frame.origin.y = view2.frame.origin.y + (view2.frame.size.height ) + 20
        self.view4.frame.origin.y = view3.frame.origin.y + (view3.frame.size.height ) + 20
        self.view5.frame.origin.y = view4.frame.origin.y + (view4.frame.size.height ) + 20
        self.view6.frame.origin.y = view5.frame.origin.y + (view5.frame.size.height ) + 20
        self.view7.frame.origin.y = view6.frame.origin.y + (view6.frame.size.height ) + 20
        self.view8.frame.origin.y = view7.frame.origin.y + (view7.frame.size.height ) + 20
        self.view9.frame.origin.y = view8.frame.origin.y + (view8.frame.size.height ) + 20
    }
    
    /*
     * Method to set different images every second
     */
    func setImageWithTimer(){
        imageView.animationImages = detailImagesArray
        imageView.animationDuration = 10
        imageView.startAnimating()
    }
    
    /*
     * Method to Highlight and border view
     */
    func borderViews(){
        AppUtils.shadowView(view: view1, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        AppUtils.shadowView(view: view2, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        AppUtils.shadowView(view: view3, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        AppUtils.shadowView(view: view4, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        AppUtils.shadowView(view: view5, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        AppUtils.shadowView(view: view6, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        AppUtils.shadowView(view: view7, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        AppUtils.shadowView(view: view8, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
        AppUtils.shadowView(view: view9, shadowColour: UIColor.gray.cgColor, shadowRadius: 3)
    }
    
    /*
     * Change height of experience Description Textview
     */
    func textViewDidChange(textView: UITextView, parentview: UIView) {
        
        let fixedWidth = textView.frame.size.width
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        textView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        
        if(parentview == view1){
            var viewFrame1 = innerView.frame
            viewFrame1 = CGRect(x: self.innerView.frame.origin.x, y: (self.venueNameLabel.frame.size.height + textView.frame.size.height), width: self.innerView.frame.size.width, height: self.innerView.frame.size.height)
            innerView.frame = viewFrame1
            
            var viewFrame = parentview.frame
            viewFrame.size = CGSize(width: parentview.frame.size.width , height: textView.frame.size.height  + innerView.frame.size.height + experienceDescriptionLabel.frame.size.height)
            parentview.frame = viewFrame
        }
        else{
            var viewFrame = parentview.frame
            viewFrame.size = CGSize(width: parentview.frame.size.width , height: textView.frame.size.height  + experienceDescriptionLabel.frame.size.height)
            parentview.frame = viewFrame
        }
    }
    
    
    override func onHttpResponse(isSuccess: Bool, responseJson: JSON, error: AnyObject!, anyIdentifier: String) {
        if(isSuccess){
            if(anyIdentifier == AppConstants.DETAIL){
                setDataInViews()
                
                let detailDB = self.detailDB.findAll().first
                if(detailDB != nil){
                    let detailImagesList = detailDB!.detailImagesDao
                    for detailImages in detailImagesList{
                        let downloadImages = DownloadImages.init(delegate: self)
                        downloadImages.downloadImageAsync(URLString: detailImages.image_url, placeHolderImage: UIImage(named: "exp1.jpg")!,identifier: AppConstants.DETAIL_EXPERIENCE, id: detailImages.id)
                    }
                    
                    let similarDetailListDao = detailDB!.similarDetailDao
                    for similarDetailDao in similarDetailListDao{
                        let downloadImages = DownloadImages.init(delegate: self)
                        downloadImages.downloadImageAsync(URLString: similarDetailDao.card_image_url, placeHolderImage: UIImage(named: "exp1.jpg")!,identifier: AppConstants.SIMILAR_EXPERIENCE, id: similarDetailDao.id)
                        recommendedImagesArray.append(UIImage(named: "exp1.jpg")!)
                    }
                    similarExpCollectionview.reloadData()
                    
                }
            }
        }
    }
    
    
    func setDataInViews(){
        let detailDao = detailDB.findAll().first
        if(detailDao != nil){
            venueNameLabel.text = detailDao!.heading
            venueDescriptionLabel.text = detailDao!.tag_line
            placeLabel.text = detailDao!.city
            timeLabel.text = "\(detailDao!.duration_min) - \(detailDao!.duration_max) Hrs"
            priceLabel.text = "Price: \(AppUtils.getCurrencySign()) \(AppUtils.calculateTravelPrice(basePrice: Int(detailDao!.base_rate)!))"
            textViewDidChange(textView: self.venueDescriptionLabel, parentview: view1)
            
            var majorAttraction = ""
            for item in detailModel!.majorAttractionArray{
                majorAttraction =  majorAttraction+"<html><body><font color=\"#ff5a60\">○</font>&ensp;</body></html>"+item.stringValue+"<br>"
            }
            highliteTextView.attributedText = AppUtils.addHTMLTextInString(text: majorAttraction)
            highliteTextView.font = UIFont.systemFont(ofSize: 14.0)
            textViewDidChange(textView: self.highliteTextView, parentview: view2)
            
            aboutAmigoTextView.text = detailDao!.about_amigo
            textViewDidChange(textView: self.aboutAmigoTextView, parentview: view3)
            
            var include = ""
            for item in detailModel!.includedArray{
                include =  include+"<html><body><font color=\"#075e54\">✓</font>&ensp;</body></html>"+item.stringValue+"<br>"
            }
            for item in detailModel!.notIncludedArray{
                include =  include+"<html><body><font color=\"#ff5a60\">✕</font>&ensp;</body></html>"+item.stringValue+"<br>"
            }
            includeTextview.attributedText = AppUtils.addHTMLTextInString(text: include)
            includeTextview.font = UIFont.systemFont(ofSize: 14.0)
            textViewDidChange(textView: self.includeTextview, parentview: view4)
            
            experienceDescriptionTextview.text = detailDao!.desc
            textViewDidChange(textView: self.experienceDescriptionTextview, parentview: view5)
            
            meetUpTextview.text = detailDao!.pickup_point
            textViewDidChange(textView: self.meetUpTextview, parentview: view6)
            
            confirmationPolicyTextview.text = detailDao!.confirmation_policy
            textViewDidChange(textView: self.confirmationPolicyTextview, parentview: view7)
            
            cancellationPolicyTextview.text = detailDao!.cancellation_policy
            textViewDidChange(textView: self.cancellationPolicyTextview, parentview: view8)
            
            changePositionOfViews()
            changeMainViewFrame()
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == similarExpCollectionview ){
            let itemWidth = collectionView.bounds.size.width
            let itemHeight = collectionView.bounds.size.height
            return CGSize(width: itemWidth, height: itemHeight)
        }
        return CGSize(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recommendedImagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseRecommendesIdentifier, for: indexPath) as! RecommendedCollectionViewCell
        cell.setDetailScreenDataInCell(detailViewController: self, indexPath: indexPath)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailDao =  detailDB.findAll().first
        if(detailDao != nil){
            let similarDetailDao = detailDao!.similarDetailDao[indexPath.row]
            data["id"] = similarDetailDao.id
        }
        self.launchViewController(viewControllerName: AppConstants.DetailViewController,dataToPass: data)
    }
    
    override func onImageResponse(isSuccess: Bool?, image: UIImage?, error: AnyObject?, anyIdentifier: String?, id: String?) {
        //TOP EXPERIENCE IMAGE RESPONSE
        if(anyIdentifier == AppConstants.SIMILAR_EXPERIENCE){
            if(image != nil){
                let similarDetailDao = detailDB.findAll().first!.similarDetailDao
                let resultPredicate = NSPredicate(format: "id = %@", id!)
                let index = similarDetailDao.index(matching: resultPredicate)
                if(index != nil){
                    recommendedImagesArray[index!] = image!
                    detailDB.realm.beginWrite()
                    similarDetailDao[index!].image_base64 = AppUtils.convertImageToBase64(image: image!)
                    detailDB.realm.add(similarDetailDao[index!], update: true)
                    try! detailDB.realm.commitWrite()
                }
            }
            similarExpCollectionview.reloadData()
        }
            
        else if(anyIdentifier == AppConstants.DETAIL_EXPERIENCE){
            loader.startAnimating()
            loader.isHidden = true
            if(image != nil){
                detailImagesArray.append(image!)
            }
            setImageWithTimer()
        }
    }
    
    @IBAction func onBookNowBtnClicked(_ sender: Any) {
        var dataPost = [String:Any]()
        let detailDao = detailDB.findAll().first
        if(detailDao != nil){
            dataPost["identifier"] = data["identifier"]
            dataPost["min_guest"] = detailDao!.min_guests
            dataPost["base_price"] = detailDao!.base_rate
            dataPost["exp_id"] = data["id"] as? String
        }
        
        self.launchViewController(viewControllerName: AppConstants.BookExperienceViewController, dataToPass: dataPost)
    }
    
    @IBAction func onBackButtonClicked(_ sender: Any) {
        onBackNavigation()
    }
}
