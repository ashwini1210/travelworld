//
//  OffersViewController.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 02/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit

class OffersViewController: BaseVC,UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    var offerImagesArray = [UIImage]()
    let reuseIdentifier = "offerCell"
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCollectionCell()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {       
        initailizeViews()
    }
    
    /*
     * Initialize all view on screen load
     */
    func initailizeViews(){
        let offerDao = OffersDB().findAll().first
        count = offerDao!.offersListDao.count
        collectionView.reloadData()
    }
    
    func registerCollectionCell(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: "OffersCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! OffersCollectionViewCell
        cell.setOffersDataInCell(offersViewController: self, indexPath: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
    @IBAction func onBackButtonClicked(_ sender: Any) {
        onBackNavigation()
    }
}
