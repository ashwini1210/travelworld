//
//  OffersCollectionViewCell.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 02/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit

class OffersCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var offerImageView: UIImageView!
    @IBOutlet weak var offerDescriptionLabel: UILabel!
    @IBOutlet weak var offerCodeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setOffersDataInCell(offersViewController: OffersViewController,indexPath: IndexPath){
        let offersDao = OffersDB().findAll().first
        if(offersDao != nil){
            let offersListDao = offersDao!.offersListDao
            self.offerDescriptionLabel.text = offersListDao[indexPath.row].desc
            self.offerCodeLabel.text = offersListDao[indexPath.row].name
            if(offersListDao[indexPath.row].image_base64 != ""){
                self.offerImageView.image = AppUtils.convertBase64ToImage(imageString: offersListDao[indexPath.row].image_base64)
            }
            else{
                self.offerImageView.image = UIImage(named: "offer1.jpg")
            }
        }
    }
}
