//
//  DestinationViewController.swift
//  Search App
//
//  Created by Apple on 06/08/18.
//  Copyright © 2018 Appleechnopurple.com. All rights reserved.
//

import UIKit


class DestinationViewController: BaseVC, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var mumbaiBtn: UIButton!
    @IBOutlet weak var delhiBtn: UIButton!
    @IBOutlet weak var hongKongBtn: UIButton!
    @IBOutlet weak var dubaiBtn: UIButton!
    @IBOutlet weak var singaporeBtn: UIButton!
    @IBOutlet weak var tokyoBtn: UIButton!
    @IBOutlet weak var baliBtn: UIButton!
    @IBOutlet weak var istanbulBtn: UIButton!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var worldView: UIView! //Load ExtensionViewController
    @IBOutlet weak var placesListCollection: UIView! //Load PlacesListViewController
    
    var dataFromHome : [String : Any]? = nil
    var searching = false
    var placesListViewController : PlacesListViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeViews()
        registerController()
        registerForKeyboardNotifications()
        hideKeyboardWhenTappedAround()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showCityListOnTopDestination()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        let borderColour = AppUtils.hexStringToUIColor(hex: "#0F6A5B").cgColor
        AppUtils.borderView(view: mumbaiBtn, borderWidth: 1, borderColour: borderColour)
        AppUtils.borderView(view: delhiBtn, borderWidth: 1, borderColour: borderColour)
        AppUtils.borderView(view: hongKongBtn, borderWidth: 1, borderColour: borderColour)
        AppUtils.borderView(view: dubaiBtn, borderWidth: 1, borderColour: borderColour)
        AppUtils.borderView(view: singaporeBtn, borderWidth: 1, borderColour: borderColour)
        AppUtils.borderView(view: baliBtn, borderWidth: 1, borderColour: borderColour)
        AppUtils.borderView(view: tokyoBtn, borderWidth: 1, borderColour: borderColour)
        AppUtils.borderView(view: istanbulBtn, borderWidth: 1, borderColour: borderColour)
    }
    
    func initializeViews(){
        placesListCollection.isHidden = true
    }
    
    func registerController(){
        //Expandable Table view
        let extensionViewController = ExtensionViewController()
        extensionViewController.destinationViewController = self
        extensionViewController.view.frame = worldView.bounds
        worldView.addSubview(extensionViewController.view)
        addChild(extensionViewController)
        extensionViewController.didMove(toParent: self)
        
        placesListViewController = PlacesListViewController()
        placesListViewController!.view.frame = placesListCollection.bounds
        placesListCollection.addSubview(placesListViewController!.view)
        addChild(placesListViewController!)
        placesListViewController!.didMove(toParent: self)
    }
    
    @IBAction func cityButtonClicked(_ sender: Any) {
        let btn = sender as! UIButton
        var showPlacesVC = false
        if(btn.tag == 1){
            showPlacesVC = true
            searchBar.text = "Mumbai"
        }
        else if(btn.tag == 2){
            showPlacesVC = true
            searchBar.text = "Delhi"
        }
        else if(btn.tag == 3){
            showPlacesVC = true
            searchBar.text = "HongKong"
        }
        else if(btn.tag == 4){
            showPlacesVC = true
            searchBar.text = "Dubai"
        }
        else if(btn.tag == 5){
            showPlacesVC = true
            searchBar.text = "Singapore"
        }
        else if(btn.tag == 6){
            showPlacesVC = true
            searchBar.text = "Tokyo"
        }
        else if(btn.tag == 7){
            showPlacesVC = true
            searchBar.text = "Bali"
        }
        else if(btn.tag == 8){
            showPlacesVC = true
            searchBar.text = "Istanbul"
        }
        
        if(showPlacesVC){
            placesListCollection.isHidden = false
            placesListViewController?.searchChar = searchBar.text
            self.placesListViewController!.initalizeModel()
        }
    }
    
    
    /************************ SEARCH BAR CODE ************************/
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //On back button, when search text clear upto 2 count, show countryView and again hide searchResultTableView
        if(searchText.count < 2){
            countryView.isHidden = false
        }else{
            countryView.isHidden = true //Hide background of countries
            placesListCollection.isHidden = false
            self.placesListViewController!.searchChar = searchText
            self.placesListViewController!.initalizeModel()
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false //Disble searching
        searchBar.text = "" //Clear data of search text field
        countryView.isHidden = false //Show background of countries
        self.searchBar.endEditing(true)
        if(!placesListCollection.isHidden){
            placesListCollection.isHidden = true
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        self.searchBar.endEditing(true)
    }
    
    /*
     * DIsplay places list screen if this screen launch on click of Top Destination
     */
    func showCityListOnTopDestination(){
        if(dataFromHome != nil){
            if dataFromHome!["identifier"] as! String == AppConstants.TOP_DESTINATION{
                placesListCollection.isHidden = false
                searchBar.text = (dataFromHome!["city"] as! String)
                placesListViewController?.searchChar = searchBar.text
                self.placesListViewController!.initalizeModel()
            }
        }
    }
    
    /*************** KEYBOARD CODE *******************/
    /*
     * Register keyboard open close notification
     */
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
    }
    
    /*
     * Hide keyboard on outer touch
     */
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    deinit {
        deregisterFromKeyboardNotifications()
    }
    
    
}
