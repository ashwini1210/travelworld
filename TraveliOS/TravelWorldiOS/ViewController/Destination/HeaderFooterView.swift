//
//  HeaderFooterView.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 21/08/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit

protocol ExpandHeaderViewDelegate {
    func toggleSection(header :HeaderFooterView, section : Int)
}

class HeaderFooterView: UITableViewHeaderFooterView {
    var delegate : ExpandHeaderViewDelegate?
    var section : Int!
    
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    
    
    func addTapGesture(){
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectHeaderAction)))
    }
    
    @objc func selectHeaderAction(gestureRecognizer : UITapGestureRecognizer){
        let cell  = gestureRecognizer.view as! HeaderFooterView
        delegate!.toggleSection(header :self, section : cell.section)
    }
    
    func customInit(title: String, image: UIImage, section : Int, delegate : ExpandHeaderViewDelegate){
        addTapGesture()
        self.headerLabel!.text = title
        self.headerImageView!.image = image
        self.section = section
        self.delegate = delegate
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
}
