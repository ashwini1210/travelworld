//
//  ExtensionViewController.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 20/08/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Section {
    var country : String!
    var city : [String]!
    var expaned : Bool
    init(country : String,city : [String],expaned : Bool) {
        self.country = country
        self.city = city
        self.expaned = expaned
    }
}

class ExtensionViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, HttpResponseProtocol, ExpandHeaderViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    let identifier = "cell"
    var tableviewData = [Section]()
    var destinationViewController : DestinationViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        initializeModels()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initializeView(){
        //Register Custom Cell
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        let headerNib = UINib.init(nibName: "HeaderFooterView", bundle: Bundle.main)
        tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "HeaderFooterView")
        
        self.tableView.register(UINib(nibName: "ExpandTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableviewData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableviewData[section].city.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 62
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(tableviewData[indexPath.section].expaned){
            return 62
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderFooterView") as! HeaderFooterView
        headerView.customInit(title: tableviewData[section].country, image: UIImage(named:"topDest2.jpg")!, section: section, delegate: self)
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //This cell show inside header
        let cell = self.tableView.dequeueReusableCell(withIdentifier: identifier) as! ExpandTableViewCell
        cell.countryLabel.text = tableviewData[indexPath.section].city[indexPath.row]
        return cell
    }
    
    func toggleSection(header: HeaderFooterView, section: Int) {
        if(tableviewData[section].expaned){
            header.arrowImage.transform = CGAffineTransform(rotationAngle: 0)
            tableviewData[section].expaned = false
        }else{
            header.arrowImage.transform =  CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            tableviewData[section].expaned = true
        }
        
        tableView.beginUpdates()
        for i in 0 ..< tableviewData[section].city.count{
            tableView.reloadRows(at: [IndexPath(row: i,section:section)], with: .automatic)
        }
        tableView.endUpdates()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRowAt: \(tableviewData[indexPath.section].city[indexPath.row])")
        let cityName = tableviewData[indexPath.section].city[indexPath.row]
        destinationViewController?.searchBar.text = cityName
        destinationViewController?.placesListCollection.isHidden = false
        destinationViewController?.placesListViewController?.searchChar = cityName
        destinationViewController?.placesListViewController?.initalizeModel()
    }
    
    func initializeModels(){
        let cityCountryModel = CityCountryModel.init(delegate: self)
        cityCountryModel.requestForCityCountryData()
    }
    
    func onHttpResponse(isSuccess: Bool, responseJson: JSON, error: AnyObject!, anyIdentifier: String) {
        if(anyIdentifier == AppConstants.CITY_COUNTRY){
            if responseJson["status"].boolValue{
                var cityArray = [String]()
                let countryDao = CountryDB().findAll().first
                if(countryDao != nil){
                    var cityList = countryDao!.AustraliacityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    
                    cityList = countryDao!.AustriacityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.BelgiumcityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.CambodiacityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.EnglandcityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.FrancecityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.GermanycityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.GreececityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.HongKongcityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.HungarycityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.IndiacityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.IndonesiacityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.ItalycityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.JapancityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.MalaysiacityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.MexicocityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.NetherlandscityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.NewZealandcityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.PortugalcityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.RussiacityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.SingaporecityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.SpaincityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.SriLankacityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.SwedencityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.ThailandcityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.TurkeycityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.USAcityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.UnitedArabEmiratescityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                    
                    cityList = countryDao!.VietnamcityDaoList
                    cityArray = [String]()
                    for cityDao in cityList{
                        cityArray.append(cityDao.city_name)
                    }
                    tableviewData.append(Section(country: cityList.first!.country_name, city: cityArray, expaned: false))
                }
                tableView.reloadData()
            }
        }
    }
    
}
