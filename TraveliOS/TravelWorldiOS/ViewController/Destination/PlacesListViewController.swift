//
//  PlacesListViewController.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 02/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SwiftyJSON

class PlacesListViewController: BaseVC , UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    var placesImagesArray = [UIImage]()
    let reuseIdentifier = "placesCell"
    var searchChar : String?
    var isResponseFromModel = false
    var placesModel : SearchExperienceModel? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCollectionCell()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        initailizeViews()
    }
    /*
     * Initialize all view on screen load
     */
    func initailizeViews(){
        
    }
    
    func initalizeModel(){
        if(!isResponseFromModel){
            isResponseFromModel = true
            placesModel = SearchExperienceModel.init(delegate: self)
            ApplicationManager.getInstance.searchExperienceModel = placesModel
            if(searchChar != nil){
                var data = [
                    "q" : searchChar! as Any,
                    "device_id" :  "ashwini@gmail.com" as Any
                    ] as [String:Any]?
                placesModel!.requestForPlaces(searchChar: data)
                data = nil
            }
        }
    }
    
    override func onHttpResponse(isSuccess: Bool, responseJson: JSON, error: AnyObject!, anyIdentifier: String) {
        if(anyIdentifier == AppConstants.PLACES){
            if(placesImagesArray.count != 0){
                placesImagesArray.removeAll()
            }
            onPlacesResponse(isSuccess: isSuccess, responseJson:responseJson)
        }
    }
    
    func onPlacesResponse(isSuccess: Bool, responseJson: JSON!){
        if isSuccess {
            if responseJson["status"].boolValue{
                for experienceDao in  placesModel!.searchExpArray{
                    let downloadImages = DownloadImages.init(delegate: self)
                    downloadImages.downloadImageAsync(URLString: experienceDao.cardimageURL, placeHolderImage: UIImage(named: "blank.jpg")!,identifier: AppConstants.PLACES, id: experienceDao.id)
                    placesImagesArray.append(UIImage(named: "blank.jpg")!)
                }
                collectionView.reloadData()
                isResponseFromModel = false
            }
        }
        else {
            //print("Error to onTopExperienceResponse: \(responseJson["message"].stringValue)")
        }
    }
    
    override func onImageResponse(isSuccess: Bool?, image: UIImage?, error: AnyObject?, anyIdentifier: String?, id: String?) {
        if(anyIdentifier == AppConstants.PLACES){
            if(image != nil){
                let experienceListDao = placesModel!.searchExpArray
                if(experienceListDao.count != 0){
                    let resultPredicate = NSPredicate(format: "id = %@", id!)
                    let index = experienceListDao.index(matching: resultPredicate)
                    if(index != nil){
                        placesImagesArray[index!] = image!
                    }
                }
            }
            collectionView.reloadData()
        }
    }
    
    func registerCollectionCell(){
        collectionView.register(UINib.init(nibName: "PlacesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return placesImagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PlacesCollectionViewCell
        cell.setExperienceDataInCell(placesListViewController: self, indexPath: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if placesModel!.searchExpArray.count != 0{
            let dao = placesModel!.searchExpArray[indexPath.row]
            var data = [String:Any]()
            data["identifier"] = AppConstants.PLACES as String
            data["id"] = dao.id
            if(dao.identifier == AppConstants.TOP_EXPERIENCE){
                self.launchViewController(viewControllerName: AppConstants.DetailViewController,dataToPass: data)
            }else if(dao.identifier == AppConstants.FESTIVAL){
                self.launchViewController(viewControllerName: AppConstants.FestivalDetailViewController,dataToPass: data)
            }
        }
    }
    
}
