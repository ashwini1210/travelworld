//
//  PlacesCollectionViewCell.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 02/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import SwiftyJSON

class PlacesCollectionViewCell: UICollectionViewCell,HttpResponseProtocol  {
    
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var generalDescriptionLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var recommendedImageview: UIImageView!
    @IBOutlet weak var wishlistButton: UIButton!
    @IBOutlet weak var rating1: UIImageView!
    @IBOutlet weak var rating2: UIImageView!
    @IBOutlet weak var rating3: UIImageView!
    @IBOutlet weak var rating4: UIImageView!
    @IBOutlet weak var rating5: UIImageView!
    var placesListViewController : PlacesListViewController?
    var currency_sign = ""
    var currency_rate = 0

    
    // this will be our "call back" action
    var wishlistBtnTapAction : (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func wishlistButtonClicked(_ sender: Any) {
        // use our "call back" action to tell the controller the button was tapped
        wishlistBtnTapAction?()
    }
    
    /*
     * Method to set Experience cell Data
     */
    func setExperienceDataInCell(placesListViewController: PlacesListViewController,indexPath: IndexPath){
        self.recommendedImageview.image = placesListViewController.placesImagesArray[indexPath.row]
        let placesModel = placesListViewController.placesModel
        if(placesModel != nil){
            let experienceDao = placesModel!.searchExpArray
            if(experienceDao.count != 0){
                self.generalDescriptionLabel.text = experienceDao[indexPath.row].heading
                self.descriptionLabel.text = experienceDao[indexPath.row].tagLine
                self.cityLabel.text = experienceDao[indexPath.row].city+","+experienceDao[indexPath.row].countryName
                //self.priceLabel.text = "From: \(experienceDao[indexPath.row].currencySign)\(Double(experienceDao[indexPath.row].priceStart)!)"
                currency_sign = AppPreference.getInstance().getString(AppConstants.CURRENCY_SIGN, defaultvalue: "")
                currency_rate = AppPreference.getInstance().getInt(AppConstants.CURRENCY_RATE, defaultvalue: 0)
                self.priceLabel.text = "From: \(currency_sign)\(Double(currency_rate * Int(experienceDao[indexPath.row].priceStart)!))"
                print("experienceDao[indexPath.row].ratings: \(experienceDao[indexPath.row].ratings)")
                displayRating(rating: experienceDao[indexPath.row].ratings)
                displayWishlistImage(productId: experienceDao[indexPath.row].id)
                initializeWishlistModel(product_id: experienceDao[indexPath.row].id, vertical_id: AppConstants.WISHLIST_DESTINATION, identifier: AppConstants.PLACES, image: self.recommendedImageview.image, vc: placesListViewController)
            }
        }
    }
    
    /*
     * Method to show wishlist image as per wishlist status.
     */
    func displayWishlistImage(productId: String?){
        let wishlistdao = WishlistDB().findByProperty(property: "product_id", value: productId!).first
        if(wishlistdao != nil){
            if( wishlistdao!.addedWishlist){
                self.wishlistButton.setImage(UIImage(named: "heart.png"), for: UIControl.State.normal)
            }
        }else{
            self.wishlistButton.setImage(UIImage(named: "heart_white.png"), for: UIControl.State.normal)
        }
    }
    
    func initializeWishlistModel(product_id :String, vertical_id : String, identifier: String, image: UIImage?, vc: PlacesListViewController){
        // set a "Callback Closure" in the cell
        self.wishlistBtnTapAction = {
            () in
            var str64 : String? = ""
            if(image != nil){
                str64 = AppUtils.convertImageToBase64(image: image!)
            }
            var data = [
                "product_id" : product_id as Any,
                "vertical_id" : vertical_id as Any,
                "imagebase64" : str64 as Any,
                "device_id" :  "ashwini@gmail.com" as Any
                ] as [String:Any]?
            
            let wishlistModel = WishlistModel.init(delegate: self)
            wishlistModel.requestToAddWishlist(wishlistData: data, identifier: identifier)
            vc.showToast(message: AppConstants.ADDING_WISHLIST_MSG)
            data = nil
        }
    }
    
    func onHttpResponse(isSuccess: Bool, responseJson: JSON, error: AnyObject!, anyIdentifier: String) {
        if(anyIdentifier == AppConstants.PLACES){
            self.wishlistButton.setImage(UIImage(named: "heart.png"), for: UIControl.State.normal)
        }
    }
    
    /*
     * Display images of rating
     */
    func displayRating(rating:String?){
        if(rating == "" || rating == "0"){
            rating1.isHidden = true
            rating2.isHidden = true
            rating3.isHidden = true
            rating4.isHidden = true
            rating5.isHidden = true
        }
        else{
            let imagesArray = AppUtils.displayRating(rating: rating)
            rating1.image = imagesArray![0]
            rating2.image = imagesArray![1]
            rating3.image = imagesArray![2]
            rating4.image = imagesArray![3]
            rating5.image = imagesArray![4]
        }
        
    }
}
