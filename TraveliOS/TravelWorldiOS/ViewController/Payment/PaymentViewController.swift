//
//  PaymentViewController.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 17/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import WebKit
import SwiftyJSON
import Razorpay

class PaymentViewController: BaseVC,RazorpayPaymentCompletionProtocol  {
    

    @IBOutlet weak var loader: UIActivityIndicatorView!
    var dataFromBooking : [String:Any]? = [:]
    var razorpay: Razorpay!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loader.startAnimating()
        initModel()
        initializePayment()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    func initModel(){
        razorpay = Razorpay.initWithKey("rzp_live_N4JqkKplI2749I", andDelegate: self)
    }
    
    func initializePayment(){
        if(dataFromBooking != nil){
        //Convert rupee in paisa
            let pr = dataFromBooking!["price"] as! String
            let amount = (Double(pr)!) * 100.0
        let options: [String:Any] = [
            "amount" : "\(amount)", //mandatory in paise
            "description": "Booking from iOS App",
            //"image": "https://url-to-image.png",
            "name": AppPreference.getInstance().getString(AppConstants.FIRST_NAME, defaultvalue: ""),
            "prefill": [
                "contact": AppPreference.getInstance().getString(AppConstants.PHONE, defaultvalue: ""),
                "email": AppPreference.getInstance().getString(AppConstants.EMAIL, defaultvalue: "")
            ],
            "theme": [
                "color": "#F37254"
            ]
        ]
        razorpay.open(options)
    }
    }
    
    
    func onPaymentError(_ code: Int32, description str: String) {
        self.showToast(message: AppConstants.TRY_AGAIN_MSG)
        self.onBackNavigation()
        //        let alertController = UIAlertController(title: "FAILURE", message: str, preferredStyle: UIAlertController.Style.alert)
        //        let cancelAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        //        self.view.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        self.showToast(message: "Payment done successfully")
        self.onBackNavigation()
        //        let alertController = UIAlertController(title: "SUCCESS", message: "Payment Id \(payment_id)", preferredStyle: UIAlertController.Style.alert)
        //        let cancelAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        //        self.view.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func onBackbuttonClicked(_ sender: Any) {
        self.onBackNavigation()
    }
}
