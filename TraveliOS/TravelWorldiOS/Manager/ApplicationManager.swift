//
//  InstanceManager.swift
//  effyIos
//
//  Created by Apple on 04/04/17.
//  Copyright © 2017 Appleechnopurple.com. All rights reserved.
//

import UIKit

class ApplicationManager: NSObject {
    
    override init() {
        super.init()
    }
    
    //ApplicationManager instance
    static let getInstance: ApplicationManager = ApplicationManager()
    var searchCityModel : SearchCityModel? = nil
    var searchExperienceModel : SearchExperienceModel? = nil
}
