
import UIKit
import RealmSwift
import Alamofire

class AppUtils: NSObject {
    
    /*
     * Method to convert hex number into colour
     */
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    /*
     * Method to give border and colour to any view
     */
    static func borderView(view: UIView, borderWidth : CGFloat, borderColour: CGColor?){
        if(borderColour != nil){
            view.layer.borderColor = borderColour
            view.layer.borderWidth = borderWidth
        }
    }
    
    /*
     * Method to give shadow and colour to any view
     */
    static func shadowView(view: UIView,shadowColour: CGColor?, shadowRadius:CGFloat){
        view.layer.shadowColor = shadowColour
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = shadowRadius
    }
    
    /*
     * Method to apply corner radius to any view
     */
    static func cornerRadius(view: UIView,roundCornerWidth : CGFloat){
        view.layer.cornerRadius = roundCornerWidth
    }
    
    //
    // Convert String to base64
    //
    static func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.pngData()!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    //
    // Convert base64 to String
    //
    static func convertBase64ToImage(imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }
    
    static func isValidEmail(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        print(result)
        return result
    }
    
    static func isValidPhone(value: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    static func formatDateString(dateString : String) -> String? {
        let dateString : String? = dateString
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date : Date? = dateFormatter.date(from: dateString!)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "dd MMM yyyy h:mm a"
        let strDate = dateFormatter2.string(from: date!)
        
        return strDate
    }
    
    static func formatDateString(dateString : String, originalformat : String, expectedformat : String) -> String? {
        let dateString : String? = dateString
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = originalformat
        let date : Date? = dateFormatter.date(from: dateString!)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = expectedformat
        let strDate = dateFormatter2.string(from: date!)
        
        return strDate
    }
    
    static func getDayOfWeek(date:String) -> String? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: date) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        if(weekDay == 1){
            return "Sun"
        }
        else if(weekDay == 2){
            return "Mon"
        }
        else if(weekDay == 3){
            return "Tue"
        }
        else if(weekDay == 4){
            return "Wed"
        }
        else if(weekDay == 5){
            return "Thur"
        }
        else if(weekDay == 6){
            return "Fri"
        }
        else if(weekDay == 7){
            return "Sat"
        }
        return ""
    }
    
    static func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    //    /*
    //     * Convert array of dictionary (key:String , value:Any) to string format
    //     */
    //    static func convertArrayToString(arrayOfDictionaryObj: Any) -> String? {
    //        do{
    //            //create a dictionary into data format
    //            let data : Data? = try JSONSerialization.data(withJSONObject: arrayOfDictionaryObj, options: JSONSerialization.WritingOptions(rawValue: 0))
    //
    //            //create data in string and send to JS
    //            let Str: String? = String(data: data!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as String
    //
    //            return Str
    //        } catch let error as NSError{
    //            print("Error in convertArrayToString while converting data : \(error)")
    //        }
    //        return nil
    //
    //    }
    
    static func getCurrencySign() -> String{
        let currencySign = AppPreference.getInstance().getString(AppConstants.CURRENCY_SIGN, defaultvalue: "")
        return currencySign
    }
    
    static func calculateTravelPrice(basePrice: Int) -> String{
        let currencyRate = AppPreference.getInstance().getInt(AppConstants.CURRENCY_RATE, defaultvalue: 0)
        let price : Double = Double(currencyRate * basePrice)
        return String(price)
    }
    
    static func convertArrayToString(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    static func addHTMLTextInString(text:String) -> NSAttributedString{
        let data = text.data(using: String.Encoding.unicode)! // mind "!"
        let attrStr = try? NSAttributedString( // do catch
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        return attrStr!
    }
    
    static func convertTimeTo24Hrs(time12Hrs: String?) -> String?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        
        let date = dateFormatter.date(from: time12Hrs!)
        dateFormatter.dateFormat = "HH:mm"
        
        let Date24 = dateFormatter.string(from: date!)
        print("24 hour formatted Date:",Date24)
        return Date24
    }
    
    /*
     * Border to bottom of textfield
     */
   static func setBottomLine(borderColor: UIColor,uiView: UIView) {
        //uiView.borderStyle = UITextBorderStyle.none
        uiView.backgroundColor = UIColor.clear
        
        let borderLine = UIView()
        let height = 1.0
        borderLine.frame = CGRect(x: 0, y: Double(uiView.frame.height) - height, width: Double(uiView.frame.width), height: height)
        
        borderLine.backgroundColor = borderColor
        uiView.addSubview(borderLine)
    }
    
    /*
     * Get images array as per rating
     */
    static func displayRating(rating:String?) -> [UIImage]?{
        var ratinfArray = [UIImage]()
        if(rating != ""){
            if (rating == "5"){
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "full_star.png")!)
            }
            else if (rating == "4.5"){
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "half_star.png")!)
            }
            else if (rating == "4"){
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
            }
            else if (rating == "3.5"){
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "half_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
            }
            else if (rating == "3"){
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
            }
            else if (rating == "2.5"){
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "half_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
            }
            else if (rating == "2"){
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
            }
            else if (rating == "1.5"){
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "half_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
            }
            else if (rating == "1"){
                ratinfArray.append(UIImage(named: "full_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
            }
            else{
                ratinfArray.append(UIImage(named: "empty_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
                ratinfArray.append(UIImage(named: "empty_star.png")!)
            }
        }
        return ratinfArray
    }
    
    static func toastView(messsage : String, view: UIView ){
        let toastLabel = UILabel(frame: CGRect(x: view.frame.size.width/2 - 150, y: view.frame.size.height-100, width: 300,  height : 35))
        toastLabel.backgroundColor = UIColor.black
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = NSTextAlignment.center;
        view.addSubview(toastLabel)
        toastLabel.text = messsage
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        UIView.animate(withDuration: 4.0, delay: 0.1, options: UIView.AnimationOptions.curveEaseOut, animations: {
            toastLabel.alpha = 0.0
            
        })
    }
}
