//
//  RestService.swift
//  TechnoPurple
//
//  Created by Apple on 19/01/18.
//  Copyright © 2018 Appleechnopurple.com. All rights reserved.
//

import UIKit


class RestService: NSObject {
    
    //App URL
    static let appURL = "https://leamigo.com/api/"
    static let URL = "https://leamigo.com/"
    static let homeCarousal = appURL+"User_profile/get_user_app_corousel"
    static let topDestination = appURL+"User_profile/get_top_destination_app"
    static let topExperience = appURL+"Experiences/top_exp_app?device_id=1"
    static let festivals = appURL+"Festivals/upcoming_festival_app?device_id=1"
    static let blogs = appURL+"Blog/get_blog"
    static let trendingACtivity = appURL+"Users/get_trending_exp_app?device_id=1"
    static let offers = appURL+"Offers/all_offers"
    static let wishlist = appURL+"Users/insert_users_wishlist"
    static let cityCountry = appURL+"User_profile/get_cities_countries"
    static let places = appURL+"Search/search_exp_fst"
    static let city = appURL+"search"
    static let signup = appURL+"Auth/signup"
    static let signin = appURL+"Auth/login/la"
    static let google_signin = appURL+"Auth/login/gp"
    static let facebook_signin = appURL+"Auth/login/fb"
    static let bookingHistory = appURL+"Users/booking_details"
    static let update_password = appURL+"User_profile/update_password"
    static let cancel_booking = appURL+"Bookings/cancel_booking_status"
    static let edit_profile = appURL+"User_profile/edit_user_profile"
    static let send_query = appURL+"Users/send_query_email"
    static let forgot_password = appURL+"Auth/forgot_password"
    static let add_booking = appURL+"Bookings/book"
    static let add_amigo_booking = appURL+"Bookings/book_amigo"
    static let detail =  appURL+"Experiences/exp_details?exp_id="
     static let fest_detail =  appURL+"Festivals/get_festival_details?festival_id="
    static let currency_rate = appURL+"User_profile/get_currency_rate"
    static let interest = appURL+"Bookings/all_interests"
    static let country_code =  "http://ip-api.com/json"
    static let payment =  URL+"Transactions/initiate_transaction"
    
}
