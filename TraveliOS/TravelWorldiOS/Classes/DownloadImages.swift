//
//  DownloadImages.swift
//  LeamigoiOS
//
//  Created by Apple on 10/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit

class DownloadImages: NSObject {
    
    public var imagedelegate : ImageResponseProtocol!
    
    override init(){
        super.init()
    }
    
    
    init(delegate : ImageResponseProtocol) {
        super.init()
        self.imagedelegate = delegate
        
    }
    
    func downloadImageAsync(URLString: String, placeHolderImage: UIImage?,identifier : String?,id:String?) {
        
        let imageCache = NSCache<NSString, UIImage>()
        
        if let cachedImage = imageCache.object(forKey: NSString(string: URLString)) {
            imagedelegate.onImageResponse(isSuccess: true, image: cachedImage, error: nil, anyIdentifier: identifier, id: id)
            return
        }
        
        if let url = URL(string: URLString) {
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                
                //print("RESPONSE FROM API: \(response)")
                if error != nil {
                    print("ERROR LOADING IMAGES FROM URL: \(error!)")
                    DispatchQueue.main.async {
                        if(placeHolderImage != nil){
                            self.imagedelegate.onImageResponse(isSuccess: true, image: placeHolderImage, error: error as AnyObject, anyIdentifier: identifier,id:id)
                        }
                    }
                    return
                }
                DispatchQueue.main.async {
                    if let data = data {
                        if let downloadedImage = UIImage(data: data) {
                            imageCache.setObject(downloadedImage, forKey: NSString(string: URLString))
                            self.imagedelegate.onImageResponse(isSuccess: true, image: downloadedImage, error: nil, anyIdentifier: identifier,id:id)
                        }
                    }
                }
            }).resume()
        }
    }
}
