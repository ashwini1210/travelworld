//
//  ImageResponseProtocol.swift
//  LeamigoiOS
//
//  Created by Apple on 10/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import Foundation
import UIKit

protocol ImageResponseProtocol {
    
    func onImageResponse(isSuccess:Bool?, image:UIImage?, error: AnyObject?, anyIdentifier:String? ,id : String?)
    
}
