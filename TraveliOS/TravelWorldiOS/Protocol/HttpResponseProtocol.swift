

import Foundation
import UIKit
import SwiftyJSON

/*
 *  Protocol method to show  response from server in respective viewcontroller
 */
protocol HttpResponseProtocol {
    
    func onHttpResponse(isSuccess:Bool, responseJson:JSON, error: AnyObject!, anyIdentifier:String )

}
