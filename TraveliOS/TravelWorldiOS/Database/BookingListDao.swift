//
//  BookingListDao.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 08/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class BookingListDao: Object {    
    @objc dynamic var product_id : String = ""
    @objc dynamic var duration : String = ""
    @objc dynamic var vertical_id : String = ""
    @objc dynamic var assigned_to : String = ""
    @objc dynamic var booking_id : String = ""
    @objc dynamic var booking_date : String = ""
    @objc dynamic var booking_time : String = ""
    @objc dynamic var meetup_point : String = ""
    @objc dynamic var payment_status : String = ""
    @objc dynamic var pax : String = ""
    @objc dynamic var amigo_status : String = ""
    @objc dynamic var total_amount : String = ""
    @objc dynamic var first_name : String = ""
    @objc dynamic var last_name : String = ""
    @objc dynamic var gender : String = ""
    @objc dynamic var image_url : String = ""
    @objc dynamic var interests : String = ""
    @objc dynamic var city_name : String = ""
    @objc dynamic var experience_name : String = ""
    @objc dynamic var card_image_url : String = ""
    @objc dynamic var amigo_name : String = ""
    
    override static func primaryKey() -> String? {
        return "booking_id"
    }
}
