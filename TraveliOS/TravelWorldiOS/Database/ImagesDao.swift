//
//  ImagesDao.swift
//  LeamigoiOS
//
//  Created by Apple on 10/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import Foundation
import RealmSwift

class ImagesDao: Object {
    @objc dynamic var id : String = ""
    @objc dynamic var imageURL : String = ""
    @objc dynamic var text : String = ""    
    @objc dynamic var image_base64 : String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
