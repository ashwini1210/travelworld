//
//  FestivalDetailDao.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 22/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift


class FestivalDetailDao: Object {
    
    @objc dynamic var id : String = ""
    @objc dynamic var product_id : String = ""
    @objc dynamic var festival_name : String = ""
    @objc dynamic var city : String = ""
    @objc dynamic var tag_line : String = ""
    @objc dynamic var city_id : String = ""
    @objc dynamic var desc : String = ""
   // @objc dynamic var major_attraction : String = ""
    @objc dynamic var included : String = ""
    @objc dynamic var not_included : String = ""
    @objc dynamic var about_amigo : String = ""
    @objc dynamic var duration_min : String = ""
    @objc dynamic var duration_max : String = ""
    @objc dynamic var duration_type : String = ""
    @objc dynamic var min_guests : String = ""
    @objc dynamic var max_guests : String = ""
    @objc dynamic var base_rate : String = ""
    @objc dynamic var base_currency_id : String = ""
    @objc dynamic var confirmation_policy : String = ""
    @objc dynamic var cancellation_policy : String = ""
    @objc dynamic var pickup_point : String = ""
    @objc dynamic var youtube_link : String = ""
     @objc dynamic var about_festival : String = ""
    @objc dynamic var currency_sign : String = ""
    @objc dynamic var month : String = ""
     @objc dynamic var day : String = ""
     @objc dynamic var category_id : String = ""
     @objc dynamic var is_top : String = ""
     @objc dynamic var status : String = ""
     @objc dynamic var festival_id : String = ""
    @objc dynamic var card_image_url : String = ""
    @objc dynamic var price_start : String = ""
    
   
    let detailImagesDao = List<DetailImagesDao>()
    let similarDetailDao = List<SimilarDetailDao>()
    
    override static func primaryKey() -> String? {
        return "festival_id"
    }
}
