//
//  BaseDAO.swift
//  effy
//
//  Created by joyson on 21/04/16.
//  Copyright © 2016 TechnoPurple. All rights reserved.
//

import Foundation
import RealmSwift

class BaseDB {
  
  var realm: Realm
  var name1 : String = "BaseDB"
  
  init()
  {    
    self.realm = try! Realm()
  }
  
  func save(_ object: Object)
  {
    do{
      try realm.write {
        realm.add(object)
      }
    }
    catch let error as NSError
    {
     
    }
  }
  
  func update(_ object: Object)
  {
    do{
      try realm.write {
        realm.add(object, update: true)
      }
    }
    catch let error as NSError
    {
     
    }
  }
  
  func update(_ object: Object, inTransaction : Bool)
  {
    do{
       // objc_sync_enter(object)
        if inTransaction
        {
            realm.add(object, update: true)
        }
        else
        {
          try realm.write {
            realm.add(object, update: true)
          }
        }
     // objc_sync_exit(object)
    }
    catch let error as NSError
    {
     
    }
  }
  
  func beginWrite()
  {
    realm.beginWrite()
  }
  
  func commitWrite()
  {
    do{
      try realm.commitWrite()
    }
    catch let error as NSError
    {
    
    }
  }
 
  
}
