//
//  BookingDB.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 08/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class BookingDB: BaseDB {
    
    override init()
    {
        super.init();
    }
    
    func findAll() -> Results<BookingDao>
    {
        return realm.objects(BookingDao.self)
    }
    
    func findByProperty(property:String,  value: String) -> Results<BookingDao>
    {
        return realm.objects(BookingDao.self).filter("\(property)='\(value)'")
    }
    
    func delete(bookingDao: BookingDao)
    {
        do{
            try realm.write {
                realm.delete(bookingDao)
            }
        }
        catch let error as NSError
        {
            print("Error in BookingDao while deleting data : \(error)")
            
        }
    }
    
    func delete()
    {
        do{
            try realm.write {
                realm.delete(realm.objects(BookingDao.self))
            }
        }
        catch let error as NSError
        {
            print("Error in BookingDao while deleting data : \(error)")
            
        }
    }
}
