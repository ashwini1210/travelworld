//
//  AddBookingDao.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 11/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class AddBookingDao: Object {
    
    @objc dynamic var booking_id : String = ""
    @objc dynamic var booking_date : String = ""
    @objc dynamic var total_amount : String = ""
    @objc dynamic var pax : String = ""
    @objc dynamic var pax_rate : String = ""
    
//    @objc dynamic var child : String = ""
//    @objc dynamic var adult : String = ""
//    @objc dynamic var product_id : String = ""
//    @objc dynamic var vertical_id : String = ""
//    @objc dynamic var city_id : String = ""
//    @objc dynamic var currency_id : String = ""
//    @objc dynamic var base_price : String = ""
//    @objc dynamic var duration_type : String = ""
    
    override static func primaryKey() -> String? {
        return "booking_id"
    }
}
