//
//  HomeCarousalDao.swift
//  LeamigoiOS
//
//  Created by Apple on 07/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import Foundation
import RealmSwift

class HomeCarousalDao: Object {

    @objc dynamic var id : Int  = 1
    @objc dynamic var status : Bool = false
    @objc dynamic var message : String = ""
     let imagesDaoList = List<ImagesDao>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func getNextKey() -> Int
    {
        let id = AppPreference.getInstance().getInt(AppConstants.DB_HOME_CAROUSAL_ID, defaultvalue: 1)
        AppPreference.getInstance().setInt(AppConstants.DB_HOME_CAROUSAL_ID, value: (id+1))
        return id;
    }
}
