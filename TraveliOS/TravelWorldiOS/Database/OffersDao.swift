//
//  OffersDao.swift
//  LeamigoiOS
//
//  Created by Apple on 11/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class OffersDao: Object {
    @objc dynamic var id : Int  = 1
    @objc dynamic var status : Bool = false
    @objc dynamic var message : String = ""
    let offersListDao = List<OffersListDao>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
