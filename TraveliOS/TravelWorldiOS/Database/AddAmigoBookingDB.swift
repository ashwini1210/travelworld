//
//  AddAmigoBookingDB.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 12/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class AddAmigoBookingDB: BaseDB {
    
    override init()
    {
        super.init();
    }
    
    func findAll() -> Results<AddAmigoBookingDao>
    {
        return realm.objects(AddAmigoBookingDao.self)
    }
    
    func findByProperty(property:String,  value: String) -> Results<AddAmigoBookingDao>
    {
        return realm.objects(AddAmigoBookingDao.self).filter("\(property)='\(value)'")
    }
    
    func delete(addAmigoBookingDao: AddAmigoBookingDao)
    {
        do{
            try realm.write {
                realm.delete(addAmigoBookingDao)
            }
        }
        catch let error as NSError
        {
            print("Error in AddAmigoBookingDao while deleting data : \(error)")
            
        }
    }
    
    func delete()
    {
        do{
            try realm.write {
                realm.delete(realm.objects(AddAmigoBookingDao.self))
            }
        }
        catch let error as NSError
        {
            print("Error in AddAmigoBookingDao while deleting data : \(error)")
            
        }
    }
}
