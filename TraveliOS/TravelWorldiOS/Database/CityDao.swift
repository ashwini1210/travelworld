//
//  CityDao.swift
//  LeamigoiOS
//
//  Created by Apple on 14/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class CityDao: Object {
    
    @objc dynamic var city_id : String = ""
    @objc dynamic var city_name : String = ""
    @objc dynamic var country_name : String = ""
    @objc dynamic var flag_url : String = ""
    @objc dynamic var slug : String = ""
    
}
