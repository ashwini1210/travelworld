//
//  TopDestinationDB.swift
//  LeamigoiOS
//
//  Created by Apple on 10/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class TopDestinationDB: BaseDB {

    override init()
    {
        super.init();
    }
    
    func findAll() -> Results<TopDestinationDao>
    {
        return realm.objects(TopDestinationDao.self)
    }
    
    func findByProperty(property:String,  value: String) -> Results<TopDestinationDao>
    {
        return realm.objects(TopDestinationDao.self).filter("\(property)='\(value)'")
    }
    
    
    
    func delete(topDestinationDao: TopDestinationDao)
    {
        do{
            try realm.write {
                realm.delete(topDestinationDao)
            }
        }
        catch let error as NSError
        {
            print("Error in TopDestinationDao while deleting data : \(error)")
            
        }
    }
    
    
    func delete()
    {
        do{
            try realm.write {
                realm.delete(realm.objects(TopDestinationDao.self))
                realm.delete(realm.objects(DestinationListDao.self))
            }
        }
        catch let error as NSError
        {
            print("Error in TopDestinationDao while deleting data : \(error)")
            
        }
    }
}
