//
//  DetailDao.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 13/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class DetailDao: Object {

    @objc dynamic var city : String = ""
    @objc dynamic var heading : String = ""
    @objc dynamic var slug : String = ""
    @objc dynamic var tag_line : String = ""
    @objc dynamic var city_id : String = ""
    @objc dynamic var category_id : String = ""
    @objc dynamic var desc : String = ""
    @objc dynamic var major_attraction : String = ""
    @objc dynamic var included : String = ""
    @objc dynamic var not_included : String = ""
    @objc dynamic var about_amigo : String = ""
    @objc dynamic var duration_min : String = ""
    @objc dynamic var duration_max : String = ""
    @objc dynamic var duration_type : String = ""
    @objc dynamic var min_guests : String = ""
    @objc dynamic var max_guests : String = ""
    @objc dynamic var base_rate : String = ""
    @objc dynamic var base_currency_id : String = ""
    @objc dynamic var confirmation_policy : String = ""
    @objc dynamic var cancellation_policy : String = ""
    @objc dynamic var pickup_point : String = ""
    @objc dynamic var exp_type : String = ""
    @objc dynamic var mode_of_transport : String = ""
    @objc dynamic var currency_sign : String = ""
    @objc dynamic var abbreviation : String = ""
    @objc dynamic var category_name : String = ""
    @objc dynamic var lanuages : String = ""
    
    
    let detailImagesDao = List<DetailImagesDao>()
    let similarDetailDao = List<SimilarDetailDao>()
    
    override static func primaryKey() -> String? {
        return "city_id"
    }
    
}
