//
//  WishlistDB.swift
//  LeamigoiOS
//
//  Created by Apple on 14/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class WishlistDB: BaseDB {
    override init()
    {
        super.init();
    }
    
    func findAll() -> Results<WishlistDao>
    {
        return realm.objects(WishlistDao.self)
    }
    
    func findByProperty(property:String,  value: String) -> Results<WishlistDao>
    {
        return realm.objects(WishlistDao.self).filter("\(property)='\(value)'")
    }
    
    func delete(wishlistDao: WishlistDao)
    {
        do{
            try realm.write {
                realm.delete(wishlistDao)
            }
        }
        catch let error as NSError
        {
            print("Error in WishlistDao while deleting data : \(error)")
            
        }
    }
    
    
    func delete()
    {
        do{
            try realm.write {
                realm.delete(realm.objects(WishlistDao.self))
            }
        }
        catch let error as NSError
        {
            print("Error in WishlistDao while deleting data : \(error)")
            
        }
    }
}
