//
//  OffersDB.swift
//  LeamigoiOS
//
//  Created by Apple on 11/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class OffersDB: BaseDB {
    override init()
    {
        super.init();
    }
    
    func findAll() -> Results<OffersDao>
    {
        return realm.objects(OffersDao.self)
    }
    
    func findByProperty(property:String,  value: String) -> Results<OffersDao>
    {
        return realm.objects(OffersDao.self).filter("\(property)='\(value)'")
    }
    
    func delete(offersDao: OffersDao)
    {
        do{
            try realm.write {
                realm.delete(offersDao)
            }
        }
        catch let error as NSError
        {
            print("Error in OffersDao while deleting data : \(error)")
            
        }
    }
    
    
    func delete()
    {
        do{
            try realm.write {
                realm.delete(realm.objects(OffersDao.self))
                realm.delete(realm.objects(OffersListDao.self))
            }
        }
        catch let error as NSError
        {
            print("Error in OffersDao while deleting data : \(error)")
            
        }
    }
}
