//
//  BlogListDao.swift
//  LeamigoiOS
//
//  Created by Apple on 11/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class BlogListDao: Object {

    @objc dynamic var id : String = ""
    @objc dynamic var title : String = ""
    @objc dynamic var isFeatured : String = ""
    @objc dynamic var desc : String = ""
    @objc dynamic var writtenBy : String = ""
    @objc dynamic var readTime : String = ""
    @objc dynamic var cardimageURL : String = ""
    @objc dynamic var profileImageUrl : String = ""
    @objc dynamic var image_base64 : String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
