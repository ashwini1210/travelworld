//
//  DetailDB.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 13/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class DetailDB: BaseDB {
    
    override init()
    {
        super.init();
    }
    
    func findAll() -> Results<DetailDao>
    {
        return realm.objects(DetailDao.self)
    }
    
    func findByProperty(property:String,  value: String) -> Results<DetailDao>
    {
        return realm.objects(DetailDao.self).filter("\(property)='\(value)'")
    }
    
    func delete(detailDao: DetailDao)
    {
        do{
            try realm.write {
                realm.delete(detailDao)
            }
        }
        catch let error as NSError
        {
            print("Error in DetailDao while deleting data : \(error)")
            
        }
    }
    
    func delete()
    {
        do{
            try realm.write {
                realm.delete(realm.objects(DetailDao.self))
                realm.delete(realm.objects(DetailImagesDao.self))
                realm.delete(realm.objects(SimilarDetailDao.self))
            }
        }
        catch let error as NSError
        {
            print("Error in DetailDao while deleting data : \(error)")
            
        }
    }
}
