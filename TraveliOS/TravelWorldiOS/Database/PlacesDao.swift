//
//  PlacesDao.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 18/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class PlacesDao: Object {
    @objc dynamic var id : Int  = 1
    @objc dynamic var status : Bool = false
    let experienceListDao = List<ExperienceListDao>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
