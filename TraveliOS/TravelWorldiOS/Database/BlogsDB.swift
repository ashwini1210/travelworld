//
//  BlogsDB.swift
//  LeamigoiOS
//
//  Created by Apple on 11/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class BlogsDB: BaseDB {
    
    override init()
    {
        super.init();
    }
    
    func findAll() -> Results<BlogsDao>
    {
        return realm.objects(BlogsDao.self)
    }
    
    func findByProperty(property:String,  value: String) -> Results<BlogsDao>
    {
        return realm.objects(BlogsDao.self).filter("\(property)='\(value)'")
    }
    
    func delete(blogsDao: BlogsDao)
    {
        do{
            try realm.write {
                realm.delete(blogsDao)
            }
        }
        catch let error as NSError
        {
            print("Error in BlogsDao while deleting data : \(error)")
            
        }
    }
    
    
    func delete()
    {
        do{
            try realm.write {
                realm.delete(realm.objects(BlogsDao.self))
                   realm.delete(realm.objects(BlogListDao.self))
            }
        }
        catch let error as NSError
        {
            print("Error in BlogsDao while deleting data : \(error)")
            
        }
    }
}
