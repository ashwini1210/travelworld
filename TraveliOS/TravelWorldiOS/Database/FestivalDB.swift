//
//  FestivalDB.swift
//  LeamigoiOS
//
//  Created by Apple on 10/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class FestivalDB: BaseDB {

    override init()
    {
        super.init();
    }
    
    func findAll() -> Results<FestivalDao>
    {
        return realm.objects(FestivalDao.self)
    }
    
    func findByProperty(property:String,  value: String) -> Results<FestivalDao>
    {
        return realm.objects(FestivalDao.self).filter("\(property)='\(value)'")
    }
    
    func delete(festivalDao: FestivalDao)
    {
        do{
            try realm.write {
                realm.delete(festivalDao)
            }
        }
        catch let error as NSError
        {
            print("Error in FestivalDao while deleting data : \(error)")
            
        }
    }
    
    
    func delete()
    {
        do{
            try realm.write {
                realm.delete(realm.objects(FestivalDao.self))
                 realm.delete(realm.objects(FestivalListDao.self))
            }
        }
        catch let error as NSError
        {
            print("Error in FestivalDao while deleting data : \(error)")
            
        }
    }
}
