//
//  OffersListDao.swift
//  LeamigoiOS
//
//  Created by Apple on 11/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class OffersListDao: Object {
    
    @objc dynamic var id : String = ""
    @objc dynamic var name : String = ""
    @objc dynamic var desc : String = ""
    @objc dynamic var usage_type : String = ""
    @objc dynamic var value : String = ""
    @objc dynamic var value_type : String = ""
    @objc dynamic var status : String = ""
    @objc dynamic var cardImage : String = ""
    @objc dynamic var added_on : String = ""
    @objc dynamic var added_by : String = ""
    @objc dynamic var updated_on : String = ""
    @objc dynamic var updated_by : String = ""
    @objc dynamic var image_base64 : String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
