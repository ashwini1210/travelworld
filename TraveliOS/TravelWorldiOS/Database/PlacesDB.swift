//
//  PlacesDB.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 18/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class PlacesDB: BaseDB {

    override init()
    {
        super.init();
    }
    
    func findAll() -> Results<PlacesDao>
    {
        return realm.objects(PlacesDao.self)
    }
    
    func findByProperty(property:String,  value: String) -> Results<PlacesDao>
    {
        return realm.objects(PlacesDao.self).filter("\(property)='\(value)'")
    }
    
    func delete(placesDao: PlacesDao)
    {
        do{
            try realm.write {
                realm.delete(placesDao)
            }
        }
        catch let error as NSError
        {
            print("Error in PlacesDao while deleting data : \(error)")
            
        }
    }
    
    func delete()
    {
        do{
            try realm.write {
                realm.delete(realm.objects(PlacesDao.self))
            }
        }
        catch let error as NSError
        {
            print("Error in PlacesDao while deleting data : \(error)")
            
        }
    }
}
