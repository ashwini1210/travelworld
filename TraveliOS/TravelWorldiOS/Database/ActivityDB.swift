//
//  ActivityDB.swift
//  LeamigoiOS
//
//  Created by Apple on 11/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class ActivityDB: BaseDB {

    override init()
    {
        super.init();
    }
    
    func findAll() -> Results<ActivityDao>
    {
        return realm.objects(ActivityDao.self)
    }
    
    func findByProperty(property:String,  value: String) -> Results<ActivityDao>
    {
        return realm.objects(ActivityDao.self).filter("\(property)='\(value)'")
    }
    
    func delete(activityDao: ActivityDao)
    {
        do{
            try realm.write {
                realm.delete(activityDao)
            }
        }
        catch let error as NSError
        {
            print("Error in ActivityDao while deleting data : \(error)")
            
        }
    }
    
    
    func delete()
    {
        do{
            try realm.write {
                realm.delete(realm.objects(ActivityDao.self))
                  realm.delete(realm.objects(ActivityListDao.self))
            }
        }
        catch let error as NSError
        {
            print("Error in ActivityDao while deleting data : \(error)")
            
        }
    }
}
