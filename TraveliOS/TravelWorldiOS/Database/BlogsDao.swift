//
//  BlogsDao.swift
//  LeamigoiOS
//
//  Created by Apple on 11/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class BlogsDao: Object {
    @objc dynamic var id : Int  = 1
    @objc dynamic var status : Bool = false
    let blogListDao = List<BlogListDao>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
