//
//  SimilarDetailDao.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 13/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class SimilarDetailDao: Object {
    @objc dynamic var id : String = ""
    @objc dynamic var heading : String = "" //for festivalDetailModel it is festival_name
    @objc dynamic var slug : String = ""
    @objc dynamic var city : String = ""
    @objc dynamic var category_id : String = ""
    @objc dynamic var tag_line : String = ""
    @objc dynamic var price_start : String = ""
    @objc dynamic var currency_id : String = ""
    @objc dynamic var status : String = ""
    @objc dynamic var is_top : String = ""
    @objc dynamic var card_image_url : String = ""
    @objc dynamic var country_name : String = "" 
    @objc dynamic var country_id : String = ""
    @objc dynamic var image_base64 : String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
