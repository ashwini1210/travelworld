//
//  FestivalDetailDB.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 22/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class FestivalDetailDB: BaseDB {
    override init()
    {
        super.init();
    }
    
    func findAll() -> Results<FestivalDetailDao>
    {
        return realm.objects(FestivalDetailDao.self)
    }
    
    func findByProperty(property:String,  value: String) -> Results<FestivalDetailDao>
    {
        return realm.objects(FestivalDetailDao.self).filter("\(property)='\(value)'")
    }
    
    func delete(detailDao: FestivalDetailDao)
    {
        do{
            try realm.write {
                realm.delete(detailDao)
            }
        }
        catch let error as NSError
        {
            print("Error in FestivalDetailDao while deleting data : \(error)")
            
        }
    }
    
    func delete()
    {
        do{
            try realm.write {
                realm.delete(realm.objects(FestivalDetailDao.self))
                realm.delete(realm.objects(DetailImagesDao.self))
                realm.delete(realm.objects(SimilarDetailDao.self))
            }
        }
        catch let error as NSError
        {
            print("Error in DetailDao while deleting data : \(error)")
            
        }
    }
}
