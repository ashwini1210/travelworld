//
//  TopExperienceDB.swift
//  LeamigoiOS
//
//  Created by Apple on 10/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class TopExperienceDB: BaseDB {

    override init()
    {
        super.init();
    }
    
    func findAll() -> Results<TopExperienceDao>
    {
        return realm.objects(TopExperienceDao.self)
    }
    
    func findByProperty(property:String,  value: String) -> Results<TopExperienceDao>
    {
        return realm.objects(TopExperienceDao.self).filter("\(property)='\(value)'")
    }
    
    func delete(topExperienceDao: TopExperienceDao)
    {
        do{
            try realm.write {
                realm.delete(topExperienceDao)
            }
        }
        catch let error as NSError
        {
            print("Error in TopExperienceDao while deleting data : \(error)")
            
        }
    }
    
   
    func delete()
    {
        do{
            try realm.write {
                realm.delete(realm.objects(TopExperienceDao.self))
                realm.delete(realm.objects(ExperienceListDao.self))
            }
        }
        catch let error as NSError
        {
            print("Error in TopExperienceDao while deleting data : \(error)")
            
        }
    }
}
