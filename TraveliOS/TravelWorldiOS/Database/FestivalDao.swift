//
//  FestivalDao.swift
//  LeamigoiOS
//
//  Created by Apple on 10/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class FestivalDao: Object {
    @objc dynamic var id : Int  = 1
    @objc dynamic var status : Bool = false
    let festivalListDao = List<FestivalListDao>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
