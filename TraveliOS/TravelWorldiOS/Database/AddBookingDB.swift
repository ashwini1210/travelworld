//
//  AddBookingDB.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 11/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class AddBookingDB: BaseDB {

    override init()
    {
        super.init();
    }
    
    func findAll() -> Results<AddBookingDao>
    {
        return realm.objects(AddBookingDao.self)
    }
    
    func findByProperty(property:String,  value: String) -> Results<AddBookingDao>
    {
        return realm.objects(AddBookingDao.self).filter("\(property)='\(value)'")
    }
    
    func delete(addBookingDao: AddBookingDao)
    {
        do{
            try realm.write {
                realm.delete(addBookingDao)
            }
        }
        catch let error as NSError
        {
            print("Error in AddBookingDao while deleting data : \(error)")
            
        }
    }
    
    func delete()
    {
        do{
            try realm.write {
                realm.delete(realm.objects(AddBookingDao.self))
            }
        }
        catch let error as NSError
        {
            print("Error in AddBookingDao while deleting data : \(error)")
            
        }
    }
}
