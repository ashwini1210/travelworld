//
//  AddAmigoBookingDao.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 12/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class AddAmigoBookingDao: Object {
    @objc dynamic var pax : String = ""
    @objc dynamic var booking_id : String = ""
    @objc dynamic var pax_rate : String = ""
    @objc dynamic var total_amount : String = ""
    @objc dynamic var booking_date : String = ""
}
