//
//  BookingDao.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 08/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class BookingDao: Object {
    @objc dynamic var id : Int  = 1
    @objc dynamic var status : Bool = false
    let bookingHistoryDao = List<BookingListDao>()
    let bookingUpcomingDao = List<BookingListDao>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
