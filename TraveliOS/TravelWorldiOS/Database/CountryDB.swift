//
//  CountryDB.swift
//  LeamigoiOS
//
//  Created by Apple on 14/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class CountryDB: BaseDB {
    
    override init()
    {
        super.init();
    }
    
    func findAll() -> Results<CountryDao>
    {
        return realm.objects(CountryDao.self)
    }
    
    func findByProperty(property:String,  value: String) -> Results<CountryDao>
    {
        return realm.objects(CountryDao.self).filter("\(property)='\(value)'")
    }
    
    func delete(countryDao: CountryDao)
    {
        do{
            try realm.write {
                realm.delete(countryDao)
            }
        }
        catch let error as NSError
        {
            print("Error in CountryDao while deleting data : \(error)")
            
        }
    }
    
    
    func delete()
    {
        do{
            try realm.write {
                realm.delete(realm.objects(CountryDao.self))
            }
        }
        catch let error as NSError
        {
            print("Error in countryDao while deleting data : \(error)")
            
        }
    }
}
