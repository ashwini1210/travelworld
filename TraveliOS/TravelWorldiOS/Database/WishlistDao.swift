//
//  WishlistDao.swift
//  LeamigoiOS
//
//  Created by Apple on 14/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class WishlistDao: Object {
    @objc dynamic var product_id : String = ""
    @objc dynamic var category_id : String = ""
    @objc dynamic var imagebase64 : String = ""
    @objc dynamic var addedWishlist : Bool = false
    
    override static func primaryKey() -> String? {
        return "product_id"
    }
}
