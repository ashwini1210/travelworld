//
//  DestinationListDao.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 10/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class DestinationListDao: Object {
    @objc dynamic var id : String = ""
    @objc dynamic var imageURL : String = ""
    @objc dynamic var text : String = ""
    @objc dynamic var image_base64 : String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
