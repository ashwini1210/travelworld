//
//  DetailImagesDao.swift
//  LeamigoiOS
//
//  Created by Ashwini Bankar on 13/10/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class DetailImagesDao: Object {
    @objc dynamic var image_url : String = ""
    @objc dynamic var id : String = ""
}
