//
//  ActivityListDao.swift
//  LeamigoiOS
//
//  Created by Apple on 11/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import UIKit
import RealmSwift

class ActivityListDao: Object {
    @objc dynamic var id : String = ""
    @objc dynamic var heading : String = ""
    @objc dynamic var cityName : String = ""
    @objc dynamic var countryId : String = ""
    @objc dynamic var tagLine : String = ""
    @objc dynamic var priceStart : String = ""
    @objc dynamic var currencyId : String = ""
    @objc dynamic var status : String = ""
    @objc dynamic var isTop : String = ""
    @objc dynamic var cardImage : String = ""
    @objc dynamic var currencySign : String = ""
    @objc dynamic var currencyName : String = ""
    @objc dynamic var countryName : String = ""
    @objc dynamic var abbreviation : String = ""
    @objc dynamic var ratings : String = ""
    @objc dynamic var liked : String = ""
    @objc dynamic var image_base64 : String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
