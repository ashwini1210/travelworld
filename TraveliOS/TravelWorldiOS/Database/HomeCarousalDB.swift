//
//  HomeCarousalDB.swift
//  LeamigoiOS
//
//  Created by Apple on 07/09/18.
//  Copyright © 2018 Ashwini Bankar. All rights reserved.
//

import Foundation
import RealmSwift

class HomeCarousalDB: BaseDB {
    
    override init()
    {
        super.init();
    }
    
    func findAll() -> Results<HomeCarousalDao>
    {
        return realm.objects(HomeCarousalDao.self)
    }
    
    func findByProperty(property:String,  value: String) -> Results<HomeCarousalDao>
    {
        return realm.objects(HomeCarousalDao.self).filter("\(property)='\(value)'")
    }
    

    
    func delete(homeCarousalDao: HomeCarousalDao)
    {
        do{
            try realm.write {
                realm.delete(homeCarousalDao)
            }
        }
        catch let error as NSError
        {
            print("Error in homeCarousalDao while deleting data : \(error)")
            
        }
    }
    
    
    func delete()
    {
        do{
            try realm.write {
                realm.delete(realm.objects(HomeCarousalDao.self))
                realm.delete(realm.objects(ImagesDao.self))
            }
        }
        catch let error as NSError
        {
            print("Error in HomeCarousalDao while deleting data : \(error)")
            
        }
    }
}
